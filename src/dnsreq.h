/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __PMD_DNSREQ_H
#define __PMD_DNSREQ_H

/*
 * We need both direct and indirect DNS certificate type identifiers.
 * Many systems lack the indirect version. See RFC 4398 for details.
 */
#ifndef HAVE_CERT_T_PKIX
#define cert_t_pkix	1
#endif
#ifndef HAVE_CERT_T_IPKIX
#define cert_t_ipkix	4
#endif

#include <netinet/in.h>

#include "array.h"
#include "cert.h"

#define MAX_IPS	16

typedef struct __dns_srv_t {
	uint16_t	priority;
	uint16_t	weight;
	uint16_t	port;
	char		target[MAXDNAME];
	int		nipv4;
	in_addr_t	ipv4[MAX_IPS];
	int		nipv6;
	struct in6_addr	ipv6[MAX_IPS];
} dns_srv_t;

array_t *query_srv(const char *domname);
char *query_ptr(int af, u_char *addr);
x509_crt_t *query_cert_rev(int af, u_char *addr, int *ncerts);
x509_crt_t *query_cert(const char *name, int *ncerts);

#endif
