/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2011,2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __PMD_PMD_H
#define __PMD_PMD_H

#include <stdint.h>
#include <endian.h>

#if __BYTE_ORDER != __BIG_ENDIAN && __BYTE_ORDER != __LITTLE_ENDIAN
#error "Byte-sex error: Endian-type not supported"
#endif

/* Default port and MC address */
#define PMD_PORT 	0xbe11			/* 48657 */
#define PMD_MCADDR	"239.255.0.1"
#define PMD_MCADDR6	"ff14::1"

#define PMD_PKT_VERSION_1	1
#define PMD_PKT_VERSION_2	2
#define PMD_PKT_VERSION		(PMD_PKT_VERSION_2)	/* Current packet format version */


/* PMD packet flags */
/* FIXME: the flags are in bitfields; so we don't need masks? */
#define PMDF_FORCESILENT	0x00000001	/* Be silent (non-audible) when event is received */
#define PMDF_MASK		0x00000001	/* Encapsulate all defined flags */

/* PMD message types */
#define PMDT_CATEGORY		0		/* 32 bit Category specification */
#define PMDT_SUBCATEGORY	1		/* 32 bit Subcategory specification */
#define PMDT_TEXT		2		/* Text in UTF-8 */
#define PMDT_RESOURCE		3		/* URI indicator */
#define PMDT_UUID		4		/* 128 bit UUID */
#define PMDT_TIMESTAMP		5		/* 64 bit Target timestamp, see structure below */
#define PMDT_CARDINAL		6		/* 32 bit unsigned number */
#define PMDT_INTEGER		7		/* 32 bit signed number */
#define PMDT_FRACTION		8		/* 64 bit signed dividend / unsigned divisor */
#define PMDT_OID		9		/* Text SNMP OID identifier */
						/* types 10..49151: free for assignment */
						/* types 49152..65279: experimental (private) use */
						/* types 65280..65530: reserved */
#define PMDT_PADDING		65531		/* Padding bytes */
#define PMDT_KEY		65532		/* Encryption key */
#define PMDT_ENCRYPTED		65533		/* Encrypted content */
#define PMDT_FINGERPRINT	65534		/* Sender certificate fingerprint */
#define PMDT_SIGNATURE		65535		/* RSA signature (must be last content) */

/* Encryption algorithms */
#define PMDE_AES128_CBC		0		/* AES in CBC mode with 128-bit keys */
#define PMDE_AES192_CBC		1		/* AES in CBC mode with 192-bit keys */
#define PMDE_AES256_CBC		2		/* AES in CBC mode with 256-bit keys */
#define PMDE_AES128_CFB		3		/* AES in CFB mode with 128-bit keys */
#define PMDE_AES192_CFB		4		/* AES in CFB mode with 192-bit keys */
#define PMDE_AES256_CFB		5		/* AES in CFB mode with 256-bit keys */

/* PMD Severity indication */
#define PMDS_DEBUG		7		/* Debugging messages (normally ignored) */
#define PMDS_INFO		6		/* Informational messages */
#define PMDS_NOTICE		5		/* Noticable condition */
#define PMDS_WARNING		4		/* Warning conditions */
#define PMDS_ERROR		3		/* Error conditions */
#define PMDS_CRITICAL		2		/* Critical conditions */
#define PMDS_ALERT		1		/* Alert conditions */
#define PMDS_EMERGENCY		0		/* Emergency messages */

/* PMD message categories */
#define PMDC_LOCAL		0		/* Local scope for private use, often not signed */
#define PMDC_PRIVATE		(PMDC_LOCAL)
#define PMDC_MONITOR		1		/* Messages from monitor systems (e.g. Nagios) */
#define PMDC_LOG		2		/* Logging messages */
#define PMDC_TRAFFIC		3		/* Traffic announcements */
#define PMDC_PUBLIC		4		/* General public announcements */
#define PMDC_COMMERCIAL		128		/* Commercial interests */

/* Subcategory for PMDC_LOCAL */
#define PMDC_LOCAL_NONE			0	/* Subcategory ignored */
#define PMDC_LOCAL_BELL			1	/* Bell ring */
#define PMDC_LOCAL_DINNER		2	/* Dinner is ready, stop gaming and eat */
/* Subcategory for PMDC_MONITOR */
#define PMDC_MONITOR_NONE		0	/* Subcategory ignored */
#define PMDC_MONITOR_TEMPERATURE	1	/* Temperature event */

#define PMDC_SUBCAT_PRIVATE	0x80000000	/* All subcats with high bit set are private values */

/* PMD timestamp structure with seconds and microseconds */
typedef struct __pmdtimestamp_t {
	struct {
#if __BYTE_ORDER == __BIG_ENDIAN
		uint32_t	usecs:20;	/* Sender timestamp; microseconds [0..999999] */
		uint32_t	reserved:4;	/* Reserved; must be 0 */
		uint32_t	secshi:8;	/* Sender timestamp high 8 bits; UNIX Epoch */
#else
		uint32_t	secshi:8;
		uint32_t	reserved:4;
		uint32_t	usecs:20;
#endif
	};
	uint32_t	secslo;		/* Sender timestamp low 32 bits; UNIX Epoch */
} pmdtimestamp_t;

/* PMD fractional number */
typedef struct __pmdfraction_t {
	int32_t		dividend;
	uint32_t	divisor;
} pmdfraction_t;

/* PMD message header */
typedef struct __pmdhdr_t {
	uint8_t		version;	/* Protocol version, currently 0x01 */
	union {
		struct {
#if __BYTE_ORDER == __BIG_ENDIAN
			uint8_t	severity:3;	/* Message severity (PMDS_*) */
			uint8_t	forcesilent:1;	/* Force silent flag */
			uint8_t	sequence:4;	/* Packet repeat send sequence */
#else
			uint8_t	sequence:4;
			uint8_t	forcesilent:1;
			uint8_t	severity:3;
#endif
		};
	};
	uint16_t	reserved;
	pmdtimestamp_t	timestamp;
	uint8_t		uuid[16];		/* Message's UUID */
	/* pmdmsg_t messages[0..n] */
} pmdhdr_t;

/* PMD message content */
/*
 * These are always padded to a 32bit boundary (i.e. length 5 occupies 8
 * octets) _expect_ when this is the last message content.
 */
typedef struct __pmdmsg_t {
	uint16_t	type;
	uint16_t	length;
	uint8_t		octets[];
} pmdmsg_t;

/* PMD encryption key content */
typedef struct __pmdkey_t {
	uint16_t	algorithm;
	uint8_t		octets[];
} pmdkey_t;

#endif
