#/bin/bash

# PingMyDroid receive hook script
# Invoked by:
#  $ pmdrecv -q -x ./recv-hook.sh
#
# Arguments to this script:
# - Severity
# - Category
# - Sub-category
# - Timestamp
# - Flags
# - UUID
# - Sequence

# This wil simply print all the arguments
echo "$@"

# Or you can play a song
# mplayer -really-quiet youvegotavisitor.mp3
