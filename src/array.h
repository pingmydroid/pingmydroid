/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __PMD_ARRAY_H
#define __PMD_ARRAY_H

typedef struct __array_t {
	void	**list;
	int	nlist;
	int	nalist;
} array_t;

#define array_size(a)		((a)->nlist)
#define array_val(a,n,t)	((t)((a)->list[(n)]))

array_t *array_new(void);
array_t *array_init(array_t *a);
void *array_add(array_t *a, void *v);
array_t *array_free_content(array_t *a);
void array_free(array_t *a);

#endif
