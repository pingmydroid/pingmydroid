/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2014  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __PMDP_PMDP_COMPAT_H
#define __PMDP_PMDP_COMPAT_H

#ifndef HAVE_IFADDRS_H
/* From /usr/include/ifaddrs.h */

struct ifaddrs {
	struct ifaddrs	*ifa_next;	/* Next item in list */
	char		*ifa_name;	/* Name of interface */
	unsigned int	ifa_flags;	/* Flags from SIOCGIFFLAGS */
	struct sockaddr	*ifa_addr;	/* Address of interface */
	struct sockaddr	*ifa_netmask;	/* Netmask of interface */
	union {
		struct sockaddr	*ifu_broadaddr;	/* Broadcast address of interface */
		struct sockaddr	*ifu_dstaddr;	/* Point-to-point destination address */
	} ifa_ifu;
#define ifa_broadaddr	ifa_ifu.ifu_broadaddr
#define ifa_dstaddr	ifa_ifu.ifu_dstaddr
	void		*ifa_data;	/* Address-specific data */
};


int getifaddrs(struct ifaddrs **ifap);
void freeifaddrs(struct ifaddrs *ifa);
#endif

#ifndef HAVE_NANOSLEEP
int nanosleep(const struct timespec *req, struct timespec *rem);
#endif

#endif
