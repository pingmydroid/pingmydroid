/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __PMD_CERT_H
#define __PMD_CERT_H

#include <openssl/err.h>
#include <openssl/x509.h>
#include <openssl/evp.h>
#include <openssl/pem.h>

#define MAX_HASH_SIZE	EVP_MAX_MD_SIZE

typedef X509		*x509_crt_t;
typedef EVP_PKEY	*x509_privkey_t;

void crt_init(void);

x509_crt_t crt_crt_load_bin(const char *buf, size_t bufsize);
x509_crt_t crt_crt_load(const char *buf, size_t bufsize);
x509_crt_t crt_crt_load_file(const char *fname);
int crt_crt_fingerprint(x509_crt_t crt, void *buf, size_t *bufsize);
void crt_crt_free(x509_crt_t crt);
int crt_crt_verify(x509_crt_t crt, const void *data, size_t datalen, void *sig, size_t siglen);

x509_crt_t *crt_bundle_load(const char *buf, size_t bufsize, size_t *nlist);
x509_crt_t *crt_bundle_load_file(const char *fname, size_t *nlist);

x509_privkey_t crt_privkey_load(const char *buf, size_t bufsize);
x509_privkey_t crt_privkey_load_file(const char *fname);
void crt_privkey_free(x509_privkey_t pkey);
int crt_privkey_sign(x509_privkey_t pkey, const void *data, size_t datalen, void *sig, size_t *siglen);

#endif
