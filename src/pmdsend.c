/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2011,2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef __WIN32__
#include <ws2tcpip.h>
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>
#include <assert.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>

#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#ifdef HAVE_SYS_IOCTL_H
#include <sys/ioctl.h>
#endif
#ifdef HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#ifdef HAVE_NET_IF_H
#include <net/if.h>
#endif
#ifdef HAVE_IFADDRS_H
#include <ifaddrs.h>
#endif
#ifdef HAVE_NETDB_H
#include <netdb.h>
#endif

#ifdef USE_LIBUUID
#include <uuid/uuid.h>
#endif

#include <pmdp.h>
#include <pmdcommon.h>
#include <pmdp_compat.h>

static int parse_time(const char *s, struct timeval *tv)
{
	time_t t;
	unsigned yy, mo, da, ho, mi;
	double se;
	char dummy;
	struct tm tm;

	if(7 != sscanf(s, "%u-%u-%u%c%u:%u:%lf", &yy, &mo, &da, &dummy, &ho, &mi, &se))
		return 0;
	if(dummy != ' ' && dummy != 't' && dummy != 'T')
		return 0;
	tm.tm_sec = (int)se;
	tm.tm_min = mi;
	tm.tm_hour = ho;
	tm.tm_mday = da;
	tm.tm_mon = mo - 1;
	tm.tm_year = yy - 1900;
	tm.tm_wday = 0;
	tm.tm_yday = 0;
	tm.tm_isdst = -1;
	if((time_t)-1 == (t = mktime(&tm)))
		return 0;
	tv->tv_sec = t;
	tv->tv_usec = (int)(1000000.0 * se) % 1000000;
	return 1;
}

static int get_sock(int af, int loopback, int ttl)
{
	int sock;
	int level = af == AF_INET ? IPPROTO_IP : IPPROTO_IPV6;
	int mcloop = af == AF_INET ? IP_MULTICAST_LOOP : IPV6_MULTICAST_LOOP;
	int mcttl  = af == AF_INET ? IP_MULTICAST_TTL : IPV6_MULTICAST_HOPS;

	if(-1 == (sock = socket(af, SOCK_DGRAM, 0))) {
		perror("socket()");
		return -1;
	}

	/* Set loopback of packets */
	if(-1 == setsockopt(sock, level, mcloop, (const char *)&loopback, sizeof(loopback))) {
		perror("setsockopt(IP_MULTICAST_LOOP)");
		close(sock);
		return -1;
	}

	/* Set multicast TTL */
	if(-1 == setsockopt(sock, level, mcttl, (const char *)&ttl, sizeof(ttl))) {
		perror("setsockopt(IP_MULTICAST_TTL)");
		close(sock);
		return -1;
	}
#if 0
	int mcdisc = af == AF_INET ? IP_MTU_DISCOVER : IPV6_MTU_DISCOVER;
	/* Set MTU discover to off */
	ttl = IP_PMTUDISC_DONT;
	if(-1 == setsockopt(sock, level, mcdisc, &ttl, sizeof(ttl))) {
		perror("setsockopt(IP_MTU_DISCOVER)");
		close(sock);
		return -1;
	}
#endif
	return sock;
}

/*
 * Callback functions
 */
static int srvcb(const struct sockaddr *sa, int salen, void *ptr)
{
	if(ptr && *(int *)ptr)
		printf("Found SRV record for %s\n", get_name(sa, 1));

	if(!find_mcaddrport(sa))
		add_mcaddrport(sa, salen, 0);
	return 1;
}

static const char usage_str[] =
	"Usage: pmdsend [options]\n"
	"Options:\n"
	"  -4|--ipv4                 Use only IPv4 (default both 4 and 6)\n"
	"  -6|--ipv6                 Use only IPv6 (default both 4 and 6)\n"
	"  -A|--defaultmc            Always send to default multicast addresses\n"
	"  -c|--category cat         Message category 'cat' (default local/0)\n"
	"  -C|--subcategory subcat   Message subcategory 'subcat' (default 0)\n"
	"  -d|--detination addrport  Add destination 'addrport' as mcaddr:port; IPv6 use [xx:xx::xx]:port\n"
	"  -D|--domain dom           Use domain name 'dom' for DNS queries (may be used multiple times)\n"
	"  -E|--enccrtfile enccrt    Start encryption with cert in 'enccrt'\n"
	"  -e|--encrypt              End of encrypted packet\n"
	"  -f|--certfile crtfile     Append certificate 'crtfile's sha-1 fingerprint\n"
	"  -F|--fraction num,den     Append a fraction message (num/den)\n"
	"  -h|--help                 This help message\n"
	"  -i|--interface ifc        Send message on interface 'ifc' (may be used multiple times; default all)\n"
	"  -I|--interval msecs       Repeat interval in milliseconds 'msecs' (default 1000ms)\n"
	"  -l|--nomcloopback         Disable loopback of MC packets\n"
	"  -L|--nolinklocal          Disable link local addresses\n"
	"  -m|--message msg          Append text message 'msg'\n"
	"  -M|--msguuid uuid         Set message's UUID to 'uuid'\n"
	"  -n|--nodns                Disable DNS server lookups\n"
	"  -N|--int int              Append a signed 32 bit number 'int'\n"
	"  -P|--pad n                Append 'n' padding octets with random value\n"
	"  -q|--silent               Set force silent flag\n"
	"  -r|--resource uri         Append resource 'uri'\n"
	"  -R|--repeat n             Repeat message 'n' times (default 1)\n"
	"  -s|--keyfile keyfile      Append signature from certificate 'keyfile's private key\n"
	"  -S|--severity sev         Set severity to 'sev'\n"
	"  -t|--timestamp time       Target timestamp 'time' (yyyy-mm-dd hh:mm:ss[.fff])\n"
	"  -T|--ttl ttl              Set the MC packet time-to-live to 'ttl'\n"
	"  -u|--uuid uuid            Append UUID 'uuid'\n"
	"  -U|--unsigned num         Append an unsigned 32 bit number 'num'\n"
	"  -v|--verbose              Be more verbose\n"
	"  -V|--version              Show version information\n"
	;


static const struct option lopts[] = {
	{ "ipv4",		no_argument,		NULL,	'4' },
	{ "ipv6",		no_argument,		NULL,	'6' },
	{ "defaultmc",		no_argument,		NULL,	'A' },
	{ "category",		required_argument,	NULL,	'c' },
	{ "subcategory",	required_argument,	NULL,	'C' },
	{ "destination",	required_argument,	NULL,	'd' },
	{ "domain",		required_argument,	NULL,	'D' },
	{ "enccrtfile",		required_argument,	NULL,	'E' },
	{ "encrypt",		no_argument,		NULL,	'e' },
	{ "certfile",		required_argument,	NULL,	'f' },
	{ "fraction",		required_argument,	NULL,	'F' },
	{ "help",		no_argument,		NULL,	'h' },
	{ "interface",		required_argument,	NULL,	'i' },
	{ "interval",		required_argument,	NULL,	'I' },
	{ "nomcloopback",	no_argument,		NULL,	'l' },
	{ "nolinklocal",	no_argument,		NULL,	'L' },
	{ "message",		required_argument,	NULL,	'm' },
	{ "msguuid",		required_argument,	NULL,	'M' },
	{ "nodns",		no_argument,		NULL,	'n' },
	{ "int",		required_argument,	NULL,	'N' },
	{ "pad",		required_argument,	NULL,	'P' },
	{ "silent",		no_argument,		NULL,	'q' },
	{ "resource",		required_argument,	NULL,	'r' },
	{ "repeat",		required_argument,	NULL,	'R' },
	{ "keyfile",		required_argument,	NULL,	's' },
	{ "severity",		required_argument,	NULL,	'S' },
	{ "timestamp",		required_argument,	NULL,	't' },
	{ "ttl",		required_argument,	NULL,	'T' },
	{ "uuid",		required_argument,	NULL,	'u' },
	{ "unsigned",		required_argument,	NULL,	'U' },
	{ "verbose",		no_argument,		NULL,	'v' },
	{ "version",		no_argument,		NULL,	'V' },
};

int main(int argc, char *argv[])
{
	int optc;
	int oidx = 0;
	int lose = 0;
	int i, j;
	char *eptr;
	uint32_t category = PMDC_LOCAL;
	uint32_t subcategory = 0;
	int haveencrypted = 0;
	int havecat = 0;
	int havesubcat = 0;
	int havetimestamp = 0;
	int pushhavecat = 0;
	int pushhavesubcat = 0;
	int pushhavetimestamp = 0;
	int32_t number;
	uint32_t unumber;
	int forcesilent = 0;
	int loopback = 1;
	unsigned repeat = 1;
	unsigned long repeatusecs = 1000*1000;
	unsigned severity = PMDS_INFO;
	char **iface = NULL;
	unsigned niface = 0;
	int ttl = 1;
	struct timeval tv;
	int *sock4list;
	int *sock6list;
	size_t nsock4;
	size_t nsock6;
	struct ifaddrs *ifr;
	struct ifaddrs *ifp;
	int nint;
	size_t pktlen;
	void *pkt;
	pmdp_msg_t *msg;
	pmdp_msg_t *pushmsg = NULL;
	pmdp_x509_crt_t *enccrt = NULL;
	pmdp_x509_crt_t *sigcrt = NULL;
	pmdp_x509_key_t *sigkey = NULL;
	int rv;
	unsigned sequence;
	uint8_t uuid[PMDP_UUID_SIZE];
	int useipv4 = 0;
	int useipv6 = 0;
	int nodns = 0;
	int usedefip = 0;
	int verbose = 0;
	int nolinklocal = 0;

	msg = pmdp_msg_new();

	while(EOF != (optc = getopt_long(argc, argv, "46Ac:C:d:D:eE:f:F:hi:I:lLm:M:nN:qP:r:R:s:S:t:T:u:U:vV", lopts, &oidx))) {
		switch(optc) {
		case '4':
			useipv4 = 1;
			break;
		case '6':
			useipv6 = 1;
			break;
		case 'A':
			usedefip = 1;
			break;
		case 'c':
			category = strtoul(optarg, &eptr, 0);
			if((eptr && *eptr)) {
				fprintf(stderr, "Invalid category\n");
				lose++;
			} else {
				if(0 != (rv = pmdp_msg_add_category(msg, category))) {
					fprintf(stderr, "Cannot add category (%d: %s)\n", rv, pmdp_strerror(rv));
					lose++;
				} else
					havecat = 1;
				if(-1 == havesubcat) {
					if(0 != (rv = pmdp_msg_add_subcategory(msg, subcategory))) {
						fprintf(stderr, "Cannot add subcategory (%d: %s)\n", rv, pmdp_strerror(rv));
						lose++;
					} else
						havesubcat = 1;
				}
			}
			break;
		case 'C':
			subcategory = strtoul(optarg, &eptr, 0);
			if(eptr && *eptr) {
				fprintf(stderr, "Invalid subcategory\n");
				lose++;
			} else {
				if(!havecat) {
					/* Save the subcat until we have a category */
					/* This will get the ordering right */
					havesubcat = -1;
				} else if(0 != (rv = pmdp_msg_add_subcategory(msg, subcategory))) {
					fprintf(stderr, "Cannot add subcategory (%d: %s)\n", rv, pmdp_strerror(rv));
					lose++;
				} else
					havesubcat = 1;
			}
			break;
		case 'd':
			eptr = xstrdup(optarg);
			if(eptr[0] == '[') {
				char *cptr;
				/* This should be an ipv6 address */
				if(!(cptr = strrchr(eptr, ']'))) {
					fprintf(stderr, "Missing ']' is IPv6 address\n");
					free(eptr);
					break;
				}
				*cptr++ = '\0';
				if((cptr = strchr(cptr, ':')))
					*cptr++ = '\0';
				else
					cptr = STRINGIZE_X(PMDP_PORT);
				if(!lookup_add_mcaddrport(eptr+1, cptr, 0))
					lose++;
			} else {
				char *cptr;
				if((cptr = strrchr(eptr, ':')))
					*cptr++ = '\0';
				else
					cptr = STRINGIZE_X(PMDP_PORT);
				if(!lookup_add_mcaddrport(eptr, cptr, 0))
					lose++;
			}
			free(eptr);
			break;
		case 'D':
			add_domname(optarg);
			break;
		case 'e':
			if(!enccrt || !pushmsg) {
				fprintf(stderr, "Cannot add encrypted without marking start\n");
				lose++;
				break;
			}
			if(0 != (rv = pmdp_msg_add_encrypted(pushmsg, msg, enccrt, PMDE_AES128_CBC))) {
				fprintf(stderr, "Cannot add encrypted content (%d: %s)\n", rv, pmdp_strerror(rv));
				lose++;
				break;
			}
			pmdp_msg_free(msg);
			msg = pushmsg;
			havecat = pushhavecat;
			havesubcat = pushhavesubcat;
			havetimestamp = pushhavetimestamp;
			pmdp_x509_crt_free(enccrt);
			enccrt = NULL;
			pushmsg = NULL;
			haveencrypted++;
			break;
		case 'E':
			if(pushmsg) {
				fprintf(stderr, "Nested encryption not supported (yet)\n");
				lose++;
				break;
			}
			if(!(enccrt = pmdp_x509_crt_load_file(optarg))) {
				fprintf(stderr, "%s: Failed to load encryption certificate\n", optarg);
				lose++;
				break;
			}
			pushmsg = msg;
			pushhavecat = havecat;
			pushhavesubcat = havesubcat;
			pushhavetimestamp = havetimestamp;
			if(!(msg = pmdp_msg_new())) {
				fprintf(stderr, "Cannot create new encrypted content\n");
				lose++;
			}
			break;
		case 'f':
			if(sigcrt) {
				fprintf(stderr, "Already loaded a signing certificate\n");
				lose++;
				break;
			}
			if(!(sigcrt = pmdp_x509_crt_load_file(optarg))) {
				fprintf(stderr, "%s: failed to load certificate\n", optarg);
				lose++;
			}
			break;
		case 'F':
			{
				char *num = xstrdup(optarg);
				char *denom;
				int32_t dividend;
				uint32_t divisor;
				if(!(denom = strchr(num, ','))) {
					fprintf(stderr, "Missing denominator\n");
					lose++;
					free(num);
					break;
				}
				*denom++ = 0;
				dividend = strtol(num, &eptr, 0);
				if(eptr && *eptr) {
					fprintf(stderr, "Invalid dividend\n");
					lose++;
					free(num);
					break;
				}
				divisor = strtoul(denom, &eptr, 0);
				if(eptr && *eptr) {
					fprintf(stderr, "Invalid divisor\n");
					lose++;
					free(num);
					break;
				}
				if(0 != (rv = pmdp_msg_add_fraction(msg, dividend, divisor))) {
					fprintf(stderr, "Cannot add fraction (%d: %s)\n", rv, pmdp_strerror(rv));
					lose++;
				}
				free(num);
			}
			break;
		case 'h':
			fprintf(stdout, "%s", usage_str);
			lose++;
			break;
		case 'i':
			if(!iface) {
				niface = 1;
				iface = xmalloc(sizeof(*iface));
			} else {
				niface++;
				iface = xrealloc(iface, sizeof(*iface) * niface);
			}
			if(!iface) {
				fprintf(stderr, "No memory\n");
				exit(1);
			}
			iface[niface-1] = xstrdup(optarg);
			break;
		case 'I':
			repeatusecs = strtoul(optarg, &eptr, 0);
			if((eptr && *eptr) || repeatusecs > 3600000) {
				fprintf(stderr, "Invalid repeat milliseconds or more than an hour\n");
				lose++;
			}
			repeatusecs *= 1000;	/* cmdline is in millisecs, we want microsecs */
			break;
		case 'l':
			loopback = 0;
			break;
		case 'L':
			nolinklocal = 1;
			break;
		case 'm':
			if(0 != (rv = pmdp_msg_add_text(msg, optarg))) {
				fprintf(stderr, "Invalid message text (%d: %s)\n", rv, pmdp_strerror(rv));
				lose++;
			}
			break;
		case 'M':
			if(-1 == pmdp_uuid_parse(optarg, uuid)) {
				fprintf(stderr, "Invalid message UUID\n");
				lose++;
				break;
			}
			memcpy(msg->uuid, uuid, sizeof(msg->uuid));
			break;
		case 'n':
			nodns = 1;
			break;
		case 'N':
			number = strtol(optarg, &eptr, 0);
			if(eptr && *eptr) {
				fprintf(stderr, "Invalid number\n");
				lose++;
			} else {
				if(0 != (rv = pmdp_msg_add_int32(msg, number))) {
					fprintf(stderr, "Cannot add signed number (%d: %s)\n", rv, pmdp_strerror(rv));
					lose++;
				}
			}
			break;
		case 'P':
			i = strtol(optarg, &eptr, 0);
			if((eptr && *eptr) || i < 0 || i > 65488) {
				fprintf(stderr, "More than 65488 bytes padding not supported\n");
				lose++;
				break;
			}
			if(0 != (rv = pmdp_msg_add_padding(msg, NULL, i))) {
				fprintf(stderr, "Cannot add padding (%d: %s)\n", rv, pmdp_strerror(rv));
				lose++;
			}
			break;
		case 'q':
			forcesilent = 1;
			break;
		case 'r':
			if(0 != (rv = pmdp_msg_add_resource(msg, optarg))) {
				fprintf(stderr, "Invalid message resource (%d: %s)\n", rv, pmdp_strerror(rv));
				lose++;
			}
			break;
		case 'R':
			repeat = strtoul(optarg, &eptr, 0);
			if((eptr && *eptr) || repeat > (1<<4)) {
				fprintf(stderr, "Invalid repeat\n");
				lose++;
			}
			break;
		case 's':
			if(sigkey) {
				fprintf(stderr, "Already loaded a signing key\n");
				lose++;
				break;
			}
			if(!(sigkey = pmdp_x509_key_load_file(optarg, NULL, NULL))) {
				fprintf(stderr, "%s: failed to load key\n", optarg);
				lose++;
			}
			break;
		case 'S':
			severity = strtoul(optarg, &eptr, 0);
			if((eptr && *eptr) || severity > PMDS_DEBUG) {
				fprintf(stderr, "Invalid severity\n");
				lose++;
			} else
				pmdp_msg_set_severity(msg, severity);
			break;
		case 't':
			if(!parse_time(optarg, &tv)) {
				fprintf(stderr, "Invalid timestamp\n");
				lose++;
			} else {
				if(0 != (rv = pmdp_msg_add_timestamp(msg, &tv))) {
					fprintf(stderr, "Cannot add timestamp (%d: %s)\n", rv, pmdp_strerror(rv));
					lose++;
				} else
					havetimestamp++;
			}
			break;
		case 'T':
			ttl = strtol(optarg, &eptr, 10);
			if((eptr && *eptr) || ttl < 1 || ttl > 255) {
				fprintf(stderr, "Invalid TTL\n");
				lose++;
			}
			break;
		case 'u':
			if(-1 == pmdp_uuid_parse(optarg, uuid)) {
				fprintf(stderr, "Invalid UUID\n");
				lose++;
				break;
			}
			if(0 != (rv = pmdp_msg_add_uuid(msg, uuid))) {
				fprintf(stderr, "Cannot add UUID (%d: %s)\n", rv, pmdp_strerror(rv));
				lose++;
			}
			break;
		case 'U':
			unumber = strtoul(optarg, &eptr, 0);
			if(eptr && *eptr) {
				fprintf(stderr, "Invalid number\n");
				lose++;
			} else {
				if(0 != (rv = pmdp_msg_add_uint32(msg, unumber))) {
					fprintf(stderr, "Cannot add unsigned number (%d: %s)\n", rv, pmdp_strerror(rv));
					lose++;
				}
			}
			break;
		case 'v':
			verbose++;
			break;
		case 'V':
			printf("PingMyDroid(tm) sender version %s\n", PACKAGE_VERSION);
			exit(0);
			break;
		default:
			lose++;
			break;
		}
	}

	if(verbose)
		setvbuf(stdout, NULL, _IOLBF, 0);

	if(!useipv4 && !useipv6)
		useipv4 = useipv6 = 1;

	if(!haveencrypted && !havecat) {
		if(verbose) {
			fprintf(stdout, "Adding default category %u\n", category);
			fflush(stdout);
		}
		if(0 != (rv = pmdp_msg_add_category(msg, category))) {
			fprintf(stderr, "Cannot add category (%d: %s)\n", rv, pmdp_strerror(rv));
			lose++;
		}
	}
	if(!haveencrypted && havesubcat <= 0) {
		if(verbose) {
			fprintf(stdout, "Adding default subcategory %u\n", subcategory);
			fflush(stdout);
		}
		if(0 != (rv = pmdp_msg_add_subcategory(msg, subcategory))) {
			fprintf(stderr, "Cannot add subcategory (%d: %s)\n", rv, pmdp_strerror(rv));
			lose++;
		}
	}

	if(enccrt) {
		fprintf(stderr, "Encrypted packet not terminated\n");
		lose++;
	}

	if(lose)
		exit(1);

	pmdp_msg_set_flags(msg, forcesilent ? PMDF_FORCESILENT : 0);

	if(0 != (rv = pmdp_msg_generate(msg, sigcrt, sigkey, &pkt, &pktlen))) {
		fprintf(stderr, "Message generation failed (%d: %s)\n", rv, pmdp_strerror(rv));
		exit(1);
	}

	add_local_domnames(nodns, verbose, nolinklocal, useipv4, useipv6);

	/* Add MC addresses from SRV records */
	if(ndomname && !nodns) {
		for(i = 0; i < ndomname; i++) {
			if(verbose > 1) {
				fprintf(stdout, "Performing DNS SRV lookup for '%s'\n", domname[i]);
				fflush(stdout);
			}
			int err = pmdp_dns_srv_by_name(domname[i], srvcb, &verbose);
			if(err && err != PMDP_ERR_NOTFOUND)
				fprintf(stderr, "SRV lookup for '%s': %s\n", domname[i], pmdp_strerror(err));
		}
	}

	if(verbose && ndomname) {
		for(i = 0; i < ndomname; i++)
			fprintf(stdout, "Bound to domain: %s\n", domname[i]);
		fflush(stdout);
	}

	if(usedefip || !nmcaddrport) {
		/* If we do not yet have any listen addresses, add the defaults */
		if(useipv4 && !lookup_add_mcaddrport(PMDP_MCADDR, STRINGIZE_X(PMDP_PORT), 0))
			exit(1);
		if(useipv6 && !lookup_add_mcaddrport(PMDP_MCADDR6, STRINGIZE_X(PMDP_PORT), 0))
			exit(1);
		if(!nmcaddrport) {
			/* We may still fail, unlikely, but possible */
			fprintf(stderr, "No target IP addresses to send to\n");
			exit(1);
		}
	}

	if(verbose) {
		for(i = 0; i < nmcaddrport; i++)
			fprintf(stdout, "Found target address: %s\n", get_name(&mcaddrport[i].sa, 1));
		fflush(stdout);
	}

	/* Get all interfaces on this host */
	if(getifaddrs(&ifr)) {
		perror("getifaddrs()");
		exit(1);
	}

	/* Count the list of interfaces */
	for(nint = 0, ifp = ifr; ifp; nint++, ifp = ifp->ifa_next)
		;

	sock4list = xmalloc(sizeof(*sock4list) * nint);
	sock6list = xmalloc(sizeof(*sock6list) * nint);
	nsock4 = nsock6 = 0;
	/* Now get sockets for all interfaces we are interested in */
	for(ifp = ifr; ifp; ifp = ifp->ifa_next) {
		int ix;
		/* We only want inet/inet6 interfaces */
		if(AF_INET != ifp->ifa_addr->sa_family && AF_INET6 != ifp->ifa_addr->sa_family)
			continue;
		/* Did the cmdline enable the interface (or IP address of interface)? */
		if(niface) {
			unsigned j;
			int found = 0;
			for(j = 0; j < niface; j++) {
				if(!strcmp(iface[j], ifp->ifa_name) || !strcasecmp(iface[j], get_name(ifp->ifa_addr, 0))) {
					found = 1;
					break;
				}
			}
			if(!found)
				continue;
		}

		if(!(ix = if_nametoindex(ifp->ifa_name))) {
			fprintf(stderr, "Warning: Cannot get ifindex of %s (%s)\n", ifp->ifa_name, get_name(ifp->ifa_addr, 0));
			continue;
		}
		if(useipv4 && AF_INET == ifp->ifa_addr->sa_family) {
#ifdef HAVE_IP_MREQN
			struct ip_mreqn mr;
#else
			struct ip_mreq mr;
#endif
			if(nolinklocal && IN_IS_ADDR_LINKLOCAL(&((const struct sockaddr_in *)ifp->ifa_addr)->sin_addr))
				continue;

			if(-1 == (sock4list[nsock4] = get_sock(AF_INET, loopback, ttl))) {
				fprintf(stderr, "No IPv4 socket available for if-addr '%s'\n", get_name(ifp->ifa_addr, 0));
				continue;
			}
			/* Bind the socket to the interface address */
			((struct sockaddr_in *)ifp->ifa_addr)->sin_port = 0;
			if(-1 == bind(sock4list[nsock4], ifp->ifa_addr, sizeof(struct sockaddr_in))) {
				perror("bind(IPv4)");
				close(sock4list[nsock4]);
				continue;
			}
			/* Set source interface */
			memset(&mr, 0, sizeof(mr));
#ifdef HAVE_IP_MREQN
			mr.imr_ifindex = ix;
#else
			mr.imr_interface = ((const struct sockaddr_in *)ifp->ifa_addr)->sin_addr;
#endif
			if(-1 == setsockopt(sock4list[nsock4], IPPROTO_IP, IP_MULTICAST_IF, &mr, sizeof(mr))) {
				perror("setsockopt(IP_MULTICAST_IF)");
				close(sock4list[nsock4]);
				continue;
			}
			nsock4++;
			if(verbose) {
				fprintf(stdout, "Send socket on interface %s, local address: %s\n", ifp->ifa_name, get_name(ifp->ifa_addr, 0));
				fflush(stdout);
			}
		}
		if(useipv6 && AF_INET6 == ifp->ifa_addr->sa_family) {
			if(nolinklocal && IN6_IS_ADDR_LINKLOCAL(&((const struct sockaddr_in6 *)ifp->ifa_addr)->sin6_addr))
				continue;

			if(-1 == (sock6list[nsock6] = get_sock(AF_INET6, loopback, ttl))) {
				fprintf(stderr, "No IPv6 socket available for if-addr '%s'\n", get_name(ifp->ifa_addr, 0));
				continue;
			}
			if(-1 == bind(sock6list[nsock6], ifp->ifa_addr, sizeof(struct sockaddr_in6))) {
				perror("bind(IPv6)");
				close(sock6list[nsock6]);
				continue;
			}

			/* Set source interface */
			if(-1 == setsockopt(sock6list[nsock6], IPPROTO_IPV6, IPV6_MULTICAST_IF, (const char *)&ix, sizeof(ix))) {
				perror("setsockopt(IPV6_MULTICAST_IF)");
				close(sock6list[nsock6]);
				continue;
			}
			nsock6++;
			if(verbose) {
				fprintf(stdout, "Send socket on interface %s, local address: %s\n", ifp->ifa_name, get_name(ifp->ifa_addr, 0));
				fflush(stdout);
			}
		}
	}

	if(!nsock4 && !nsock6) {
		fprintf(stderr, "No interface left or available to send on\n");
		exit(1);
	}

	sequence = 0;
	for(;repeat; repeat--) {
		ssize_t nsent;
		struct timeval tv1, tv2;
		gettimeofday(&tv1, NULL);
		for(i = 0; i < nsock4; i++) {
			/* Send a packet */
			for(j = 0; j < nmcaddrport; j++) {
				struct sockaddr_in s;
				socklen_t sl;

				if(mcaddrport[j].sa.sa_family != AF_INET)
					continue;
				/*
				 * Take care of special case:
				 * IPv4 cannot send 127.0.0.1 -> multicast, it must be 127.0.0.1 -> 127-0.0.1
				 * If we send anytging else than target 127.0.0.1 to loopback then we get network unreachable.
				 */
				sl = sizeof(s);
				getsockname(sock4list[i], (struct sockaddr *)&s, &sl);
				if((IN_IS_ADDR_LOOPBACK(&s.sin_addr) && !IN_IS_ADDR_LOOPBACK(&mcaddrport[j].sa4.sin_addr))
				|| (!IN_IS_ADDR_LOOPBACK(&s.sin_addr) && IN_IS_ADDR_LOOPBACK(&mcaddrport[j].sa4.sin_addr)))
					continue;
				if((nsent = sendto(sock4list[i], pkt, pktlen, 0, &mcaddrport[j].sa, sizeof(mcaddrport[j].sa4))) < 0) {
					struct sockaddr_in s;
					socklen_t sl = sizeof(s);
					getsockname(sock4list[i], (struct sockaddr *)&s, &sl);
					fprintf(stderr, "%s", get_name((struct sockaddr *)&s, 1));
					fprintf(stderr, " -> %s: ", get_name(&mcaddrport[j].sa, 1));
					perror("sendto(IPv4)");
				}
				if(verbose) {
					struct sockaddr_in s;
					socklen_t sl = sizeof(s);
					getsockname(sock4list[i], (struct sockaddr *)&s, &sl);
					fprintf(stdout, "Send packet sequence %u, size %d (%u), %s", sequence, (int)nsent, (unsigned)pktlen, get_name((struct sockaddr *)&s, 1));
					fprintf(stdout, " -> %s\n", get_name(&mcaddrport[j].sa, 1));
					fflush(stdout);
				}
			}
		}
		for(i = 0; i < nsock6; i++) {
			/* Send a packet */
			for(j = 0; j < nmcaddrport; j++) {
				struct sockaddr_in6 s;
				socklen_t sl;

				if(mcaddrport[j].sa.sa_family != AF_INET6)
					continue;
				/*
				 * Take care of special case:
				 * IPv6 cannot send ::1 -> multicast, it must be ::1 -> ::1
				 * If we send anything else than ::1 to loopback then we get network unreachable.
				 */
				sl = sizeof(s);
				getsockname(sock6list[i], (struct sockaddr *)&s, &sl);
				if((IN6_IS_ADDR_LOOPBACK(&s.sin6_addr) && !IN6_IS_ADDR_LOOPBACK(&mcaddrport[j].sa6.sin6_addr))
				|| (!IN6_IS_ADDR_LOOPBACK(&s.sin6_addr) && IN6_IS_ADDR_LOOPBACK(&mcaddrport[j].sa6.sin6_addr)))
					continue;
				if((nsent = sendto(sock6list[i], pkt, pktlen, 0, &mcaddrport[j].sa, sizeof(mcaddrport[j].sa6))) < 0) {
					fprintf(stderr, "%s", get_name((struct sockaddr *)&s, 1));
					fprintf(stderr, " -> %s: ", get_name(&mcaddrport[j].sa, 1));
					perror("sendto(IPv6)");
				}
				if(verbose) {
					fprintf(stdout, "Send packet sequence %u, size %d (%u), %s", sequence, (int)nsent, (unsigned)pktlen, get_name((struct sockaddr *)&s, 1));
					fprintf(stdout, " -> %s\n", get_name(&mcaddrport[j].sa, 1));
					fflush(stdout);
				}
			}
		}
		if(repeat > 1 && repeatusecs > 0) {
			struct timespec ts, tsx;
			unsigned long difftime;
			/* Compensate approx for the time spent sending */
			gettimeofday(&tv2, NULL);
			tv2.tv_sec -= tv1.tv_sec;
			if(tv2.tv_usec < tv1.tv_usec) {
				tv2.tv_usec -= tv1.tv_usec;
				tv2.tv_usec += 1000000;
				tv2.tv_sec--;
			} else
				tv2.tv_usec -= tv1.tv_usec;
			difftime = 100000 * tv2.tv_sec + tv2.tv_usec;
			if(difftime < repeatusecs) {
				ts.tv_sec = (repeatusecs - difftime)/ 1000000;
				ts.tv_nsec = ((long)(repeatusecs - difftime) % 1000000L) * 1000L;
retry_sleep:
				if(-1 == nanosleep(&ts, &tsx)) {
					if(errno == EINTR) {
						ts = tsx;
						goto retry_sleep;
					}
				}
			}
		}
		pmdp_msg_set_sequence(msg, ++sequence);
		if(havetimestamp) {
			/* Update the header timestamp if the message contains a timestamp */
			gettimeofday(&tv, NULL);
			pmdp_msg_set_timestamp(msg, &tv);
		}
	}

	return 0;
}
