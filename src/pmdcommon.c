/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef __WIN32__
#include <ws2tcpip.h>
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif

#ifdef HAVE_NETDB_H
#include <netdb.h>
#endif

#ifdef HAVE_IFADDRS_H
#include <ifaddrs.h>
#endif

#ifdef HAVE_NET_IF_H
#include <net/if.h>
#endif

#include <pmdcommon.h>
#include <pmdp_compat.h>

mcaddrport_t *mcaddrport;	/* List of addresses/ports we use to send/receive */
int nmcaddrport;

char **domname;			/* List of domain names we bind to */
unsigned ndomname;

ifgrpifs_t *ifgrpifs;		/* List of interface indices that have a family of addresses */
unsigned nifgrpifs;

/*
 * Allocation errors are fatal; no need to check for NULL all over the place
 * when we cannot recover anyway. It sucks, but that is life.
 */
void *xmalloc(size_t s)
{
	void *p;
	assert(s > 0);
	if(!(p = malloc(s))) {
		fprintf(stderr, "error: no memory in malloc\n");
		exit(1);
	}
	return p;
}

void *xrealloc(void *op, size_t s)
{
	void *p;
	assert(s > 0);
	if(!(p = realloc(op, s))) {
		fprintf(stderr, "error: no memory in realloc\n");
		exit(1);
	}
	return p;
}

char *xstrdup(const char *s)
{
	char *p;
	assert(s != NULL);
	if(!(p = strdup(s))) {
		fprintf(stderr, "error: no memory in xstrdup\n");
		exit(1);
	}
	return p;
}

/*
 * Resolve an address to a name
 */
const char *get_name(const struct sockaddr *sa, int showport)
{
	static char hbuf[NI_MAXHOST + 16];
	char port[16];
	switch(sa->sa_family) {
	case AF_INET:
		if(getnameinfo(sa, sizeof(struct sockaddr_in), hbuf, sizeof(hbuf)-16, NULL, 0, NI_NUMERICHOST))
			strcpy(hbuf, "?ipv4?");
		if(showport) {
			sprintf(port, ":%u", ntohs(((struct sockaddr_in *)sa)->sin_port));
			strcat(hbuf, port);
		}
		break;
	case AF_INET6:
		if(showport) {
			hbuf[0] = '[';
			if(getnameinfo(sa, sizeof(struct sockaddr_in6), hbuf+1, sizeof(hbuf)-17, NULL, 0, NI_NUMERICHOST))
				strcpy(hbuf, "[?ipv6?]");
			sprintf(port, "]:%u", ntohs(((struct sockaddr_in6 *)sa)->sin6_port));
			strcat(hbuf, port);
		} else {
			if(getnameinfo(sa, sizeof(struct sockaddr_in6), hbuf, sizeof(hbuf), NULL, 0, NI_NUMERICHOST))
				strcpy(hbuf, "?ipv6?");
		}
		break;
	default:
		strcpy(hbuf, "Invalid address family in get_name()");
		break;
	}
	return hbuf;
}

/*
 * Handle listen addresses and ports for incoming packets
 */
mcaddrport_t *find_mcaddrport(const struct sockaddr *sa)
{
	int i;
	for(i = 0; i < nmcaddrport; i++) {
		if(sa->sa_family != mcaddrport[i].sa.sa_family)
			continue;
		switch(sa->sa_family) {
		case AF_INET:
			if(((struct sockaddr_in *)sa)->sin_port == mcaddrport[i].sa4.sin_port
			  && !memcmp(&((struct sockaddr_in *)sa)->sin_addr, &mcaddrport[i].sa4.sin_addr, sizeof(mcaddrport[i].sa4.sin_addr)))
				return &mcaddrport[i];
			break;
		case AF_INET6:
			if(((struct sockaddr_in6 *)sa)->sin6_port == mcaddrport[i].sa6.sin6_port
			  && !memcmp(&((struct sockaddr_in6 *)sa)->sin6_addr, &mcaddrport[i].sa6.sin6_addr, sizeof(mcaddrport[i].sa6.sin6_addr)))
				return &mcaddrport[i];
			break;
		}
	}
	return NULL;
}

mcaddrport_t *add_mcaddrport(const struct sockaddr *sa, size_t salen, int ismc)
{
	if(ismc) {
		if((sa->sa_family == AF_INET && !IN_MULTICAST(ntohl(((struct sockaddr_in *)sa)->sin_addr.s_addr)))
		|| (sa->sa_family == AF_INET6 && !IN6_IS_ADDR_MULTICAST(((struct sockaddr_in6 *)sa)->sin6_addr.s6_addr))) {
			fprintf(stderr, "Warning: %s: address not multicast, ignored\n", get_name(sa, 0));
			return NULL;
		}
	}
	if(!mcaddrport) {
		mcaddrport = xmalloc(sizeof(*mcaddrport));
		nmcaddrport = 1;
	} else {
		nmcaddrport++;
		mcaddrport = xrealloc(mcaddrport, nmcaddrport * sizeof(*mcaddrport));
	}
	memcpy(&mcaddrport[nmcaddrport-1], sa, salen);
	return &mcaddrport[nmcaddrport-1];
}

void free_mcaddrport(void)
{
	if(mcaddrport)
		free(mcaddrport);
}

int lookup_add_mcaddrport(const char *name, const char *port, int ismc)
{
	struct addrinfo hints;
	struct addrinfo *ai;
	struct addrinfo *aip;
	int res;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;
	if((res = getaddrinfo(name, port, &hints, &ai))) {
		fprintf(stderr, "Cannot resolv %s port %s: %s\n", name, port, gai_strerror(res));
		return 0;
	}
	for(aip = ai; aip; aip = aip->ai_next) {
		if(aip->ai_addr->sa_family != AF_INET && aip->ai_addr->sa_family != AF_INET6)
			continue;
		if(find_mcaddrport(aip->ai_addr))
			continue;
		add_mcaddrport(aip->ai_addr, aip->ai_addrlen, ismc);
	}
	return 1;
}

void add_domname(const char *dname)
{
	unsigned i;

	/* If we know it, don't add it */
	for(i = 0; i < ndomname; i++) {
		if(!strcasecmp(domname[i], dname))
			return;
	}

	if(!domname)
		domname = xmalloc(sizeof(*domname));
	else
		domname = xrealloc(domname, (ndomname+1)*sizeof(*domname));
	domname[ndomname++] = xstrdup(dname);
}

void free_domname(void)
{
	unsigned i;
	for(i = 0; i < ndomname; i++)
		free(domname[i]);
	if(ndomname)
		free(domname);
}

static void add_ifgrpif(unsigned family, unsigned ifidx, const struct in_addr *addr)
{
	unsigned i;
	for(i = 0; i < nifgrpifs; i++) {
		if(ifgrpifs[i].family == family && ifgrpifs[i].ifidx == ifidx)
			return;
	}
	if(!ifgrpifs)
		ifgrpifs = xmalloc(sizeof(*ifgrpifs));
	else
		ifgrpifs = xrealloc(ifgrpifs, (nifgrpifs+1)*sizeof(*ifgrpifs));
	ifgrpifs[nifgrpifs].family = family;
	if(addr)
		ifgrpifs[nifgrpifs].addr = *addr;
	ifgrpifs[nifgrpifs].ifidx = ifidx;
	nifgrpifs++;
}

void add_local_domnames(int nodns, int verbose, int nolinklocal, int useipv4, int useipv6)
{
	struct ifaddrs *ifp, *ifa;

	if(-1 == getifaddrs(&ifp))
		return;
	for(ifa = ifp; ifa; ifa = ifa->ifa_next) {
		socklen_t salen;
		char hname[1025+1];
		size_t sl;
		char *cptr;
		unsigned ix;

		if(!(ix = if_nametoindex(ifa->ifa_name))) {
			fprintf(stderr, "Warning: Cannot get ifindex of %s (%s)\n", ifa->ifa_name, get_name(ifa->ifa_addr, 0));
			continue;
		}

		if(useipv4 && ifa->ifa_addr->sa_family == AF_INET) {
			if(nolinklocal && IN_IS_ADDR_LINKLOCAL(&((const struct sockaddr_in *)ifa->ifa_addr)->sin_addr))
				continue;
			if(ifa->ifa_flags & IFF_MULTICAST)
				add_ifgrpif(AF_INET, ix, &((const struct sockaddr_in *)ifa->ifa_addr)->sin_addr);
			salen = sizeof(struct sockaddr_in);
		} else if(useipv6 && ifa->ifa_addr->sa_family == AF_INET6) {
			if(nolinklocal && IN6_IS_ADDR_LINKLOCAL(&((const struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr))
				continue;
			if(ifa->ifa_flags & IFF_MULTICAST)
				add_ifgrpif(AF_INET6, ix, NULL);
			salen = sizeof(struct sockaddr_in6);
		} else
			continue;

		if(!nodns) {
			if(verbose) {
				fprintf(stdout, "Found local address '%s', performing lookup", get_name(ifa->ifa_addr, 0));
				fflush(stdout);
			}
			if(0 != getnameinfo(ifa->ifa_addr, salen, hname, sizeof(hname)-1, NULL, 0, NI_NAMEREQD))
				goto skipif;
			sl = strlen(hname);
			if(hname[sl-1] != '.')			/* If we do not have an absolute FQDN, then make it absolute */
				strcat(hname, ".");
			if(!(cptr = strchr(hname, '.')))	/* We must have at least a '.' somewhere */
				goto skipif;
			if(!strchr(cptr+1, '.'))		/* If there is only one '.', then we have no domain */
				goto skipif;

			add_domname(cptr+1);
			if(verbose)
				fprintf(stdout, " --> %s", cptr+1);

		skipif:
			if(verbose)
				fprintf(stdout, "\n");
		}
	}
	freeifaddrs(ifp);
}

