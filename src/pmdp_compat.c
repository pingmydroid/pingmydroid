/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2014  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>
#include <time.h>

#include <pmdp_compat.h>

#ifdef HAVE_NANOSLEEP
int nanosleep(const struct timespec *req, struct timespec *rem)
{
#ifdef HAVE_USLEEP
	usleep(req->tv_sec * 1000000 + req->tv_nsec / 1000);
#else
#warning "No nanosleep and no usleep available, not waiting at all"
#endif
	return 0;
}
#endif

#ifndef HAVE_GETIFADDRS
int getifaddrs(struct ifaddrs **ifap)
{
	// FIXME...
	(void)ifap;
	return -1;
}

void freeifaddrs(struct ifaddrs *ifa)
{
	// FIXME...
	(void)ifa;
}

#endif
