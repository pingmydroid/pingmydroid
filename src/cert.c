/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cert.h"

#define NOISY	1

#define MAX_BUNDLE_CERTS	1024

void crt_init(void)
{
	ERR_load_crypto_strings();
	ERR_clear_error();
}

static char *load_file(const char *fname, size_t *size)
{
	FILE *fp;
	char *buf;
	long fsize;

	if(!fname || !size)
		return NULL;
	if(!(fp = fopen(fname, "r")))
		return NULL;
	if(-1 == fseek(fp, 0, SEEK_END)) {
		fclose(fp);
		return NULL;
	}
	if((fsize = ftell(fp)) < 0) {
		fclose(fp);
		return NULL;
	}
	if(-1 == fseek(fp, 0, SEEK_SET)) {
		fclose(fp);
		return NULL;
	}
	if(!(buf = malloc(fsize))) {
		fclose(fp);
		return NULL;
	}
	if(fsize != fread(buf, 1, (size_t)fsize, fp)) {
		free(buf);
		fclose(fp);
		return NULL;
	}
	fclose(fp);
	*size = (unsigned int)fsize;
	return buf;
}

x509_crt_t crt_crt_load_bin(const char *buf, size_t bufsize)
{
	BIO *bio;
	X509 *crt;

	if(!(bio = BIO_new_mem_buf((void *)buf, bufsize)))
		return NULL;
	crt = d2i_X509_bio(bio, NULL);
	BIO_free(bio);
	return crt;
}

x509_crt_t crt_crt_load(const char *buf, size_t bufsize)
{
	BIO *bio;
	X509 *crt;

	if(!(bio = BIO_new_mem_buf((void *)buf, bufsize)))
		return NULL;
	crt = PEM_read_bio_X509(bio, NULL, 0, NULL);
	BIO_free(bio);
	return crt;
}

x509_crt_t crt_crt_load_file(const char *fname)
{
	char *buf;
	size_t size;
	x509_crt_t crt;
	if(!(buf = load_file(fname, &size)))
		return NULL;
	crt = crt_crt_load(buf, size);
	free(buf);
	return crt;
}

void crt_crt_free(x509_crt_t crt)
{
	X509_free(crt);
}

int crt_crt_fingerprint(x509_crt_t crt, void *buf, size_t *bufsize)
{
	unsigned n = *bufsize;
	int res;
	res = X509_digest(crt, EVP_sha1(), buf, &n);
	*bufsize = n;
	return 0 == res ? -1 : 0;
}

int crt_crt_verify(x509_crt_t crt, const void *data, size_t datalen, void *sig, size_t siglen)
{
	EVP_PKEY *pubkey;
	EVP_MD_CTX md_ctx;
	int res;

	if(!(pubkey = X509_get_pubkey(crt))) {
#ifdef NOISY
		ERR_print_errors_fp(stderr);
#endif
		return -1;
	}
	EVP_MD_CTX_init(&md_ctx);
	EVP_VerifyInit(&md_ctx, EVP_sha1());
	EVP_VerifyUpdate(&md_ctx, data, datalen);
	res = EVP_VerifyFinal(&md_ctx, sig, siglen, pubkey);
	EVP_PKEY_free(pubkey);
	EVP_MD_CTX_cleanup(&md_ctx);
	return res == 1 ? 0 : -1;
}

x509_crt_t *crt_bundle_load(const char *buf, size_t bufsize, size_t *nlist)
{
	BIO *bio;
	X509 *crt;
	X509 **pcrt = NULL;
	size_t n = 0;

	if(!(bio = BIO_new_mem_buf((void *)buf, bufsize)))
		return NULL;
	while(1) {
		if(!(crt = PEM_read_bio_X509(bio, NULL, 0, NULL))) {
			ERR_clear_error();
			break;
		}
		if(!(pcrt = realloc(pcrt, sizeof(*pcrt)*(n+1))))
			break;
		pcrt[n++] = crt;
	}
	BIO_free(bio);
	*nlist = n;
	return pcrt;
}

x509_crt_t *crt_bundle_load_file(const char *fname, size_t *nlist)
{
	char *buf;
	size_t size;
	x509_crt_t *pcrt;
	if(!(buf = load_file(fname, &size)))
		return NULL;
	pcrt = crt_bundle_load(buf, size, nlist);
	free(buf);
	return pcrt;
}

x509_privkey_t crt_privkey_load(const char *buf, size_t bufsize)
{
	BIO *bio;
	EVP_PKEY *pkey;

	if(!(bio = BIO_new_mem_buf((void *)buf, bufsize)))
		return NULL;
	pkey = PEM_read_bio_PrivateKey(bio, NULL, 0, NULL);
	BIO_free(bio);
	return pkey;
}

x509_privkey_t crt_privkey_load_file(const char *fname)
{
	char *buf;
	size_t size;
	x509_privkey_t pkey;
	if(!(buf = load_file(fname, &size)))
		return NULL;
	pkey = crt_privkey_load(buf, size);
	free(buf);
	return pkey;
}

void crt_privkey_free(x509_privkey_t pkey)
{
	EVP_PKEY_free(pkey);
}

int crt_privkey_sign(x509_privkey_t pkey, const void *data, size_t datalen, void *sig, size_t *siglen)
{
	EVP_MD_CTX md_ctx;
	unsigned n = *siglen;
	EVP_MD_CTX_init(&md_ctx);
	EVP_SignInit(&md_ctx, EVP_sha1());
	EVP_SignUpdate(&md_ctx, data, datalen);
	if(-1 == EVP_SignFinal(&md_ctx, sig, &n, pkey)) {
#ifdef NOISY
		ERR_print_errors_fp(stderr);
#endif
		EVP_MD_CTX_cleanup(&md_ctx);
		*siglen = n;
		return -1;
	}
	EVP_MD_CTX_cleanup(&md_ctx);
	*siglen = n;
	return 0;
}
