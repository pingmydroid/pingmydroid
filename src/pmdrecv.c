/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2011,2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef __WIN32__
#include <ws2tcpip.h>
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <net/if.h>
#include <assert.h>
#include <errno.h>
#include <time.h>
#include <signal.h>
#include <poll.h>

#include <netdb.h>
#include <arpa/nameser.h>
#include <resolv.h>

#ifdef HAVE_LIBCURL
#include <curl/curl.h>
#endif

#include <pmdp.h>
#include <pmdcommon.h>

//#define FREEONEXIT	1				/* Valgrind testing */

#define PASTPACKETTTL	120				/* Packets seen are tracked this long */
#define PASTPACKETALLOC	16				/* We start to track this max packets; extends dynamically */

volatile int quit = 0;

/* This union prevents "strict aliasing" warnings */
typedef union __sa_any_t{
	struct sockaddr sa;
	struct sockaddr_in sa4;
	struct sockaddr_in6 sa6;
} sa_any_t;

typedef struct __pastpacket_t {
	int		ttl;
	uint8_t		uuid[PMDP_UUID_SIZE];
} pastpacket_t;

static pastpacket_t *pastpackets;		/* Received packets tracking for duplicate elimination */
static int npastpackets;

/* In-memory certificate store for signatures and decryption */
typedef struct __x509cert_t {
	pmdp_x509_crt_t	*cert;
	uint8_t		fingerprint[PMDP_MAX_HASHSIZE];
	size_t		fpsize;
	pmdp_x509_key_t	*pkey;
} x509cert_t;

static x509cert_t *x509certs;
static int nx509certs;

/* List of unique ports we need to listen on */
typedef struct {
	int		af;
	unsigned short	port;
} portlist_t;

static portlist_t *portlist;	/* List of unique listen ports */
static int nportlist;

static int partcb(const pmdp_msg_t *msg, const pmdp_msg_part_t *part, void *ptr);

/*
 * Certificate handing functions
 */
/* Add a certificate(+key) to the store */
static int add_cert(pmdp_x509_crt_t *x509, pmdp_x509_key_t *pkey)
{
	int i;
	x509cert_t cert;
	uint8_t *fp;

	cert.cert = x509;
	cert.pkey = pkey;
	cert.fpsize = sizeof(cert.fingerprint);
	if(pmdp_x509_fingerprint(x509, &fp, &cert.fpsize)) {
		fprintf(stderr, "Certificate fingerprint failed\n");
		return -1;
	}
	memcpy(cert.fingerprint, fp, cert.fpsize);
	free(fp);
	for(i = 0; i < nx509certs; i++) {
		x509cert_t *c = &x509certs[i];
		if(c->fpsize == cert.fpsize && !memcmp(c->fingerprint, cert.fingerprint, cert.fpsize)) {
			if(pkey && !c->pkey) {
				c->pkey = pkey;
				return i;
			}
			/*fprintf(stderr, "Duplicate certificate ignored\n");*/
			return -1;
		}
	}
	if(!x509certs) {
		x509certs = xmalloc(sizeof(*x509certs));
		nx509certs = 1;
	} else {
		nx509certs++;
		x509certs = xrealloc(x509certs, nx509certs * sizeof(*x509certs));
	}
	x509certs[nx509certs-1] = cert;
	return nx509certs - 1;
}

#ifdef FREEONEXIT
static void free_cert(void)
{
	unsigned i;
	for(i = 0; i < nx509certs; i++) {
		pmdp_x509_crt_free(x509certs[i].cert);
		pmdp_x509_key_free(x509certs[i].pkey);
	}
	if(x509certs)
		free(x509certs);
}
#endif

/* Find a cerificate based on its fingerprint */
static x509cert_t *find_fingerprint(const uint8_t *fp, unsigned fpsize)
{
	int i;
	for(i = 0; i < nx509certs; i++) {
		x509cert_t *c = &x509certs[i];
		if(c->fpsize == fpsize && !memcmp(c->fingerprint, fp, fpsize)) {
			return c;
		}
	}
	return NULL;
}

/*
 * Tracking duplicate packets based on UUID
 */
/* Timeout handler to clear the list */
static void sigalarm(int sig)
{
	int i;
	(void)sig;
	if(!npastpackets)
		return;
	for(i = 0; i < npastpackets; i++) {
		if(pastpackets[i].ttl)
			pastpackets[i].ttl--;
	}
}

static void sigquitit(int sig)
{
	(void)sig;
	quit = 1;
}

/* Add a UUID to the list of "seen" packets */
static void pastpacket_add(const uint8_t *uuid)
{
	int i;
	pastpacket_t *newpp;
	pastpacket_t *oldpp;

	if(!pastpackets) {
		if(!(pastpackets = calloc(PASTPACKETALLOC, sizeof(*pastpackets))))
			return;
		npastpackets = PASTPACKETALLOC;
	}
	for(i = 0; i < npastpackets; i++) {
		if(!pastpackets[i].ttl) {
			memcpy(pastpackets[i].uuid, uuid, sizeof(pastpackets[i].uuid));
			pastpackets[i].ttl = PASTPACKETTTL;
			return;
		}
	}
	/* If we get here, there is no more room; extend the array double size.
	 * If we use realloc, the signal handler can cry foul on using freed mem. If
	 * the timer fires after we copy the data, that just means that we lose a
	 * second, nothing to worry about.
	 */
	if(!(newpp = calloc(2 * npastpackets, sizeof(*pastpackets))))
		return;
	/* Copy the lower half of the array */
	memcpy(&newpp[0], &pastpackets[0], npastpackets * sizeof(*pastpackets));
	/* and swap the arrays */
	oldpp = pastpackets;
	pastpackets = newpp;
	npastpackets *= 2;
	free(oldpp);
	pastpacket_add(uuid);	/* Call self to finish the add */
}

/* See if we know the UUID of a packet */
static int pastpacket_find(const uint8_t *uuid)
{
	int i;
	for(i = 0; i < npastpackets; i++) {
		if(pastpackets[i].ttl && !memcmp(pastpackets[i].uuid, uuid, sizeof(pastpackets[i].uuid)))
			return 1;
	}
	return 0;
}

/*
 * Printing routines
 * Make the information (semi-)readable to humans.
 */
static const char *get_severity(uint32_t s)
{
	switch(s) {
	case PMDS_DEBUG:	return "PMDS_DEBUG";
	case PMDS_INFO:		return "PMDS_INFO";
	case PMDS_NOTICE:	return "PMDS_NOTICE";
	case PMDS_WARNING:	return "PMDS_WARNING";
	case PMDS_ERROR:	return "PMDS_ERROR";
	case PMDS_CRITICAL:	return "PMDS_CRITICAL";
	case PMDS_ALERT:	return "PMDS_ALERT";
	case PMDS_EMERGENCY:	return "PMDS_EMERGENCY";
	}
	return "Invalid severity";
}

static const char *get_category(uint32_t c)
{
	static char buf[64];
	switch(c) {
	case PMDC_LOCAL:	return "PMDC_LOCAL";
	case PMDC_MONITOR:	return "PMDC_MONITOR";
	case PMDC_LOG:		return "PMDC_LOG";
	case PMDC_TRAFFIC:	return "PMDC_TRAFFIC";
	case PMDC_PUBLIC:	return "PMDC_PUBLIC";
	case PMDC_COMMERCIAL:	return "PMDC_COMMERCIAL";
	}
	sprintf(buf, "%u (Unknown category)", c);
	return buf;
}

static const char *get_subcategory(uint32_t cat, uint32_t scat)
{
	static char buf[64];
	if(!scat) {
		/* This is a PMDC_x_NONE */
		strcpy(buf, get_category(cat));
		if(buf[0] == 'P') {
			/* We know this category, append _NONE */
			strcat(buf, "_NONE");
			return buf;
		} else {
			sprintf(buf, "PMDC_(%08x)_NONE", cat);
			return buf;
		}
	}
	switch(cat) {
	case PMDC_LOCAL:
		switch(scat) {
			case PMDC_LOCAL_NONE:	return "PMDC_LOCAL_NONE";
			case PMDC_LOCAL_BELL:	return "PMDC_LOCAL_BELL";
			case PMDC_LOCAL_DINNER:	return "PMDC_LOCAL_DINNER";
			default:
				sprintf(buf, "Unknown subcategory PMDC_LOCAL_(%08x)", scat);
				break;
		}
		break;
	case PMDC_MONITOR:
		switch(scat) {
			case PMDC_MONITOR_TEMPERATURE:	return "PMDC_MONITOR_TEMPERATURE";
			default:
				sprintf(buf, "Unknown subcategory PMDC_MONITOR_(%08x)", scat);
				break;
		}
		break;
	case PMDC_LOG:
		sprintf(buf, "Unknown subcategory PMDC_LOG_(%08x)", scat);
		break;
	case PMDC_TRAFFIC:
		sprintf(buf, "Unknown subcategory PMDC_TRAFFIC_(%08x)", scat);
		break;
	case PMDC_PUBLIC:
		sprintf(buf, "Unknown subcategory PMDC_PUBLIC_(%08x)", scat);
		break;
	case PMDC_COMMERCIAL:
		sprintf(buf, "Unknown subcategory PMDC_COMMERCIAL_(%08x)", scat);
		break;
	case PMDC_SERVICE:
		switch(scat) {
			case PMDC_SERVICE_NONE:		return "PMDC_SERVICE_NONE";
			case PMDC_SERVICE_READYTOORDER:	return "PMDC_SERVICE_READYTOORDER";
			case PMDC_SERVICE_CHECKPLEASE:	return "PMDC_SERVICE_CHECKPLEASE";
			case PMDC_SERVICE_QUEUEREQUEST:	return "PMDC_SERVICE_QUEUEREQUEST";
			case PMDC_SERVICE_QUEUEASSIGN:	return "PMDC_SERVICE_QUEUEASSIGN";
			case PMDC_SERVICE_QUEUESTATUS:	return "PMDC_SERVICE_QUEUESTATUS";
			default:
				sprintf(buf, "Unknown subcategory PMDC_QUEUE_(%08x)", scat);
				break;
		}
		break;
	default:
		sprintf(buf, "Unknown subcategory PMDC_(%08x)_(%08x)", cat, scat);
		break;
	}
	return buf;
}

#if 0
static char *get_algorithm(uint16_t algo, size_t *alen)
{
	static char buf[64];
	switch(algo) {
	case PMDE_AES128_CBC:
		*alen = 16;
		return "PMDE_AES128_CBC";
	case PMDE_AES192_CBC:
		*alen = 24;
		return "PMDE_AES192_CBC";
	case PMDE_AES256_CBC:
		*alen = 32;
		return "PMDE_AES256_CBC";
	default:
		*alen = 0;
		sprintf(buf, "Unknown encryption algorithm PMDE_(%04x)", algo);
		return buf;
	}
}
#endif

static void print_part(const pmdp_msg_part_t *part, const char *pfx)
{
	unsigned i;
	char buf[64];
	struct tm *tmp;
	uint32_t subcat;
	static uint32_t cat = 0;
	x509cert_t *fpc;

	switch(part->type) {
	case PMDT_TEXT:
		fprintf(stdout, "%s: PMDT_TEXT(%u, %zu): %s\n", pfx, part->type, part->length, (part->flags & PMDP_PARSE_VALIDUTF8) ? part->string : "<invalid-utf8>");
		break;
	case PMDT_RESOURCE:
		fprintf(stdout, "%s: PMDT_RESOURCE(%u, %zu): %s\n", pfx, part->type, part->length, (part->flags & PMDP_PARSE_VALIDUTF8) ? part->string : "<invalid-utf8>");
		break;
	case PMDT_UUID:
		pmdp_uuid_unparse(part->uuid, buf);
		fprintf(stdout, "%s: PMDT_UUID(%u, %zu): %s\n", pfx, part->type, part->length, buf);
		break;
	case PMDT_TIMESTAMP:
		fprintf(stdout, "%s: PMDT_TIMESTAMP(%u, %zu): ", pfx, part->type, part->length);
		tmp = localtime(&part->timestamp.tv_sec);
		strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", tmp);
		fprintf(stdout, "%s.%06u\n", buf, (unsigned)part->timestamp.tv_usec);
		break;
	case PMDT_CATEGORY:
		fprintf(stdout, "%s: PMDT_CATEGORY(%u, %zu): ", pfx, part->type, part->length);
		cat = part->category;
		fprintf(stdout, "%s (%u, 0x%08x)\n", get_category(cat), cat, cat);
		break;
	case PMDT_SUBCATEGORY:
		fprintf(stdout, "%s: PMDT_SUBCATEGORY(%u, %zu): ", pfx, part->type, part->length);
		subcat = part->subcategory;
		fprintf(stdout, "%s (%u, 0x%08x)%s\n", get_subcategory(cat, subcat), subcat, subcat, (subcat & 0x8000000) ? " (Private)" : "");
		break;
	case PMDT_CARDINAL:
		fprintf(stdout, "%s: PMDT_CARDINAL(%u, %zu): ", pfx, part->type, part->length);
		fprintf(stdout, "%u (0x%08x)\n", part->cardinal, part->cardinal);
		break;
	case PMDT_INTEGER:
		fprintf(stdout, "%s: PMDT_INTEGER(%u, %zu): ", pfx, part->type, part->length);
		fprintf(stdout, "%d (0x%08x)\n", part->integer, part->integer);
		break;
	case PMDT_FRACTION:
		fprintf(stdout, "%s: PMDT_FRACTION(%u, %zu): ", pfx, part->type, part->length);
		fprintf(stdout, "%d / %u%s\n", part->fraction.dividend, part->fraction.divisor, !part->fraction.divisor ? (!part->fraction.dividend ? " (NAN)" : (part->fraction.dividend < 0 ? " (-Inf)" : " (+Inf)")) : "");
		break;
	case PMDT_KEY:
		fprintf(stdout, "%s: PMDT_KEY(%u, %zu): ...\n", pfx, part->type, part->length);
		break;
	case PMDT_ENCRYPTED:
		fprintf(stdout, "%s: PMDT_ENCRYPTED(%u, %zu):\n", pfx, part->type, part->length);
		if((fpc = find_fingerprint(part->encrypted.fingerprint->octets, part->encrypted.fingerprint->length))) {
			if(!fpc->pkey) {
				fprintf(stdout, "%s: warning: Private key for indicated certificate not available, cannot decrypt\n", pfx);
			} else {
				pmdp_msg_t *emsg;
				int rv;
				unsigned flags = PMDP_PARSE_UTYPES | PMDP_PARSE_TRAILING | PMDP_PARSE_BADUTF8;
				if(0 != (rv = pmdp_msg_parse_encrypted(fpc->pkey, part, &emsg, &flags))) {
					fprintf(stdout, "%s: error: Encrypted message parse error: %s\n", pfx, pmdp_strerror(rv));
				} else {
					char *cptr = xmalloc(strlen(pfx) + 2);
					strcpy(cptr, pfx);
					strcat(cptr, "\t");
					pmdp_msg_iterate_parts(emsg, partcb, cptr);
					free(cptr);
					pmdp_msg_free(emsg);
				}
			}
		}
		break;
	case PMDT_PADDING:
		fprintf(stdout, "%s: PMDT_PADDING(%u, %zu): %zu octets\n", pfx, part->type, part->length, part->length);
		break;
	case PMDT_FINGERPRINT:
		fprintf(stdout, "%s: PMDT_FINGERPRINT(%u, %zu): (%s): ", pfx, part->type, part->length, find_fingerprint(part->octets, part->length) ? "Known" : "Unknown");
		if(part->length > 0) {
			fprintf(stdout, "%02x", part->octets[0]);
			for(i = 1; i < part->length; i++)
				fprintf(stdout, ":%02x", part->octets[i]);
		}
		fprintf(stdout, "\n");
		break;
	case PMDT_SIGNATURE:
		fprintf(stdout, "%s: PMDT_SIGNATURE(%u, %zu): ", pfx, part->type, part->length);
		fprintf(stdout, "%s\n", !(part->flags & PMDP_PARSE_VALIDFP) ? "Signature unverified" : ((part->flags & PMDP_PARSE_VALIDSIG) ? "Signature verified" : "Invalid signature"));
		break;
	}
}

static int make_portlist(void)
{
	int i, j;
	for(i = 0; i < nmcaddrport; i++) {
		int found = 0;
		unsigned short port;
		if(mcaddrport[i].sa.sa_family == AF_INET)
			port = mcaddrport[i].sa4.sin_port;
		else
			port = mcaddrport[i].sa6.sin6_port;
		for(j = 0; j < nportlist; j++) {
			if(portlist[j].af == mcaddrport[i].sa.sa_family && portlist[j].port == port) {
				found = 1;
				break;
			}
		}
		if(found)
			continue;
		if(!portlist) {
			portlist = xmalloc(sizeof(*portlist));
			nportlist = 1;
		} else {
			nportlist++;
			portlist = xrealloc(portlist, nportlist * sizeof(*portlist));
		}
		portlist[nportlist-1].af = mcaddrport[i].sa.sa_family;
		portlist[nportlist-1].port = port;
	}
	return nportlist;
}

/*
 * Callback functions
 */
static int srvcb(const struct sockaddr *sa, int salen, void *ptr)
{
	if(ptr && *(int *)ptr)
		printf("Found SRV record for %s\n", get_name(sa, 1));

	if(!find_mcaddrport(sa))
		add_mcaddrport(sa, salen, 1);
	return 1;
}

static int certcb(pmdp_x509_crt_t *crt, void *ptr)
{
	int idx = add_cert(crt, NULL);

	if(ptr && *(int *)ptr && idx >= 0) {
		size_t i;
		printf("Found DNS certificate:");
		for(i = 0; i < x509certs[idx].fpsize; i++)
			printf("%c%02x", i ? ':' : ' ', x509certs[idx].fingerprint[i]);
		printf("\n");
	}

	return 1;
}

static int partcb(const pmdp_msg_t *msg, const pmdp_msg_part_t *part, void *ptr)
{
	print_part(part, (const char *)ptr);
	return 1;
}

/*
 * Main part of the application starts here...
 */
static const char usage_str[] =
	"Usage: pmdrecv [options]\n"
	"Options:\n"
	"  -4|--ipv4               Listen on IPv4 addresses (default both 4 and 6)\n"
	"  -6|--ipv6               Listen on IPv6 addresses (default both 4 and 6)\n"
	"  -A|--defaultmc          Always add PMDP standard MC address and port\n"
	"  -a|--execall            Execute command on all packets received\n"
	"  -c|--certbundle bundle  Load certificate bundle file (may be used multiple times)\n"
	"  -d|--nodups             Suppress printing duplicate packets\n"
	"  -D|--domain dom         Use domain name 'dom' for DNS queries (may be used multiple times)\n"
	"  -e|--exit               Exit after receiving one packet\n"
	"  -f|--certfile crtfile   Decryption certificate in crtfile (see also -k)\n"
	"  -h|--help               This message\n"
	"  -l|--listen addrport    Listen on IP mcaddr:port; IPv6 use [xx:xx::xx]:port\n"
	"  -L|--nolinklocal        Disable link local addresses\n"
	"  -n|--nodns              Disable DNS server lookups\n"
	"  -q|--quiet              Be quiet\n"
	"  -s|--keyfile keyfile    Load private key (see also -s)\n"
	"  -v|--verbose            Be more verbose\n"
	"  -V|--version            Show version information\n"
	"  -x|--exec cmd           Execute 'cmd' in a shell upon arrival of packet\n"
	;

static const struct option lopts[] = {
	{ "ipv4",		no_argument,		NULL,	'4' },
	{ "ipv6",		no_argument,		NULL,	'6' },
	{ "defaultmc",		no_argument,		NULL,	'A' },
	{ "execall",		no_argument,		NULL,	'a' },
	{ "certbundle",		required_argument,	NULL,	'c' },
	{ "nodups",		no_argument,		NULL,	'd' },
	{ "domain",		required_argument,	NULL,	'D' },
	{ "exit",		no_argument,		NULL,	'e' },
	{ "certfile",		required_argument,	NULL,	'f' },
	{ "help",		no_argument,		NULL,	'h' },
	{ "listen",		required_argument,	NULL,	'l' },
	{ "nolinklocal",	no_argument,		NULL,	'L' },
	{ "nodns",		no_argument,		NULL,	'n' },
	{ "quiet",		no_argument,		NULL,	'q' },
	{ "keyfile",		required_argument,	NULL,	's' },
	{ "verbose",		no_argument,		NULL,	'v' },
	{ "version",		no_argument,		NULL,	'V' },
	{ "exec",		required_argument,	NULL,	'x' },
};

int main(int argc, char *argv[])
{
	int optc;
	int oidx = 0;
	int lose = 0;
	char *eptr;
	int oneshot = 0;
	int i;
	char *pktbuf;
	char *cmd = NULL;
	int quiet = 0;
	int allcmd = 0;
	pmdp_x509_crt_t *x509 = NULL;
	pmdp_x509_key_t *pkey = NULL;
	struct itimerval itt;
	int nodups = 0;
	int nodns = 0;
	int useipv4 = 0;
	int useipv6 = 0;
	int usedefip = 0;
	struct pollfd *pfd;
	int nsock;
	int validsig;
	int verbose = 0;
	int nolinklocal = 0;

	pktbuf = xmalloc(PMDP_MAX_MSGSIZE);

	while(EOF != (optc = getopt_long(argc, argv, "46aAc:dD:ef:hl:Lnqs:vVx:", lopts, &oidx))) {
		switch(optc) {
		case '4':
			useipv4 = 1;
			break;
		case '6':
			useipv6 = 1;
			break;
		case 'a':
			allcmd = 1;
			break;
		case 'A':
			usedefip = 1;
			break;
		case 'c':
			if(0 != (i = pmdp_x509_crt_bundle_load_file(optarg, certcb, NULL))) {
				fprintf(stderr, "%s: %s\n", optarg, pmdp_strerror(i));
				lose++;
			}
			break;
		case 'd':
			nodups = 1;
			break;
		case 'D':
			add_domname(optarg);
			break;
		case 'e':
			oneshot = 1;
			break;
		case 'f':
			if(!(x509 = pmdp_x509_crt_load_file(optarg))) {
				fprintf(stderr, "%s: cannot load certificate\n", optarg);
				lose++;
				break;
			}
			if(pkey) {
				if(-1 == add_cert(x509, pkey)) {
					pmdp_x509_crt_free(x509);
					pmdp_x509_key_free(pkey);
				}
				x509 = NULL;
				pkey = NULL;
			}
			break;
		case 'h':
			fprintf(stderr, "%s", usage_str);
			lose++;
			break;
		case 'l':
			eptr = xstrdup(optarg);
			if(eptr[0] == '[') {
				char *cptr;
				/* This should be an ipv6 address */
				if(!(cptr = strrchr(eptr, ']'))) {
					fprintf(stderr, "Missing ']' is IPv6 address\n");
					free(eptr);
					break;
				}
				*cptr++ = '\0';
				if((cptr = strchr(cptr, ':')))
					*cptr++ = '\0';
				else
					cptr = STRINGIZE_X(PMDP_PORT);
				if(!lookup_add_mcaddrport(eptr+1, cptr, 1))
					lose++;
			} else {
				char *cptr;
				if((cptr = strrchr(eptr, ':')))
					*cptr++ = '\0';
				else
					cptr = STRINGIZE_X(PMDP_PORT);
				if(!lookup_add_mcaddrport(eptr, cptr, 1))
					lose++;
			}
			free(eptr);
			break;
		case 'L':
			nolinklocal = 1;
			break;
		case 'n':
			nodns = 1;
			break;
		case 'q':
			quiet = 1;
			break;
		case 's':
			if(!(pkey = pmdp_x509_key_load_file(optarg, NULL, NULL))) {
				fprintf(stderr, "%s: cannot load private key\n", optarg);
				lose++;
				break;
			}
			if(x509) {
				if(-1 == add_cert(x509, pkey)) {
					pmdp_x509_crt_free(x509);
					pmdp_x509_key_free(pkey);
				}
				x509 = NULL;
				pkey = NULL;
			}
			break;
		case 'v':
			verbose++;
			break;
		case 'V':
			printf("PingMyDroid(tm) receiver version %s\n", PACKAGE_VERSION);
			exit(0);
			break;
		case 'x':
			cmd = xstrdup(optarg);
			if(strchr(cmd, '\'') || strchr(cmd, '"')) {
				fprintf(stderr, "Exec command '%s' may not include quotes\n", cmd);
				lose++;
			}
			break;
		default:
			lose++;
			break;
		}
	}

	if(lose)
		exit(1);

	if(quiet && verbose) {
		fprintf(stderr, "Being both quiet and verbose is a contradiction, will be verbose\n");
		quiet = 0;
	}

	if(!useipv4 && !useipv6) {
		useipv4 = 1;
		useipv6 = 1;
	}

	add_local_domnames(nodns, verbose, nolinklocal, useipv4, useipv6);

	/* Add MC addresses from SRV records */
	if(ndomname && !nodns) {
		for(i = 0; i < ndomname; i++) {
			int err = pmdp_dns_srv_by_name(domname[i], srvcb, &verbose);
			if(err && err != PMDP_ERR_NOTFOUND)
				fprintf(stderr, "SRV lookup for '%s': %s\n", domname[i], pmdp_strerror(err));
		}
	}

	if(verbose && ndomname) {
		for(i = 0; i < ndomname; i++)
			fprintf(stdout, "Bound to domain: %s\n", domname[i]);
	}

	if(usedefip || !nmcaddrport) {
		/* If we do not yet have any listen addresses, add the defaults */
		if(useipv4 && !lookup_add_mcaddrport(PMDP_MCADDR, STRINGIZE_X(PMDP_PORT), 1))
			exit(1);
		if(useipv6 && !lookup_add_mcaddrport(PMDP_MCADDR6, STRINGIZE_X(PMDP_PORT), 1))
			exit(1);
		if(!nmcaddrport) {
			/* We may still fail, unlikely, but possible */
			fprintf(stderr, "No IP addresses available for listening\n");
			exit(1);
		}
	}

	if(verbose) {
		for(i = 0; i < nmcaddrport; i++)
			fprintf(stdout, "Found target address: %s\n", get_name(&mcaddrport[i].sa, 1));
	}

	/* Retrieve the certificates for all domains */
	if(ndomname && !nodns) {
		for(i = 0; i < ndomname; i++)
			pmdp_dns_crt_by_name(domname[i], certcb, &verbose);
	}

	/* Get all unique ports by address family */
	if(!make_portlist()) {
		fprintf(stderr, "No unique ports found to listen on\n");
		exit(1);
	}

	if(verbose) {
		for(i = 0; i < nportlist; i++)
			fprintf(stdout, "Found listen port: %s:%u\n", portlist[i].af == AF_INET ? "IPv4" : "IPv6", ntohs(portlist[i].port));
	}

	/* Get room for socket poll descriptors */
	/* This is the maximum we can get */
	pfd = xmalloc(sizeof(*pfd) * nportlist);
	nsock = 0;

	/* Get sockets for all unique ports on the correct address families */
	for(i = 0; i < nportlist; i++) {
		int sock;
		int j;
		int reuse = 1;
		int pktinfo = 1;
		mcaddrport_t sa;
		if(-1 == (sock = socket(portlist[i].af, SOCK_DGRAM, 0))) {
			perror("socket()");
			continue;
		}
#ifdef HAVE_SO_REUSEPORT
		if(-1 == setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &reuse, sizeof(reuse))) {
#else
		if(-1 == setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse))) {
#endif
			perror("setsockopt(SO_REUSEADDR)");
			close(sock);
			continue;
		}
		memset(&sa, 0, sizeof(sa));	/* This translates to INADDR_ANY/::/any interface */
		sa.sa.sa_family = portlist[i].af;
		if(portlist[i].af == AF_INET) {
			if(-1 == setsockopt(sock, IPPROTO_IP, IP_PKTINFO, &pktinfo, sizeof(pktinfo))) {
				perror("setsockopt(IP_PKTINFO)");
				close(sock);
				continue;
			}
			sa.sa4.sin_port = portlist[i].port;
			if(-1 == bind(sock, &sa.sa, sizeof(sa.sa4))) {
				fprintf(stderr, "port %d: ", ntohs(portlist[i].port));
				perror("bind(IPv4)");
				close(sock);
				continue;
			}
			for(j = 0; j < nmcaddrport; j++) {
				struct ip_mreq group;
				unsigned k;
				char nbuf[IF_NAMESIZE+1];
				if(portlist[i].af != mcaddrport[j].sa.sa_family || portlist[i].port != mcaddrport[j].sa4.sin_port)
					continue;
				for(k = 0; k < nifgrpifs; k++) {
					if(ifgrpifs[k].family != AF_INET)
						continue;
					memset(&group, 0, sizeof(group));
					memcpy(&group.imr_multiaddr, &mcaddrport[j].sa4.sin_addr, sizeof(group.imr_multiaddr));
					group.imr_interface = ifgrpifs[k].addr;
					if(-1 == setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &group, sizeof(group))) {
						fprintf(stderr, "%s: ", get_name(&mcaddrport[j].sa, 0));
						perror("setsockopt(IP_ADD_MEMBERSHIP)");
						continue;
					}
					if(verbose) {
						fprintf(stdout, "Add MC membership port %u, target %s, interface %s\n",
							ntohs(portlist[i].port), get_name(&mcaddrport[j].sa, 0), if_indextoname(ifgrpifs[k].ifidx, nbuf));
					}
				}
			}
		} else {
			if(-1 == setsockopt(sock, IPPROTO_IPV6, IPV6_RECVPKTINFO, &pktinfo, sizeof(pktinfo))) {
				perror("setsockopt(IPV6_RECVPKTINFO)");
				close(sock);
				continue;
			}
			sa.sa6.sin6_port = portlist[i].port;
			if(-1 == bind(sock, &sa.sa, sizeof(sa.sa6))) {
				fprintf(stderr, "port %d: ", ntohs(portlist[i].port));
				perror("bind(IPv6)");
				close(sock);
				continue;
			}
			for(j = 0; j < nmcaddrport; j++) {
				struct ipv6_mreq group;
				unsigned k;
				char nbuf[IF_NAMESIZE+1];
				if(portlist[i].af != mcaddrport[j].sa.sa_family || portlist[i].port != mcaddrport[j].sa4.sin_port)
					continue;
				for(k = 0; k < nifgrpifs; k++) {
					if(ifgrpifs[k].family != AF_INET6)
						continue;
					memset(&group, 0, sizeof(group));
					memcpy(&group.ipv6mr_multiaddr, &mcaddrport[j].sa6.sin6_addr, sizeof(group.ipv6mr_multiaddr));
					group.ipv6mr_interface = ifgrpifs[k].ifidx;
					if(-1 == setsockopt(sock, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, &group, sizeof(group))) {
						fprintf(stderr, "%s: ", get_name(&mcaddrport[j].sa, 0));
						perror("setsockopt(IPV6_ADD_MEMBERSHIP)");
						continue;
					}
					if(verbose) {
						fprintf(stdout, "Add MC membership port %u, target %s, interface %s\n",
							ntohs(portlist[i].port), get_name(&mcaddrport[j].sa, 0), if_indextoname(ifgrpifs[k].ifidx, nbuf));
					}
				}
			}
		}
		pfd[nsock].fd = sock;
		pfd[nsock].events = POLLIN;
		pfd[nsock].revents = 0;
		nsock++;
	}

	if(!nsock) {
		fprintf(stderr, "No sockets available to listen on\n");
		exit(1);
	}

	/* Setup interval timer to expire old packet's uuid */
	itt.it_interval.tv_sec = 1;
	itt.it_interval.tv_usec = 0;
	itt.it_value.tv_sec = 1;
	itt.it_value.tv_usec = 0;
	signal(SIGALRM, sigalarm);
	signal(SIGINT, sigquitit);
	signal(SIGTERM, sigquitit);
	signal(SIGHUP, sigquitit);
	signal(SIGPIPE, sigquitit);
	setitimer(ITIMER_REAL, &itt, NULL);

	/* Mainloop, reading data, handling packets */
	while(!quit) {
		int err;
		ssize_t len;
		unsigned flags;
		pmdp_msg_t *msg;
		sa_any_t peer;
		socklen_t peerlen;
		struct timeval tv, ts;
		struct tm *tmp;
		char pfx[256];
		char tbuf[64];
		uint8_t curuuid[PMDP_UUID_SIZE];
		char curuuidstr[2*PMDP_UUID_SIZE+4+1];
		unsigned severity;
		unsigned sequence;
		unsigned version;
		uint32_t pflags;
		int seenbefore;
		struct iovec iov;
		struct msghdr mhdr;
		uint8_t ctrl[1024];	/* FIXME: find sensible size */
		struct cmsghdr *cmsg;
		int ifidx = -1;
		sa_any_t target;
		char ifnamebuf[IF_NAMESIZE];

		err = poll(pfd, nsock, -1);
		if(-1 == err) {
			if(errno == EINTR)
				continue;
			perror("poll()");
			exit(1);	/* FIXME: grace? */
		}
		if(!err)
			continue;

		for(i = 0; i < nsock; i++) {
			if(!(pfd[i].revents & POLLIN))
				continue;

			iov.iov_base = pktbuf;
			iov.iov_len = PMDP_MAX_MSGSIZE;
			mhdr.msg_name = &peer;
			mhdr.msg_namelen = sizeof(peer);
			mhdr.msg_iov = &iov;
			mhdr.msg_iovlen = 1;
			mhdr.msg_control = &ctrl;
			mhdr.msg_controllen = sizeof(ctrl);
			mhdr.msg_flags = 0;

			/* There is data to be handled */
			if(-1 == (len = recvmsg(pfd[i].fd, &mhdr, 0))) {
				if(errno == EINTR)
					continue;
				perror("recvmsg()");
				continue;
			} else if(len < 0) {
				fprintf(stderr, "Packet invalid size, got %zd expected >= %zu\n", len, sizeof(pmdp_pkt_hdr_t));
				continue;
			}

			gettimeofday(&tv, NULL);	/* Mark the receipt timestamp */

			peerlen = mhdr.msg_namelen;

			/* Retrieve the ancillary data to see what the target was */
			for(cmsg = CMSG_FIRSTHDR(&mhdr); cmsg; cmsg = CMSG_NXTHDR(&mhdr, cmsg)) {
				socklen_t sl;
				typedef union {
						struct in_pktinfo	in;
						struct in6_pktinfo	in6;
				} cmsgdata_t;
				cmsgdata_t *cmsgdata;
				cmsgdata = (cmsgdata_t *)CMSG_DATA(cmsg);
				sl = sizeof(target);
				getsockname(pfd[i].fd, &target.sa, &sl);
				if(cmsg->cmsg_level == IPPROTO_IP && cmsg->cmsg_type == IP_PKTINFO) {
					assert(cmsg->cmsg_len >= sizeof(struct in_pktinfo));
					ifidx = cmsgdata->in.ipi_ifindex;
					target.sa4.sin_family = AF_INET;
					memcpy(&target.sa4.sin_addr, &cmsgdata->in.ipi_addr, sizeof(target.sa4.sin_addr));
				} else if(cmsg->cmsg_level == IPPROTO_IPV6 && cmsg->cmsg_type == IPV6_PKTINFO) {
					assert(cmsg->cmsg_len >= sizeof(struct in6_pktinfo));
					ifidx = cmsgdata->in6.ipi6_ifindex;
					target.sa6.sin6_family = AF_INET6;
					memcpy(&target.sa6.sin6_addr, &cmsgdata->in6.ipi6_addr, sizeof(target.sa6.sin6_addr));
				}
			}

			/* And create a printable prefix */
			tmp = localtime(&tv.tv_sec);
			strftime(tbuf, sizeof(tbuf), "%Y-%m-%d %H:%M:%S", tmp);
			snprintf(pfx, sizeof(pfx), "%s.%06lu: %s", tbuf, tv.tv_usec, get_name(&peer.sa, 1));
			pfx[sizeof(pfx)-1] = 0;

			if(!quiet) {
				if(-1 == ifidx) {
					fprintf(stdout, "%s: warning: Missing interface info for packet\n", pfx);
				} else {
					fprintf(stdout, "%s: packet on interface %s(%d), target %s, size %zd\n",
							pfx, if_indextoname(ifidx, ifnamebuf), ifidx,
							get_name(&target.sa, 1), len);
				}
			}

			/* Parse the packet with most common parse errors returned as warning */
			flags = PMDP_PARSE_UTYPES | PMDP_PARSE_TRAILING | PMDP_PARSE_BADUTF8;
			if(0 != (err = pmdp_msg_parse(pktbuf, (size_t)len, &msg, &flags))) {
				fprintf(stderr, "Pmdp packet error: %s\n", pmdp_strerror(err));
				continue;
			}

			/* Extract the packet information for analysis and printing */
			pmdp_msg_get_uuid(msg, curuuid);
			pmdp_msg_get_version(msg, &version);
			pmdp_msg_get_severity(msg, &severity);
			pmdp_msg_get_sequence(msg, &sequence);
			pmdp_msg_get_flags(msg, &pflags);
			pmdp_msg_get_timestamp(msg, &ts);
			pmdp_uuid_unparse(curuuid, curuuidstr);
			tmp = localtime(&ts.tv_sec);
			strftime(tbuf, sizeof(tbuf), "%Y-%m-%d %H:%M:%S", tmp);

			/* Check if we've seen the UUID before */
			if(!(seenbefore = pastpacket_find(curuuid)))
				pastpacket_add(curuuid);
			else if(nodups) {
				/* Seen it, and we do not want to see doubles */
				if(verbose)
					fprintf(stdout, "%s: duplicate filtered %s\n", pfx, curuuidstr);
				pmdp_msg_free(msg);
				continue;
			}

			if(!quiet) {
				if(flags & PMDP_PARSE_UTYPES)
					fprintf(stdout, "%s: warning: Message contains unknown content types\n", pfx);
				if(flags & PMDP_PARSE_TRAILING)
					fprintf(stdout, "%s: warning: Message has trailing data\n", pfx);
				if(flags & PMDP_PARSE_BADUTF8)
					fprintf(stdout, "%s: warning: Message contains bad UTF-8 codepoints\n", pfx);
				if(verbose) {
					if(flags & PMDP_PARSE_HAVESIG)
						fprintf(stdout, "%s: Message is signed\n", pfx);
					if(flags & PMDP_PARSE_HAVEENCR)
						fprintf(stdout, "%s: Message contains encrypted data\n", pfx);
					if(flags & PMDP_PARSE_HAVEMULTI)
						fprintf(stdout, "%s: Message encapsulates multiple events\n", pfx);
				}
			}

			/* Check for a signature and check it for validity */
			if(flags & PMDP_PARSE_HAVESIG) {
				x509cert_t *fpc;
				pmdp_msg_part_t *fppart;
				if(!(fppart = pmdp_msg_get_fingerprint(msg))) {
					fprintf(stderr, "Cannot retrieve message's fingerprint\n");
				} else {
					if(!(fpc = find_fingerprint(fppart->octets, fppart->length))) {
						pmdp_dns_crt_by_addr(&peer.sa, peerlen, certcb, &verbose);
						if((fpc = find_fingerprint(fppart->octets, fppart->length)))
							goto found_cert;
					} else {
found_cert:
						if(!(validsig = 0 == (err = pmdp_msg_verify(msg, fpc->cert))))
							fprintf(stderr, "warning: Message signature error: %s\n", pmdp_strerror(err));
					}
				}
			}

			/* Inform the user */
			if(!quiet && !(seenbefore && nodups)) {
				fprintf(stdout, "%s: Version   : %u\n", pfx, version);
				fprintf(stdout, "%s: Severity  : %s\n", pfx, get_severity(severity));
				fprintf(stdout, "%s: Flags     : %u %s\n", pfx, pflags, (pflags & PMDF_FORCESILENT) ? "PMDF_FORCESILENT" : "(none)");
				fprintf(stdout, "%s: Sequence  : %u\n", pfx, sequence);
				fprintf(stdout, "%s: Timestamp : %s.%06u\n", pfx, tbuf, (unsigned)ts.tv_usec);
				fprintf(stdout, "%s: UUID      : %s\n", pfx, curuuidstr);
				pmdp_msg_iterate_parts(msg, partcb, pfx);
			}

			/* Execute a external command on packet reception*/
			if(cmd) {
				char cmdline[4096];
				if(allcmd || !seenbefore) {
					int rv;
					snprintf(cmdline, sizeof(cmdline), "%s '%u' '%s' '%u' '%s' %u",
						cmd,
						severity,
						tbuf,
						(pflags & PMDF_FORCESILENT) ? 1 : 0,
						curuuidstr,
						sequence
						);
					rv = system(cmdline);
					if(rv) {
						if(-1 == rv) {
							perror("system()");
							exit(1);
						}
						if(WIFSIGNALED(rv) && (WTERMSIG(rv) == SIGINT || WTERMSIG(rv) == SIGQUIT || WTERMSIG(rv) == SIGTERM))
							goto done_here;
						if(!quiet)
							fprintf(stderr, "system() on requested command returned non-zero: %d\n", rv);
					}
				}
			}

			pmdp_msg_free(msg);

			if(oneshot)
				goto done_here;
		}
	}

done_here:
#ifdef FREEONEXIT
	free_domname();
	free_mcaddrport();
	free_cert();
	if(portlist)
		free(portlist);
	free(pktbuf);
	free(pfd);
#ifdef HAVE_LIBCURL
	curl_global_cleanup();
#endif
#endif
	return 0;
}
