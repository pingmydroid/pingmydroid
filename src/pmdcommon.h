/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PMDP_PMDCOMMON_H
#define __PMDP_PMDCOMMON_H

#define STRINGIZE(s)	#s
#define STRINGIZE_X(s)	STRINGIZE(s)

#define IN_IS_ADDR_LOOPBACK(a)		((ntohl((a)->s_addr) & 0xff000000) == 0x7f000000)
#define IN_IS_ADDR_LINKLOCAL(a)		((ntohl((a)->s_addr) & 0xffff0000) == 0xa9fe0000)

void *xmalloc(size_t s);
void *xrealloc(void *op, size_t s);
char *xstrdup(const char *s);

const char *get_name(const struct sockaddr *sa, int showport);

/* A list of requested addresses/ports to listen on */
typedef union {
	struct sockaddr		sa;
	struct sockaddr_in	sa4;
	struct sockaddr_in6	sa6;
} mcaddrport_t;

mcaddrport_t *find_mcaddrport(const struct sockaddr *sa);
mcaddrport_t *add_mcaddrport(const struct sockaddr *sa, size_t salen, int ismc);
void free_mcaddrport(void);
int lookup_add_mcaddrport(const char *name, const char *port, int ismc);

extern mcaddrport_t *mcaddrport;
extern int nmcaddrport;

void add_domname(const char *dname);
void free_domname(void);
void add_local_domnames(int nodns, int verbose, int nolinklocal, int useipv4, int useipv6);

extern char **domname;
extern unsigned ndomname;

typedef struct __ifgrpifs_t {
	unsigned family;
	unsigned ifidx;
	struct in_addr addr;
} ifgrpifs_t;

extern ifgrpifs_t *ifgrpifs;
extern unsigned nifgrpifs;
#endif
