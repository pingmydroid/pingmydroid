/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <assert.h>
#include <errno.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/nameser.h>
#include <resolv.h>

#include <curl/curl.h>

#ifndef NS_ALG_RSASHA1
#define NS_ALG_RSASHA1	5
#endif

#include "util.h"
#include "dnsreq.h"
#include "array.h"
#include "cert.h"


typedef struct __dns_cert_t {
	uint16_t	type;
	uint16_t	keytag;
	uint8_t		algorithm;
	size_t		certlen;
	char		cert[NS_MAXMSG];
} dns_cert_t;

static u_char dnsbuffer[NS_MAXMSG];			/* DNS lookup buffer */

array_t *query_srv(const char *domname)
{
	char buf[MAXDNAME];
	int res;
	ns_msg handle;
	int i, j;
	array_t *srvlist;

	if(!domname || !*domname || strlen(domname) >= sizeof(buf) - sizeof("_pmdp._udp..")) {
		return NULL;
	}
	/* Create the FQDN for the service; domname is made absolute if it has no trailing '.' */
	snprintf(buf, sizeof(buf), "_pmdp._udp.%s%s", domname, domname[strlen(domname)-1] == '.' ? "" : ".");

	if(!(srvlist = array_new()))
		return NULL;

	if((res = res_search(buf, ns_c_in, ns_t_srv, dnsbuffer, sizeof(dnsbuffer))) < 0) {
		array_free(srvlist);
		return NULL;	/* error */
	}
	if(ns_initparse(dnsbuffer, res, &handle) < 0) {
		array_free(srvlist);
		return NULL;	/* error */
	}

	/* Get the records from the answer section */
	for(i = 0; i < ns_msg_count(handle, ns_s_an); i++) {
		ns_rr rr;
		if(ns_parserr(&handle, ns_s_an, i, &rr)) {
			array_free(srvlist);
			return NULL;	/* parse error */
		}
		if(ns_rr_type(rr) == ns_t_srv) {
			dns_srv_t srv;
			const u_char *cp;
			memset(&srv, 0, sizeof(srv));
			//memdump(ns_rr_rdata(rr), ns_rr_rdlen(rr));
			if(ns_rr_rdlen(rr) < 2+2+2+1 || ns_rr_rdlen(rr) > 2+2+2+sizeof(srv.target)-1)
				continue;	/* Must have content to handle */
			cp = ns_rr_rdata(rr);
			srv.priority = ns_get16(cp);
			cp += 2;
			srv.weight = ns_get16(cp);
			cp += 2;
			srv.port = ns_get16(cp);
			cp += 2;
			if(ns_name_uncompress(ns_msg_base(handle), ns_msg_end(handle), cp, srv.target, sizeof(srv.target)) < 0)
				continue;
			array_add(srvlist, memdup(&srv, sizeof(srv)));
			//printf("SRV %u %u %u %s\n", srv.priority, srv.weight, srv.port, srv.target);
		}
	}
	/* Get the records from the additional section */
	/* They may provide us with the addresses of the srv target */
	for(i = 0; i < ns_msg_count(handle, ns_s_ar); i++) {
		ns_rr rr;
		if(ns_parserr(&handle, ns_s_ar, i, &rr)) {
			array_free(srvlist);
			return NULL;	/* parse error */
		}
		if(ns_rr_type(rr) == ns_t_a) {
			if(ns_rr_rdlen(rr) != sizeof(in_addr_t))
				continue;
			for(j = 0; j < array_size(srvlist); j++) {
				dns_srv_t *p = array_val(srvlist, j, dns_srv_t *);
				if(!strcasecmp(p->target, ns_rr_name(rr)) && p->nipv4 < MAX_IPS) {
					memcpy(&p->ipv4[p->nipv4], ns_rr_rdata(rr), sizeof(p->ipv4[0]));
					p->nipv4++;
					break;
				}
			}
		} else if(ns_rr_type(rr) == ns_t_aaaa) {
			if(ns_rr_rdlen(rr) != sizeof(struct in6_addr))
				continue;
			for(j = 0; j < array_size(srvlist); j++) {
				dns_srv_t *p = array_val(srvlist, j, dns_srv_t *);
				if(!strcasecmp(p->target, ns_rr_name(rr)) && p->nipv6 < MAX_IPS) {
					memcpy(&p->ipv6[p->nipv6], ns_rr_rdata(rr), sizeof(p->ipv6[0]));
					p->nipv6++;
					break;
				}
			}
		}
	}

	/* Find all addresses we do not yet have */
	for(j = 0; j < array_size(srvlist); j++) {
		dns_srv_t *p = array_val(srvlist, j, dns_srv_t *);
		struct addrinfo hints;
		struct addrinfo *aires, *aip;
		int nai;
		/* No need to query further if we got adresses via the additional section */
		if(p->nipv4 || p->nipv6)
			continue;

		/* Now find the addresses of the target */
		memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_DGRAM;
		hints.ai_flags = AI_PASSIVE;
		if((nai = getaddrinfo(p->target, NULL, &hints, &aires)))
			continue;
		for(aip = aires; aip; aip = aip->ai_next) {
			if(aip->ai_family == AF_INET && p->nipv4 < MAX_IPS) {
				memcpy(&p->ipv4[p->nipv4], &((struct sockaddr_in *)aip->ai_addr)->sin_addr.s_addr, sizeof(p->ipv4[0]));
				p->nipv4++;
			} else if(aip->ai_family == AF_INET6 && p->nipv6 < MAX_IPS) {
				memcpy(&p->ipv6[p->nipv6], ((struct sockaddr_in6 *)aip->ai_addr)->sin6_addr.s6_addr, sizeof(p->ipv6[0]));
				p->nipv6++;
			}
		}
		freeaddrinfo(aires);
	}
	return srvlist;
}

static char *reverse_name(int af, u_char *addr, char *buf, size_t bufsize)
{
	if(!addr || !buf)
		return NULL;

	switch(af) {
	case AF_INET:
		snprintf(buf, bufsize, "%u.%u.%u.%u.in-addr.arpa.", addr[3], addr[2], addr[1], addr[0]);
		break;
	case AF_INET6:
		snprintf(buf, bufsize, "%u.%u.%u.%u.%u.%u.%u.%u.%u.%u.%u.%u.%u.%u.%u.%u.ip6.arpa.",
			addr[15], addr[14], addr[13], addr[12], addr[11], addr[10], addr[9], addr[8],
			addr[7], addr[6], addr[5], addr[4], addr[3], addr[2], addr[1], addr[0]);
		break;
	default:
		return NULL;
	}
	return buf;
}

/* Return the first PTR record for the address */
char *query_ptr(int af, u_char *addr)
{
	char buf[128];
	int res;
	ns_msg handle;
	int i;
	ns_rr rr;

	if(!reverse_name(af, addr, buf, sizeof(buf)))
		return NULL;

	if((res = res_search(buf, ns_c_in, ns_t_ptr, dnsbuffer, sizeof(dnsbuffer))) < 0)
		return NULL;
	if(ns_initparse(dnsbuffer, res, &handle) < 0)
		return NULL;

	for(i = 0; i < ns_msg_count(handle, ns_s_an); i++) {
		if(ns_parserr(&handle, ns_s_an, i, &rr)) {
			/* parse error */
			return NULL;
		}
		if(ns_rr_type(rr) == ns_t_ptr) {
			char hname[MAXDNAME];
			if(ns_name_uncompress(ns_msg_base(handle), ns_msg_end(handle), ns_rr_rdata(rr), hname, sizeof(hname)) < 0)
				continue;
			return strdup(hname);
		}
	}
	return NULL;
}

struct rd_data_buf {
	char	*data;
	size_t	size;
};

static size_t wr_data(char *ptr, size_t size, size_t nmemb, void *userdata)
{
	struct rd_data_buf *db = (struct rd_data_buf *)userdata;
	if(!(db->data = realloc(db->data, db->size + size*nmemb)))
		return 0;
	memcpy(db->data + db->size, ptr, size*nmemb);
	db->size += size*nmemb;
	return size*nmemb;
}

static char *http_request(const char *uri, size_t *buflen)
{
	struct rd_data_buf db;
	CURL *curl = curl_easy_init();

	memset(&db, 0, sizeof(db));

	if(!curl)
		return NULL;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_NOPROGRESS, (int)1))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_NOSIGNAL, (int)1))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_WRITEDATA, &db))		goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, wr_data))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_FAILONERROR, (int)1))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_URL, uri))		goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, (int)1))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_MAXREDIRS, (int)30))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_MAXFILESIZE, (int)65536))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_WHATEVER))	goto cleanup;

	if(CURLE_OK != curl_easy_perform(curl)) {
		free(db.data);
		db.data = NULL;
	} else {
		*buflen = db.size;
	}

cleanup:
	curl_easy_cleanup(curl);
	return db.data;
}

static x509_crt_t *query_cert_common(const char *name, int *ncerts)
{
	int res;
	ns_msg handle;
	int i;
	ns_rr rr;
	array_t a;
	x509_crt_t crt;

	if(!name || !*name)
		return NULL;

	if((res = res_search(name, ns_c_in, ns_t_cert, dnsbuffer, sizeof(dnsbuffer))) < 0)
		return NULL;
	if(ns_initparse(dnsbuffer, res, &handle) < 0)
		return NULL;

	array_init(&a);

	for(i = 0; i < ns_msg_count(handle, ns_s_an); i++) {
		if(ns_parserr(&handle, ns_s_an, i, &rr)) {
			/* parse error, return what we got so far */
			*ncerts = a.nlist;
			return (x509_crt_t *)a.list;
		}
		if(ns_rr_type(rr) == ns_t_cert) {
			uint16_t certtype;
			/*uint16_t certkeytag;*/
			uint8_t certalgo;
			int certlen;
			const u_char *cp;
			char *pem;
			size_t pemlen = 0;
			char *uri;
			if(ns_rr_rdlen(rr) < 2+2+1+4)
				continue;	/* Must have content to handle */
			cp = ns_rr_rdata(rr);
			certtype = ns_get16(cp);
			cp += 2;
			/*certkeytag = ns_get16(cp);*/
			cp += 2;
			certalgo = *cp++;
			certlen = ns_rr_rdlen(rr) - 5;
			if(certalgo && certalgo != NS_ALG_RSASHA1)
				continue;	/* We want unspec or RSA/SHA1 certs */
			switch(certtype) {
			case cert_t_pkix:	/* DER formatted certificate */
				if((crt = crt_crt_load_bin((const char *)cp, certlen)))
					array_add(&a, crt);
				break;
			case cert_t_ipkix:	/* URL of the certificate */
				if(!(uri = malloc(certlen + 1)))
					continue;
				memcpy(uri, cp, certlen);
				uri[certlen] = '\0';
				pem = http_request(uri, &pemlen);
				free(uri);
				if(!pem || !pemlen)
					continue;
				if((crt = crt_crt_load(pem, pemlen)))
					array_add(&a, crt);
				free(pem);
				break;
			default:
				continue;
			}
		}
	}
	*ncerts = a.nlist;
	return (x509_crt_t *)a.list;
}

x509_crt_t *query_cert(const char *name, int *ncerts)
{
	char buf[MAXDNAME];

	if(!name || !*name || strlen(name) + sizeof("_pmdp._udp..") >= sizeof(buf))
		return NULL;

	/* Create the FQDN for the service; domname is made absolute if it has no trailing '.' */
	snprintf(buf, sizeof(buf), "_pmdp._udp.%s%s", name, name[strlen(name)-1] == '.' ? "" : ".");

	return query_cert_common(buf, ncerts);
}

x509_crt_t *query_cert_rev(int af, u_char *addr, int *ncerts)
{
	char buf[128];

	if(!addr || !ncerts)
		return NULL;

	if(!reverse_name(af, addr, buf, sizeof(buf)))
		return NULL;
	return query_cert_common(buf, ncerts);
}

