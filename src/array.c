/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include "array.h"

#define ARRAY_NALLOC	32

array_t *array_new(void)
{
	array_t *a;
	return calloc(1, sizeof(*a));
}

array_t *array_init(array_t *a)
{
	if(!a)
		return NULL;
	memset(a, 0, sizeof(*a));
	return a;
}

void *array_add(array_t *a, void *v)
{
	if(!a)
		return NULL;
	if(!a->list) {
		if(!(a->list = malloc(ARRAY_NALLOC * sizeof(*a->list))))
			return NULL;
		a->nalist = ARRAY_NALLOC;
		a->nlist = 0;
	} else if(a->nlist >= a->nalist) {
		void *ln = realloc(a->list, a->nalist * 2);
		if(!ln)
			return NULL;
		a->nalist *= 2;
		a->list = ln;
	}
	a->list[a->nlist++] = v;
	return v;
}

array_t *array_free_content(array_t *a)
{
	int i;
	if(!a)
		return NULL;
	if(a->list) {
		for(i = 0; i < a->nlist; i++)
			free(a->list[i]);
		free(a->list);
	}
	memset(a, 0, sizeof(*a));
	return a;
}

void array_free(array_t *a)
{
	array_free_content(a);
	free(a);
}

