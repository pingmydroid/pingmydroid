#/bin/bash

# PingMyDroid receive hook script
# Invoked by:
#  $ src/pmdrecv -q -x contrib/recv-hook.sh
#
# Arguments to this script:
# - Severity
# - Category
# - Sub-category
# - Timestamp
# - Flags
# - UUID
# - Sequence

# This wil simply print all the arguments
#echo "$@"

# Or you can play a song
/usr/bin/play -q /usr/share/gnome-games/sounds/gobble.ogg > /dev/null 2>&1 < /dev/null &
