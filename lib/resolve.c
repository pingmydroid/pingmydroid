/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <pmdp_priv.h>

#ifdef HAVE_LIBCARES
#include <ares.h>
#include <ares_dns.h>
#ifdef HAVE_ARPA_NAMESER_H
#include <arpa/nameser.h>
#else
#include <pmdp_nameser.h>
#endif
#endif

#ifdef HAVE_LIBCURL
#include <curl/curl.h>
#endif


/*
 * We need both direct and indirect DNS certificate type identifiers.
 * Many systems lack the indirect version. See RFC 4398 for details.
 */
#ifndef HAVE_CERT_T_PKIX
#define cert_t_pkix	1
#endif
#ifndef HAVE_CERT_T_IPKIX
#define cert_t_ipkix	4
#endif

#ifndef NS_ALG_RSASHA1
#define NS_ALG_RSASHA1	5
#endif

#include <pmdp.h>

struct rd_data_buf {
	char	*data;
	size_t	size;
};

static size_t wr_data(char *ptr, size_t size, size_t nmemb, void *userdata)
{
	struct rd_data_buf *db = (struct rd_data_buf *)userdata;
	if(!(db->data = realloc(db->data, db->size + size*nmemb)))
		return 0;
	memcpy(db->data + db->size, ptr, size*nmemb);
	db->size += size*nmemb;
	return size*nmemb;
}

static char *http_request(const char *uri, size_t *buflen)
{
#ifdef HAVE_LIBCURL
	struct rd_data_buf db;
	CURL *curl = curl_easy_init();

	memset(&db, 0, sizeof(db));

	if(!curl)
		return NULL;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_NOPROGRESS, (int)1))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_NOSIGNAL, (int)1))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_WRITEDATA, &db))		goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, wr_data))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_FAILONERROR, (int)1))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_URL, uri))		goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, (int)1))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_MAXREDIRS, (int)30))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_MAXFILESIZE, (int)65536))	goto cleanup;
	if(CURLE_OK != curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_WHATEVER))	goto cleanup;

	if(CURLE_OK != curl_easy_perform(curl)) {
		free(db.data);
		db.data = NULL;
	} else {
		*buflen = db.size;
	}

cleanup:
	curl_easy_cleanup(curl);
	return db.data;
#else
	(void)uri;
	(void)buflen;
	return NULL;
#endif
}

static char *reverse_name(const struct sockaddr *sa, socklen_t salen, char *buf, size_t bufsize)
{
	uint8_t *addr;
	if(!sa || !buf)
		return NULL;

	switch(sa->sa_family) {
	case AF_INET:
		if(salen < sizeof(struct sockaddr_in))
			return NULL;
		addr = (uint8_t *)&((struct sockaddr_in *)sa)->sin_addr;
		snprintf(buf, bufsize, "%u.%u.%u.%u.in-addr.arpa.", addr[3], addr[2], addr[1], addr[0]);
		break;
	case AF_INET6:
		if(salen < sizeof(struct sockaddr_in6))
			return NULL;
		addr = (uint8_t *)&((struct sockaddr_in6 *)sa)->sin6_addr;
		snprintf(buf, bufsize, "%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.ip6.arpa.",
			addr[15] & 0xf, addr[15] >> 4, addr[14] & 0xf, addr[14] >> 4,
			addr[13] & 0xf, addr[13] >> 4, addr[12] & 0xf, addr[12] >> 4,
			addr[11] & 0xf, addr[11] >> 4, addr[10] & 0xf, addr[10] >> 4,
			addr[9] & 0xf, addr[9] >> 4, addr[8] & 0xf, addr[8] >> 4,
			addr[7] & 0xf, addr[7] >> 4, addr[6] & 0xf, addr[6] >> 4,
			addr[5] & 0xf, addr[5] >> 4, addr[4] & 0xf, addr[4] >> 4,
			addr[3] & 0xf, addr[3] >> 4, addr[2] & 0xf, addr[2] >> 4,
			addr[1] & 0xf, addr[1] >> 4, addr[0] & 0xf, addr[0] >> 4);
		break;
	default:
		return NULL;
	}
	return buf;
}

/* libcares wrapped DNS lookups */
#ifdef HAVE_LIBCARES

static void check_aresinit(void)
{
	static int isinit = 0;
	if(!isinit) {
		if(ARES_SUCCESS != ares_library_init(ARES_LIB_INIT_ALL))
			abort();
		isinit = 1;
	}
}

struct dnsrr {
	char		rrname[NS_MAXDNAME];	/* Expanded labels */
	unsigned	rrnamelen;		/* Actual size, in case of embedded NUL char(s) */
	unsigned	rrtype;
	unsigned	rrclass;
	unsigned	rrttl;		/* Not used in question */
	unsigned	rrrdlength;	/* Not used in question */
	unsigned char	*rrrdata;	/* Not used in question */
};

struct dnscbdata {
	void		*buf;
	size_t		buflen;
	int		status;
	unsigned	qdcount;	/* Nr of questions */
	unsigned	ancount;	/* Nr of answer RRs */
	unsigned	nscount;	/* Nr of authority RRs */
	unsigned	arcount;	/* Nr of additional RRs */
	struct dnsrr	*qdrrs;		/* Only one allocated */
	struct dnsrr	*anrrs;
	struct dnsrr	*nsrrs;
	struct dnsrr	*arrrs;
};

typedef int (*__ares_query_result_cb_t)(const struct dnscbdata *result, void *arg);

struct do_ares_query_data {
	void	*ptr;
	__ares_query_result_cb_t cb;
	int	status;
	int	rv;
};

static inline int check_length(const unsigned char *base, const unsigned char *cur, int len, unsigned space)
{
	return (cur - base) + space <= (unsigned)len;
}

static int expand_name(const unsigned char *base, int len, unsigned char **buf, char *target, unsigned *targetlen)
{
	const unsigned char *lbuf = *buf;
	char *tptr = target;
	int seenptr = 0;

	*targetlen = 0;

	/* Must have at least one octet */
	if(!check_length(base, lbuf, len, 1))
		return ARES_EBADNAME;

	if(!*lbuf) {
		/* This is the root */
		*target++ = '.';
		*target = 0;
		(*buf)++;
		return 0;
	}

	while(1) {
		unsigned char c = *lbuf++;
		unsigned l;
		if(!seenptr)
			(*buf)++;
		switch(c & NS_CMPRSFLGS) {
		case 0x00:	/* Length of label */
			if(!c) {
				/* Root indicates end */
				if(*targetlen >= NS_MAXDNAME)
					return ARES_EBADNAME;
				*tptr = 0;	/* Zero terminate name */
				return 0;
			}
			if(!check_length(base, lbuf, len, c))
				return ARES_EBADNAME;
			while(c--) {
				if(!seenptr)
					(*buf)++;
				*tptr++ = (char)(*lbuf++);
				(*targetlen)++;
				if(*targetlen >= NS_MAXDNAME-1)
					return ARES_EBADNAME;
			}
			*tptr++ = '.';
			(*targetlen)++;
			if(*targetlen >= NS_MAXDNAME)
				return ARES_EBADNAME;
			break;
		case 0x40:	/* Deprecated binary label */
		case 0x80:	/* Invalid */
			return ARES_EBADNAME;
		case NS_CMPRSFLGS:	/* Pointer */
			/*
			 * Only one pointer allowed, except that the first may
			 * be pointer to other name. There can be multiple
			 * indirections. Bail out at 10 as it does not make
			 * sense to traverse the packet forever.
			 */
			if(!seenptr)
				(*buf)++;
			if(seenptr > 10)
				return ARES_EBADNAME;
			seenptr++;
			/* Must at least have one extra octet for pointer */
			if(!check_length(base, lbuf, len, 1))
				return ARES_EBADNAME;
			l = ((c & ~NS_CMPRSFLGS) << 8) + *lbuf;
			/* Check if we can see the pointer destination */
			if(!check_length(base, base, len, l))
				return ARES_EBADNAME;
			lbuf = base + l;	/* Go to pointer location */
			break;
		}
	}
}

static void dnscallback(void *arg, int status, int timeouts, unsigned char *abuf, int alen)
{
	struct do_ares_query_data *ptr = (struct do_ares_query_data *)arg;
	struct dnscbdata cbd;
	const unsigned char *base = abuf;
	unsigned  i;
	int err;

	(void)timeouts;

	memset(&cbd, 0, sizeof(cbd));

	cbd.buf = abuf;
	cbd.buflen = alen;

	ptr->rv = 0;

	if(ARES_SUCCESS != (ptr->status = status))
		return;

	if(alen < NS_HFIXEDSZ) {
		/* No room for header */
		ptr->status = ARES_EFORMERR;
		return;
	}

	if(DNS_HEADER_TC(abuf)) {
		/* We don't want truncated packets */
		ptr->status = ARES_EOF;
		return;
	}

	/* We don't want packets with errors */
	if(0 != (ptr->status = DNS_HEADER_RCODE(abuf)))
		return;

	/* The RR counts of each section */
	cbd.qdcount = DNS_HEADER_QDCOUNT(abuf);
	cbd.ancount = DNS_HEADER_ANCOUNT(abuf);
	cbd.nscount = DNS_HEADER_NSCOUNT(abuf);
	cbd.arcount = DNS_HEADER_ARCOUNT(abuf);

	cbd.qdrrs = malloc((cbd.qdcount + cbd.ancount + cbd.nscount + cbd.arcount) * sizeof(cbd.qdrrs[0]));
	cbd.anrrs = &cbd.qdrrs[cbd.qdcount];
	cbd.nsrrs = &cbd.qdrrs[cbd.qdcount + cbd.ancount];
	cbd.arrrs = &cbd.qdrrs[cbd.qdcount + cbd.ancount + cbd.nscount];

	abuf += NS_HFIXEDSZ;	/* Skip header */

	/* Expand all queries */
	for(i = 0; i < cbd.qdcount; i++) {
		if(0 != (err = expand_name(base, alen, &abuf, cbd.qdrrs[i].rrname, &cbd.qdrrs[i].rrnamelen))) {
			ptr->status = err;
			goto err_out;
		}
		if(!check_length(base, abuf, alen, NS_QFIXEDSZ)) {
			ptr->status = ARES_EFORMERR;
			goto err_out;
		}
		cbd.qdrrs[i].rrtype = DNS_QUESTION_TYPE(abuf);
		cbd.qdrrs[i].rrclass = DNS_QUESTION_CLASS(abuf);
		cbd.qdrrs[i].rrttl = 0;
		cbd.qdrrs[i].rrrdlength = 0;
		cbd.qdrrs[i].rrrdata = NULL;
		abuf += NS_QFIXEDSZ;
	}

	/* Expand all answers */
	for(i = 0; i < cbd.ancount; i++) {
		if(0 != (err = expand_name(base, alen, &abuf, cbd.anrrs[i].rrname, &cbd.anrrs[i].rrnamelen))) {
			ptr->status = err;
			goto err_out;
		}
		if(!check_length(base, abuf, alen, NS_RRFIXEDSZ)) {
			ptr->status = ARES_EFORMERR;
			goto err_out;
		}
		cbd.anrrs[i].rrtype = DNS_RR_TYPE(abuf);
		cbd.anrrs[i].rrclass = DNS_RR_CLASS(abuf);
		cbd.anrrs[i].rrttl = DNS_RR_TTL(abuf);
		cbd.anrrs[i].rrrdlength = DNS_RR_LEN(abuf);
		abuf += NS_RRFIXEDSZ;
		cbd.anrrs[i].rrrdata = abuf;
		if(!check_length(base, abuf, alen, cbd.anrrs[i].rrrdlength)) {
			ptr->status = ARES_EFORMERR;
			goto err_out;
		}
		abuf += cbd.anrrs[i].rrrdlength;
	}

	/* Expand all authority */
	for(i = 0; i < cbd.nscount; i++) {
		if(0 != (err = expand_name(base, alen, &abuf, cbd.nsrrs[i].rrname, &cbd.nsrrs[i].rrnamelen))) {
			ptr->status = err;
			goto err_out;
		}
		if(!check_length(base, abuf, alen, NS_RRFIXEDSZ)) {
			ptr->status = ARES_EFORMERR;
			goto err_out;
		}
		cbd.nsrrs[i].rrtype = DNS_RR_TYPE(abuf);
		cbd.nsrrs[i].rrclass = DNS_RR_CLASS(abuf);
		cbd.nsrrs[i].rrttl = DNS_RR_TTL(abuf);
		cbd.nsrrs[i].rrrdlength = DNS_RR_LEN(abuf);
		abuf += NS_RRFIXEDSZ;
		cbd.nsrrs[i].rrrdata = abuf;
		if(!check_length(base, abuf, alen, cbd.nsrrs[i].rrrdlength)) {
			ptr->status = ARES_EFORMERR;
			goto err_out;
		}
		abuf += cbd.nsrrs[i].rrrdlength;
	}

	/* Expand all additional */
	for(i = 0; i < cbd.arcount; i++) {
		if(0 != (err = expand_name(base, alen, &abuf, cbd.arrrs[i].rrname, &cbd.arrrs[i].rrnamelen))) {
			ptr->status = err;
			goto err_out;
		}
		if(!check_length(base, abuf, alen, NS_RRFIXEDSZ)) {
			ptr->status = ARES_EFORMERR;
			goto err_out;
		}
		cbd.arrrs[i].rrtype = DNS_RR_TYPE(abuf);
		cbd.arrrs[i].rrclass = DNS_RR_CLASS(abuf);
		cbd.arrrs[i].rrttl = DNS_RR_TTL(abuf);
		cbd.arrrs[i].rrrdlength = DNS_RR_LEN(abuf);
		abuf += NS_RRFIXEDSZ;
		cbd.arrrs[i].rrrdata = abuf;
		if(!check_length(base, abuf, alen, cbd.arrrs[i].rrrdlength)) {
			ptr->status = ARES_EFORMERR;
			goto err_out;
		}
		abuf += cbd.arrrs[i].rrrdlength;
	}

	/* Do callback */
	ptr->rv = ptr->cb(&cbd, ptr->ptr);

err_out:
	free(cbd.qdrrs);
}

static int do_ares_query(const char *name, int clss, int type, __ares_query_result_cb_t cb, void *arg)
{
	ares_channel channel;
	struct ares_options options;
	int rv = 0;
	struct do_ares_query_data dat;

	check_aresinit();
	dat.ptr = arg;
	dat.cb = cb;

	options.tries = 3;
	options.timeout = 3000;
	if(ARES_SUCCESS != ares_init_options(&channel, &options, ARES_OPT_TRIES | ARES_OPT_TIMEOUTMS))
		return PMDP_ERR_RESOLVE;

	ares_query(channel, name, clss, type, dnscallback, &dat);

	while(1) {
		fd_set read_fds, write_fds;
		int err;
		int nfds;
		struct timeval *tvp, tv;
		FD_ZERO(&read_fds);
		FD_ZERO(&write_fds);
		nfds = ares_fds(channel, &read_fds, &write_fds);
		if(!nfds)
			goto err_out;
		tvp = ares_timeout(channel, NULL, &tv);
		err = select(nfds, &read_fds, &write_fds, NULL, tvp);
		if(err < 0) {
			if(errno == EINTR)
				continue;
			rv = PMDP_ERR_RESOLVE;
			goto err_out;
		}
		ares_process(channel, &read_fds, &write_fds);
	}

	if(ARES_SUCCESS == dat.status)
		rv = dat.rv;	/* This comes from the callback */
	else
		rv = PMDP_ERR_RESOLVE;

err_out:
	ares_destroy(channel);
	return rv;
}

struct srv_by_name_data {
	void	*ptr;
	pmdp_fncb_addr_t cb;
};

static int srv_by_name_callback(const struct dnscbdata *result, void *arg)
{
	unsigned i;
	struct srv_by_name_data *cbdata = (struct srv_by_name_data *)arg;
	int cb = 0;

	/* Get the records from the answer section */
	for(i = 0; i < result->ancount; i++) {
		struct dnsrr *rr = &result->anrrs[i];
		char target[NS_MAXDNAME];
		unsigned targetlen;
		/*uint16_t priority;*/	/* We do not use priority/weight */
		/*uint16_t weight;*/
		uint16_t port;
		unsigned char *cp;
		struct addrinfo hints;
		struct addrinfo *ai, *aip;

		/* We only want SRV records in class IN */
		if(rr->rrtype != ns_t_srv || rr->rrclass != ns_c_in)
			continue;

		if(rr->rrrdlength < 2+2+2+1 || rr->rrrdlength > 2+2+2+sizeof(target)-1)
			continue;	/* Must have content to handle */
		cp = rr->rrrdata;
		/*priority = DNS__16BIT(cp);*/
		cp += 2;
		/*weight = DNS__16BIT(cp);*/
		cp += 2;
		port = DNS__16BIT(cp);
		cp += 2;
		if(expand_name(result->buf, result->buflen, &cp, target, &targetlen))
			continue;
		/*
		 * According to RFC2782:
		 * A Target of "." means that the service is decidedly
		 * not available at this domain.
		 *
		 * That means we quit the lookup when we see it.
		 */
		if(!targetlen || !strcmp(target, "."))
			return 0;

		/*printf("SRV - - %u %s\n", port, target);*/
		/*
		 * Here we have a hostname and a port. We might want to
		 * look at the additional section for addresses, but
		 * there is _no_guarantee_ that _all_ addresses are
		 * returned in the additional section due to space
		 * constraints. Therefore, we must do this manually to
		 * make sure that all addresses are returned.
		 */
		/* Now find the addresses of the target */
		memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_DGRAM;
		//hints.ai_flags = AI_PASSIVE;
		if(0 != getaddrinfo(target, NULL, &hints, &ai))
			continue;
		for(aip = ai; aip; aip = aip->ai_next) {
			switch(aip->ai_family) {
			case AF_INET:
				cb = 1;
				((struct sockaddr_in *)aip->ai_addr)->sin_port = htons(port);
				if(!cbdata->cb(aip->ai_addr, aip->ai_addrlen, cbdata->ptr)) {
					freeaddrinfo(ai);
					return 0;
				}
				break;
			case AF_INET6:
				cb = 1;
				((struct sockaddr_in6 *)aip->ai_addr)->sin6_port = htons(port);
				if(!cbdata->cb(aip->ai_addr, aip->ai_addrlen, cbdata->ptr)) {
					freeaddrinfo(ai);
					return 0;
				}
				break;
			}
		}
		freeaddrinfo(ai);
	}
	return cb ? 0 : PMDP_ERR_NOTFOUND;
}

struct ptr_by_addr_data {
	void	*ptr;
	pmdp_fncb_name_t cb;
};

static int ptr_by_addr_callback(const struct dnscbdata *result, void *arg)
{
	struct ptr_by_addr_data *cbdata = (struct ptr_by_addr_data *)arg;
	unsigned i;
	int cb = 0;

printf("ptrbyaddr\n");
	for(i = 0; i < result->ancount; i++) {
		struct dnsrr *rr = &result->anrrs[i];
		char target[NS_MAXDNAME];
		unsigned targetlen;
		unsigned char *cp;

		/* We only want PTR records in class IN */
		if(rr->rrtype != ns_t_ptr || rr->rrclass != ns_c_in)
			continue;

		cp = rr->rrrdata;
		if(expand_name(result->buf, result->buflen, &cp, target, &targetlen))
			continue;
		cb = 1;
		if(!cbdata->cb(target, targetlen, cbdata->ptr)) {
			return 0;
		}
	}
	return cb ? 0 : PMDP_ERR_NOTFOUND;
}

struct query_cert_common_data {
	void	*ptr;
	pmdp_fncb_crt_t cb;
};

static int query_cert_common_callback(const struct dnscbdata *result, void *arg)
{
	struct query_cert_common_data *cbdata = (struct query_cert_common_data *)arg;
	unsigned i;
	pmdp_x509_crt_t *crt;

	for(i = 0; i < result->ancount; i++) {
		struct dnsrr *rr = &result->anrrs[i];
		uint16_t certtype;
		/*uint16_t certkeytag;*/
		uint8_t certalgo;
		unsigned certlen;
		const unsigned char *cp;
		char *pem;
		size_t pemlen = 0;
		char *uri;

		/* We only want CERT records in class IN */
		if(rr->rrtype != ns_t_cert || rr->rrclass != ns_c_in)
			continue;
		if(rr->rrrdlength < 2+2+1+4)
			continue;	/* Must have content to handle */
		cp = rr->rrrdata;
		certtype = DNS__16BIT(cp);
		cp += 2;
		/*certkeytag = DNS__16BIT(cp);*/
		cp += 2;
		certalgo = *cp++;
		certlen = rr->rrrdlength - 5;
		if(certalgo && certalgo != NS_ALG_RSASHA1)
			continue;	/* We want unspec or RSA/SHA1 certs */
		switch(certtype) {
		case cert_t_pkix:	/* DER formatted certificate */
			if((crt = pmdp_x509_crt_load_der((const char *)cp, certlen))) {
				if(!cbdata->cb(crt, cbdata->ptr))
					pmdp_x509_crt_free(crt);
			}
			break;
		case cert_t_ipkix:	/* URL of the certificate */
			if(!(uri = malloc(certlen + 1)))
				continue;
			memcpy(uri, cp, certlen);
			uri[certlen] = '\0';
			pem = http_request(uri, &pemlen);
			free(uri);
			if(!pem || !pemlen)
				continue;
			if((crt = pmdp_x509_crt_load_pem(pem, pemlen))) {
				if(!cbdata->cb(crt, cbdata->ptr))
					pmdp_x509_crt_free(crt);
			}
			free(pem);
			break;
		default:
			continue;
		}
	}
	return 0;
}

/* Library entries for c-ares */

int pmdp_dns_srv_by_name(const char *domname, pmdp_fncb_addr_t callback, void *ptr)
{
	char buf[NS_MAXDNAME];
	struct srv_by_name_data dat;

	if(!domname || !*domname || strlen(domname) >= sizeof(buf) - sizeof("_pmdp._udp..") || !callback)
		return PMDP_ERR_INVARG;

	dat.ptr = ptr;
	dat.cb = callback;

	/* Create the FQDN for the service; domname is made absolute if it has no trailing '.' */
	snprintf(buf, sizeof(buf), "_pmdp._udp.%s%s", domname, domname[strlen(domname)-1] == '.' ? "" : ".");

	return do_ares_query(buf, ns_c_in, ns_t_srv, srv_by_name_callback, &dat);
}

int pmdp_dns_ptr_by_addr(const struct sockaddr *sa, int salen, pmdp_fncb_name_t callback, void *ptr)
{
	struct ptr_by_addr_data dat;
	char buf[128];

	dat.ptr = ptr;
	dat.cb = callback;

	if(!sa || !callback)
		return PMDP_ERR_INVARG;

	if(!reverse_name(sa, (socklen_t)salen, buf, sizeof(buf))) {
		return PMDP_ERR_RESOLVE;
	}

	return do_ares_query(buf, ns_c_in, ns_t_ptr, ptr_by_addr_callback, &dat);
}

static int query_cert_common(const char *name, pmdp_fncb_crt_t callback, void *ptr)
{
	struct query_cert_common_data dat;

	dat.ptr = ptr;
	dat.cb = callback;

	if(!name || !*name)
		return PMDP_ERR_INVARG;

	return do_ares_query(name, ns_c_in, ns_t_cert, query_cert_common_callback, &dat);
}

#else	/* do not have HAVE_LIBCARES */

#ifndef HAVE_RESOLV_H
#error "Need libresolv for DNS queries, or use c-ares"
#endif

/**
 * @brief Retrieve SRV records for domain name
 *
 * @param domname The domain name for associated SRV records
 * @param callback Callback function called for each SRV record found
 * @param ptr Argument passed to the callback function
 */
int pmdp_dns_srv_by_name(const char *domname, pmdp_fncb_addr_t callback, void *ptr)
{
	u_char *dnsbuffer;
	char buf[NS_MAXDNAME];
	int res;
	ns_msg handle;
	int i;
	int cb = 0;

	if(!domname || !*domname || strlen(domname) >= sizeof(buf) - sizeof("_pmdp._udp..") || !callback)
		return PMDP_ERR_INVARG;

	/* Create the FQDN for the service; domname is made absolute if it has no trailing '.' */
	snprintf(buf, sizeof(buf), "_pmdp._udp.%s%s", domname, domname[strlen(domname)-1] == '.' ? "" : ".");

	if(!(dnsbuffer = malloc(NS_MAXMSG)))
		return PMDP_ERR_NOMEM;

	if(0 >= (res = res_search(buf, ns_c_in, ns_t_srv, dnsbuffer, NS_MAXMSG))) {
		free(dnsbuffer);
		return PMDP_ERR_RESOLVE;
	}
	if(ns_initparse(dnsbuffer, res, &handle) < 0) {
		free(dnsbuffer);
		return PMDP_ERR_RESOLVE;
	}

	/* Get the records from the answer section */
	for(i = 0; i < ns_msg_count(handle, ns_s_an); i++) {
		ns_rr rr;
		if(ns_parserr(&handle, ns_s_an, i, &rr)) {
			free(dnsbuffer);
			return PMDP_ERR_RESOLVE;
		}
		if(ns_rr_type(rr) == ns_t_srv) {
			char target[NS_MAXDNAME];
			/*uint16_t priority;*/	/* We do not use priority/weight */
			/*uint16_t weight;*/
			uint16_t port;
			const u_char *cp;
			struct addrinfo hints;
			struct addrinfo *ai, *aip;
			if(ns_rr_rdlen(rr) < 2+2+2+1 || ns_rr_rdlen(rr) > 2+2+2+sizeof(target)-1)
				continue;	/* Must have content to handle */
			cp = ns_rr_rdata(rr);
			/*priority = DNS__16BIT(cp);*/
			cp += 2;
			/*weight = DNS__16BIT(cp);*/
			cp += 2;
			port = DNS__16BIT(cp);
			cp += 2;
			if(0 >= ns_name_uncompress(ns_msg_base(handle), ns_msg_end(handle), cp, target, sizeof(target)))
				continue;
			/*
			 * According to RFC2782:
			 * A Target of "." means that the service is decidedly
			 * not available at this domain.
			 *
			 * That means we quit the lookup when we see it.
			 */
			if(!strcmp(target, "."))
				goto quitit;

			/*printf("SRV - - %u %s\n", port, target);*/
			/*
			 * Here we have a hostname and a port. We might want to
			 * look at the additional section for addresses, but
			 * there is _no_guarantee_ that _all_ addresses are
			 * returned in the additional section due to space
			 * constraints. Therefore, we must do this manually to
			 * make sure that all addresses are returned.
			 */
			/* Now find the addresses of the target */
			memset(&hints, 0, sizeof(hints));
			hints.ai_family = AF_UNSPEC;
			hints.ai_socktype = SOCK_DGRAM;
			hints.ai_flags = AI_PASSIVE;
			if(0 != getaddrinfo(target, NULL, &hints, &ai))
				continue;
			for(aip = ai; aip; aip = aip->ai_next) {
				switch(aip->ai_family) {
				case AF_INET:
					cb = 1;
					((struct sockaddr_in *)aip->ai_addr)->sin_port = htons(port);
					if(!callback(aip->ai_addr, aip->ai_addrlen, ptr)) {
						freeaddrinfo(ai);
						goto quitit;
					}
					break;
				case AF_INET6:
					cb = 1;
					((struct sockaddr_in6 *)aip->ai_addr)->sin6_port = htons(port);
					if(!callback(aip->ai_addr, aip->ai_addrlen, ptr)) {
						freeaddrinfo(ai);
						goto quitit;
					}
					break;
				}
			}
			freeaddrinfo(ai);
		}
	}
quitit:
	free(dnsbuffer);
	return cb ? 0 : PMDP_ERR_NOTFOUND;
}

int pmdp_dns_ptr_by_addr(const struct sockaddr *sa, int salen, pmdp_fncb_name_t callback, void *ptr)
{
	u_char *dnsbuffer;
	char buf[128];
	int res;
	ns_msg handle;
	int i;
	ns_rr rr;
	int cb = 0;

	if(!sa || !callback)
		return PMDP_ERR_INVARG;

	if(!(dnsbuffer = malloc(NS_MAXMSG)))
		return PMDP_ERR_NOMEM;

	if(!reverse_name(sa, (socklen_t)salen, buf, sizeof(buf))) {
		free(dnsbuffer);
		return PMDP_ERR_RESOLVE;
	}

	if(0 >= (res = res_search(buf, ns_c_in, ns_t_ptr, dnsbuffer, NS_MAXMSG))) {
		free(dnsbuffer);
		return (h_errno == HOST_NOT_FOUND || h_errno == NO_DATA) ? PMDP_ERR_NOTFOUND : PMDP_ERR_RESOLVE;
	}
	if(ns_initparse(dnsbuffer, res, &handle) < 0) {
		free(dnsbuffer);
		return PMDP_ERR_RESOLVE;
	}

	for(i = 0; i < ns_msg_count(handle, ns_s_an); i++) {
		if(ns_parserr(&handle, ns_s_an, i, &rr)) {
			/* parse error */
			free(dnsbuffer);
			return PMDP_ERR_RESOLVE;
		}
		if(ns_rr_type(rr) == ns_t_ptr) {
			char hname[NS_MAXDNAME];
			if(ns_name_uncompress(ns_msg_base(handle), ns_msg_end(handle), ns_rr_rdata(rr), hname, sizeof(hname)) < 0)
				continue;
			cb = 1;
			if(!callback(hname, strlen(hname), ptr)) {
				free(dnsbuffer);
				return 0;
			}
		}
	}
	free(dnsbuffer);
	return cb ? 0 : PMDP_ERR_NOTFOUND;
}

static int query_cert_common(const char *name, pmdp_fncb_crt_t callback, void *ptr)
{
	u_char *dnsbuffer;
	int res;
	ns_msg handle;
	int i;
	ns_rr rr;
	pmdp_x509_crt_t *crt;

	if(!name || !*name)
		return PMDP_ERR_INVARG;

	if(!(dnsbuffer = malloc(NS_MAXMSG)))
		return PMDP_ERR_NOMEM;

	if(0 >= (res = res_search(name, ns_c_in, ns_t_cert, dnsbuffer, NS_MAXMSG))) {
		free(dnsbuffer);
		return (h_errno == HOST_NOT_FOUND || h_errno == NO_DATA) ? PMDP_ERR_NOTFOUND : PMDP_ERR_RESOLVE;
	}
	if(ns_initparse(dnsbuffer, res, &handle) < 0) {
		free(dnsbuffer);
		return PMDP_ERR_RESOLVE;
	}

	for(i = 0; i < ns_msg_count(handle, ns_s_an); i++) {
		if(ns_parserr(&handle, ns_s_an, i, &rr)) {
			/* parse error, return what we got so far */
			free(dnsbuffer);
			return PMDP_ERR_RESOLVE;
		}
		if(ns_rr_type(rr) == ns_t_cert) {
			uint16_t certtype;
			/*uint16_t certkeytag;*/
			uint8_t certalgo;
			int certlen;
			const u_char *cp;
			char *pem;
			size_t pemlen = 0;
			char *uri;
			if(ns_rr_rdlen(rr) < 2+2+1+4)
				continue;	/* Must have content to handle */
			cp = ns_rr_rdata(rr);
			certtype = DNS__16BIT(cp);
			cp += 2;
			/*certkeytag = DNS__16BIT(cp);*/
			cp += 2;
			certalgo = *cp++;
			certlen = ns_rr_rdlen(rr) - 5;
			if(certalgo && certalgo != NS_ALG_RSASHA1)
				continue;	/* We want unspec or RSA/SHA1 certs */
			switch(certtype) {
			case cert_t_pkix:	/* DER formatted certificate */
				if((crt = pmdp_x509_crt_load_der((const char *)cp, certlen))) {
					if(!callback(crt, ptr))
						pmdp_x509_crt_free(crt);
				}
				break;
			case cert_t_ipkix:	/* URL of the certificate */
				if(!(uri = malloc(certlen + 1)))
					continue;
				memcpy(uri, cp, certlen);
				uri[certlen] = '\0';
				pem = http_request(uri, &pemlen);
				free(uri);
				if(!pem || !pemlen)
					continue;
				if((crt = pmdp_x509_crt_load_pem(pem, pemlen))) {
					if(!callback(crt, ptr))
						pmdp_x509_crt_free(crt);
				}
				free(pem);
				break;
			default:
				continue;
			}
		}
	}
	free(dnsbuffer);
	return 0;
}

#endif	/* HAVE_LIBCARES */

int pmdp_dns_crt_by_name(const char *name, pmdp_fncb_crt_t callback, void *ptr)
{
	char buf[NS_MAXDNAME];

	if(!name || !*name || strlen(name) + sizeof("_pmdp._udp..") >= sizeof(buf) || !callback)
		return PMDP_ERR_INVARG;

	/* Create the FQDN for the service; domname is made absolute if it has no trailing '.' */
	snprintf(buf, sizeof(buf), "_pmdp._udp.%s%s", name, name[strlen(name)-1] == '.' ? "" : ".");

	return query_cert_common(buf, callback, ptr);
}

int pmdp_dns_crt_by_addr(const struct sockaddr *sa, int salen, pmdp_fncb_crt_t callback, void *ptr)
{
	char buf[128];

	if(!sa || !callback)
		return PMDP_ERR_INVARG;

	if(!reverse_name(sa, (socklen_t)salen, buf, sizeof(buf)))
		return PMDP_ERR_RESOLVE;
	return query_cert_common(buf, callback, ptr);
}
