/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __PMDP_PMDP_H
#define __PMDP_PMDP_H

#include <stdint.h>		/* For all basic types */
#include <sys/time.h>		/* For struct timeval */

struct sockaddr;

#if __BYTE_ORDER != __BIG_ENDIAN && __BYTE_ORDER != __LITTLE_ENDIAN
#error "Byte-sex error: Endian-type not supported"
#endif

#undef BEGIN_PMDP_C_DECLS
#undef END_PMDP_C_DECLS
#ifdef __cplusplus
#define BEGIN_PMDP_C_DECLS	extern "C" {
#define END_PMDP_C_DECLS	}
#else
#define BEGIN_PMDP_C_DECLS	/* empty */
#define END_PMDP_C_DECLS	/* empty */
#endif

#undef PMDP_PARAMS
#if defined(__STDC__) || defined(_AIX) || (defined(__mips) && defined(_SYSTYPE_SVR4)) || defined(WIN32) || defined(__cplusplus)
#define PMDP_PARAMS(protos)	protos
#else
#define PMDP_PARAMS(protos)	()
#endif

/* Default port and MC address */
#define PMDP_PORT 	48657		/* 0xbe11 */
#define PMDP_MCADDR	"239.255.0.1"
#define PMDP_MCADDR6	"ff14::1"

/** PMD packet version */
#define PMDP_PKT_VERSION_1	1
#define PMDP_PKT_VERSION_2	2
#define PMDP_PKT_VERSION	(PMDP_PKT_VERSION_2)	/* Current packet format version */


/** PMD library packet flags */
#define PMDF_FORCESILENT	0x00000001	/** Be silent (non-audible) when event is received */
#define PMDF_MASK		0x00000001	/** Encapsulate all defined flags */

/** PMD message content types */
#define PMDT_CATEGORY		0		/** 32 bit Category specification */
#define PMDT_SUBCATEGORY	1		/** 32 bit Subcategory specification */
#define PMDT_TEXT		2		/** Text in UTF-8 */
#define PMDT_RESOURCE		3		/** URI indicator */
#define PMDT_UUID		4		/** 128 bit UUID */
#define PMDT_TIMESTAMP		5		/** 64 bit Target timestamp, see structure below */
#define PMDT_CARDINAL		6		/** 32 bit unsigned number */
#define PMDT_INTEGER		7		/** 32 bit signed number */
#define PMDT_FRACTION		8		/** 64 bit signed dividend / unsigned divisor */
						/** types 10..49151: free for assignment */
						/** types 49152..65279: experimental (private) use */
						/** types 65280..65530: reserved */
#define PMDT_PADDING		65531		/** Padding bytes */
#define PMDT_KEY		65532		/** Encryption key */
#define PMDT_ENCRYPTED		65533		/** Encrypted content */
#define PMDT_FINGERPRINT	65534		/** Sender certificate fingerprint */
#define PMDT_SIGNATURE		65535		/** RSA signature (must be last content) */

/** Library internal message types */
#define PMDT_INT_MESSAGE	0x80000001	/** Message in a message */

/** Encryption algorithms */
#define PMDE_FB_CBC		0x0100
#define PMDE_FB_CFB		0x0200
#define PMDE_FB_MASK		0xff00

#define PMDE_ALGO_AES128	0x0001
#define PMDE_ALGO_AES192	0x0002
#define PMDE_ALGO_AES256	0x0003
#define PMDE_ALGO_CAMELLIA128	0x0004
#define PMDE_ALGO_CAMELLIA192	0x0005
#define PMDE_ALGO_CAMELLIA256	0x0006
#define PMDE_ALGO_MASK		0x00ff

#define PMDE_AES128_CBC		(PMDE_ALGO_AES128 | PMDE_FB_CBC)		/** AES in CBC mode with 128-bit keys */
#define PMDE_AES192_CBC		(PMDE_ALGO_AES192 | PMDE_FB_CBC)		/** AES in CBC mode with 192-bit keys */
#define PMDE_AES256_CBC		(PMDE_ALGO_AES256 | PMDE_FB_CBC)		/** AES in CBC mode with 256-bit keys */
#define PMDE_AES128_CFB		(PMDE_ALGO_AES128 | PMDE_FB_CFB)		/** AES in CFB mode with 128-bit keys */
#define PMDE_AES192_CFB		(PMDE_ALGO_AES192 | PMDE_FB_CFB)		/** AES in CFB mode with 192-bit keys */
#define PMDE_AES256_CFB		(PMDE_ALGO_AES256 | PMDE_FB_CFB)		/** AES in CFB mode with 256-bit keys */
#define PMDE_CAMELLIA128_CBC	(PMDE_ALGO_CAMELLIA128 | PMDE_FB_CBC)		/** CAMELLIA in CBC mode with 128-bit keys */
#define PMDE_CAMELLIA192_CBC	(PMDE_ALGO_CAMELLIA192 | PMDE_FB_CBC)		/** CAMELLIA in CBC mode with 192-bit keys */
#define PMDE_CAMELLIA256_CBC	(PMDE_ALGO_CAMELLIA256 | PMDE_FB_CBC)		/** CAMELLIA in CBC mode with 256-bit keys */
#define PMDE_CAMELLIA128_CFB	(PMDE_ALGO_CAMELLIA128 | PMDE_FB_CFB)		/** CAMELLIA in CFB mode with 128-bit keys */
#define PMDE_CAMELLIA192_CFB	(PMDE_ALGO_CAMELLIA192 | PMDE_FB_CFB)		/** CAMELLIA in CFB mode with 192-bit keys */
#define PMDE_CAMELLIA256_CFB	(PMDE_ALGO_CAMELLIA256 | PMDE_FB_CFB)		/** CAMELLIA in CFB mode with 256-bit keys */

/** PMD Severity indication */
#define PMDS_DEBUG		7		/** Debugging messages (normally ignored) */
#define PMDS_INFO		6		/** Informational messages */
#define PMDS_NOTICE		5		/** Noticable condition */
#define PMDS_WARNING		4		/** Warning conditions */
#define PMDS_ERROR		3		/** Error conditions */
#define PMDS_CRITICAL		2		/** Critical conditions */
#define PMDS_ALERT		1		/** Alert conditions */
#define PMDS_EMERGENCY		0		/** Emergency messages */

/** PMD message categories */
#define PMDC_LOCAL		0		/** Local scope for private use, often not signed */
#define PMDC_PRIVATE		(PMDC_LOCAL)
#define PMDC_MONITOR		1		/** Messages from monitor systems (e.g. Nagios) */
#define PMDC_LOG		2		/** Logging messages */
#define PMDC_TRAFFIC		3		/** Traffic announcements */
#define PMDC_PUBLIC		4		/** General public announcements */
#define PMDC_COMMERCIAL		128		/** Commercial interests */
#define PMDC_SERVICE		129		/** Services related messages */

/** Subcategory for PMDC_LOCAL */
#define PMDC_LOCAL_NONE			0	/** Subcategory ignored */
#define PMDC_LOCAL_BELL			1	/** Bell ring */
#define PMDC_LOCAL_DINNER		2	/** Dinner is ready, stop gaming and eat */

/** Subcategory for PMDC_MONITOR */
#define PMDC_MONITOR_NONE		0	/** Subcategory ignored */
#define PMDC_MONITOR_TEMPERATURE	1	/** Temperature event */

/** Subcategory for PMDC_SERVICE */
#define PMDC_SERVICE_NONE		0	/** Subcategory ignored */
#define PMDC_SERVICE_READYTOORDER	1	/** Restaurant service request */
#define PMDC_SERVICE_CHECKPLEASE	2	/** Restaurant service request */
#define PMDC_SERVICE_QUEUEREQUEST	257	/** Queue: request a number */
#define PMDC_SERVICE_QUEUEASSIGN	258	/** Queue: number assigment */
#define PMDC_SERVICE_QUEUESTATUS	259	/** Queue: next! */

#define PMDC_SUBCAT_PRIVATE	0x80000000	/** All subcats with high bit set are private values */

/** PMD library constants */
#define PMDP_UUID_SIZE		16		/** A UUID is 16 octets long */
#define PMDP_MAX_MSGSIZE	65535		/** Maximum size of a message */
#define PMDP_MAX_HASHSIZE	64		/** Fingerprint hash size (20 is enough for SHA-1, but we may change this at some point) */

/** PMD library error return values */
#define PMDP_ERR_NOMEM		(-1)		/** Memory allocation failed */
#define PMDP_ERR_INVARG		(-2)		/** Invalid argument */
#define PMDP_ERR_NORANDOM	(-3)		/** Random data not available */
#define PMDP_ERR_NOTUTF8	(-4)		/** String is not valid UTF-8 */
#define PMDP_ERR_BADKEY		(-5)		/** Invalid certificate key */
#define PMDP_ERR_SIZE		(-6)		/** Content size not correct */
#define PMDP_ERR_TOOLARGE	(-7)		/** Total content size too large to send */
#define PMDP_ERR_TOOSMALL	(-8)		/** Packet size too small to interpret */
#define PMDP_ERR_RESERVED	(-9)		/** Packet has reserved fields set to non-zero */
#define PMDP_ERR_VERSION	(-10)		/** Packet version mismatch */
#define PMDP_ERR_TYPE		(-11)		/** Packet contains unknown content type */
#define PMDP_ERR_TRAILING	(-12)		/** Packet contains trailing junk */
#define PMDP_ERR_FINGERPRINT	(-13)		/** Failed to calculate certificate fingerprint */
#define PMDP_ERR_SIGN		(-14)		/** Failed to calculate signature */
#define PMDP_ERR_VERIFY		(-15)		/** Failed to verify signature */
#define PMDP_ERR_CERTIFICATE	(-16)		/** Certificate error */
#define PMDP_ERR_ENCRYPT	(-17)		/** Encryption failed */
#define PMDP_ERR_DECRYPT	(-18)		/** Decryption failed */
#define PMDP_ERR_NOFILE		(-19)		/** File cannot be opened */
#define PMDP_ERR_RESOLVE	(-20)		/** Resolver DNS query failed */
#define PMDP_ERR_NOTFOUND	(-21)		/** Resolver DNS query gave no result */
#define PMDP_ERR_EXPIRED	(-22)		/** Certificate is expired or not yet valid */
#define PMDP_ERR_NOTIMPL	(-23)		/** Function not implemented */
#define PMDP_ERR_NOKEY		(-24)		/** No key found to decrypt message with */
#define PMDP_ERR_NOALGO		(-25)		/** Unsupported key algorithm */
#define PMDP_ERR_CRC		(-26)		/** CRC on key failed */

/** Library internal flags */
#define PMDP_INTF_REFSIG	0x01000000	/** Set if pmdp_msg_t->signature is a reference */
#define PMDP_INTF_REFFP		0x02000000	/** Set if pmdp_msg_t->fingerprint is a reference */
#define PMDP_INTF_REFPKT	0x04000000	/** Set if pmdp_msg_t->pkt is a reference */
#define PMDP_INTF_NOHDR		0x08000000	/** Set if pmdp_msg_t->pkt is only content (from excrypted) */

/** Parser flags */
#define PMDP_PARSE_UTYPES	0x00000001	/** Allow unknown content types to be parsed */
#define PMDP_PARSE_TRAILING	0x00000002	/** Allow trailing junk in a packet */
#define PMDP_PARSE_BADUTF8	0x00000004	/** Allow bad utf-8 strings */
#define PMDP_PARSE_HAVESIG	0x00010000	/** Parser found a fingerprint/signature at bottom */
#define PMDP_PARSE_HAVEENCR	0x00020000	/** Parser found (interpretable) encrypted content */
#define PMDP_PARSE_HAVEMULTI	0x00040000	/** Parser found multi-event content */
#define PMDP_PARSE_VALIDSIG	0x00080000	/** The signature is valid */
#define PMDP_PARSE_VALIDFP	0x00100000	/** The signature is associated with a fingerprint */
#define PMDP_PARSE_VALIDUTF8	0x00200000	/** The string is valid UTF-8 */

/** PMD packet timestamp format */
typedef struct __pmdp_pkt_timestamp_t {
	uint8_t		usecsup;		/**< b12..b19 of usecs */
	uint8_t		usecshi;		/**< b4..b11 of usecs */
	uint8_t		usecslo;		/**< b4..b11 of usecs in high nibble and reserved in low nibble */
	uint8_t		secshi;			/**< b32..b39 of time_t if it is 64 bit */
/* Warning: Bitfields on 32 bit types are aparently not reliable */
#if 0
	struct {
#if __BYTE_ORDER == __BIG_ENDIAN
		uint32_t	usecs:20;	/**< Sender timestamp; microseconds [0..999999] */
		uint32_t	reserved:4;	/**< Reserved; must be 0 */
		uint32_t	secshi:8;	/**< Sender timestamp high 8 bits; UNIX Epoch */
#else
		uint32_t	secshi:8;
		uint32_t	reserved:4;
		uint32_t	usecs:20;
#endif
	};
#endif
	uint32_t        secslo;			/**< Sender timestamp low 32 bits; UNIX Epoch */
} pmdp_pkt_timestamp_t;


/** PMD packet fractional number
 * Value of dividend divided by divosor. Plus/minus infinity is defined by
 * setting divisor to 0 (zero) and the dividend to a positive or negative
 * value.  Not-a-number (NAN) is defined by setting both dividend and divisor
 * to 0 (zero).
 */
typedef struct __pmdp_fraction_t {
	int32_t		dividend;	/**< Dividend of the fraction */
	uint32_t	divisor;	/**< Divisor of the fraction */
} pmdp_fraction_t;


/** PMD packet header */
typedef struct __pmdp_pkt_hdr_t {
	uint8_t		version;			/**< Protocol version, currently 0x02 */
	union {
		struct {
#if __BYTE_ORDER == __BIG_ENDIAN
			uint8_t	severity:3;		/**< Message severity (PMDS_*) */
			uint8_t	forcesilent:1;		/**< Force silent flag */
			uint8_t	sequence:4;		/**< Packet repeat send sequence */
#else
			uint8_t	sequence:4;
			uint8_t	forcesilent:1;
			uint8_t	severity:3;
#endif
		};
	};
	uint16_t		reserved;		/**< Reserved, must be 0 */
	pmdp_pkt_timestamp_t	timestamp;		/**< Timestamp of message */
	uint8_t			uuid[PMDP_UUID_SIZE];	/**< UUID of message */
	/* pmdp_pkt_content_t messages[0..n] */
} pmdp_pkt_hdr_t;


/** PMD packet message content */
typedef struct __pmdp_pkt_content_t {
	uint16_t	ctype;				/**< Message content type (PMDT_*) */
	uint16_t	clen;				/**< Message content length */
	uint8_t		bytes[];			/**< Message content data of size 'clen' */
} pmdp_pkt_content_t;


/** PMD packet key content */
typedef struct __pmdp_pkt_key_t {
	uint16_t	crc;				/**< Decryption check value; CRC-CCITT of rest of packet */
	uint16_t	type;				/**< Encryption type (PMDE_*) */
	uint8_t		octets[];			/**< Key and IV data */
} pmdp_pkt_key_t;

/* Forward declaration */
struct __pmdp_msg_part_t;

/** Encrypted content part */
typedef struct __pmdp_encrypted_t {
	struct __pmdp_msg_part_t	*key;		/**< Reference to the PMDT_KEY content */
	struct __pmdp_msg_part_t	*fingerprint;	/**< Reference to the PMDT_FINGERPRINT content */
	uint8_t				octets[1];	/**< Actual encrypted content */
} pmdp_encrypted_t;

/** PMD library message content */
typedef struct __pmdp_msg_part_t {
	uint32_t		type;			/**< Message content type */
	size_t			length;			/**< Message length (should match the content) */
	uint32_t		flags;			/**< Flags validating the content */
	union {
		uint32_t		category;	/**< Category (PMDT_CATEGORY) */
		uint32_t		subcategory;	/**< Sub-category (PMDT_SUBCATEGORY) */
		uint32_t		cardinal;	/**< Unsiged 32-bit value (PMDT_CARDINAL) */
		int32_t			integer;	/**< Signed 32-bit value (PMDT_INTEGER) */
		uint8_t			uuid[PMDP_UUID_SIZE];	/**< Binary UUID (PMDT_UUID) */
		struct timeval		timestamp;	/**< Microsecond timestamp (PMDT_TIMESTAMP) */
		pmdp_fraction_t		fraction;	/**< Fractional number (PMDT_FRACTION) */
		pmdp_pkt_key_t		key;		/**< Encryted data key+iv (PMDT_KEY) */
		pmdp_encrypted_t	encrypted;	/**< Encrypted data container */
		char			string[1];	/**< String types text and uri */
		uint8_t			octets[1];	/**< Binary types uuid, padding, key, encrypted, fingerprint and signature */
	};
} pmdp_msg_part_t;


/** PMD library message wrapper */
typedef struct __pmdp_msg_t {
	unsigned	version;			/**< Protocol version */
	unsigned	severity;			/**< Message severity (PMDS_*) */
	unsigned	sequence;			/**< Message sequence number */
	struct timeval	timestamp;			/**< Message sender's timestamp */
	uint8_t		uuid[PMDP_UUID_SIZE];		/**< Message UUID */
	uint32_t	flags;				/**< Message flags */
	uint32_t	intflags;			/**< Library internal flags */
	pmdp_msg_part_t	**parts;			/**< (De-)Constructed content parts */
	unsigned	nparts;				/**< Number of content parts */
	unsigned	naparts;			/**< Array allocation count */

	pmdp_msg_part_t	*fingerprint;			/**< Fingerprint of signature */
	pmdp_msg_part_t	*signature;			/**< Signature data */

	pmdp_pkt_hdr_t	*pkt;				/**< Generated message data */
	size_t		pktlen;				/**< Generated message packet length */
	uint8_t		*sigtag;			/**< Position of signature content within packet */
} pmdp_msg_t;

/* Certificate handling */
/* These definitions will hide the backing primitives so we can transparently
 * change backends if we want to
 */
struct __pmdp_x509_crt_t;
struct __pmdp_x509_key_t;

typedef struct __pmdp_x509_crt_t pmdp_x509_crt_t;	/** Opaque X509 certificate structure */
typedef struct __pmdp_x509_key_t pmdp_x509_key_t;	/** Opaque X509 key structure */


BEGIN_PMDP_C_DECLS

typedef int (*pmdp_fncb_crt_t) PMDP_PARAMS( (pmdp_x509_crt_t *, void *) );	/** Callback for certificates from pmdp_x509_crt_bundle_load_file(), pmdp_dns_crt_by_name() and pmdp_dns_crt_by_addr() */
typedef int (*pmdp_fncb_addr_t) PMDP_PARAMS( (const struct sockaddr *, int, void *) );	/** Callback for DNS addresses from pmdp_dns_srv_by_name() */
typedef int (*pmdp_fncb_name_t) PMDP_PARAMS( (const char *, size_t, void *) );	/** Callback for reverse DNS resolution from pmdp_dns_ptr_by_addr() */
typedef int (*pmdp_fncb_parts_t) PMDP_PARAMS( (const pmdp_msg_t *, const pmdp_msg_part_t *part, void *) );	/** Callback for message content part iteration with pmdp_msg_iterate_parts() */
typedef int (*pmdp_fncb_passwd_t) PMDP_PARAMS( (char *, int, void *) );		/** Callback for password at x509 private key load */

/* Library API interface */
pmdp_msg_t *pmdp_msg_new PMDP_PARAMS( (void) );
void pmdp_msg_free PMDP_PARAMS( (pmdp_msg_t *msg) );
const char *pmdp_strerror PMDP_PARAMS( (int err) );
uint16_t pmdp_crc_ccitt PMDP_PARAMS( (const uint8_t *buf, size_t len) );

int pmdp_msg_add_raw PMDP_PARAMS( (pmdp_msg_t *msg, pmdp_msg_part_t *part) );
int pmdp_msg_add_string PMDP_PARAMS( (pmdp_msg_t *msg, uint32_t type, const char *txt, size_t len) );
int pmdp_msg_add_binary PMDP_PARAMS( (pmdp_msg_t *msg, uint32_t type, const void *data, size_t datalen) );

int pmdp_msg_add_int32 PMDP_PARAMS( (pmdp_msg_t *msg, int32_t i) );
int pmdp_msg_add_uint32 PMDP_PARAMS( (pmdp_msg_t *msg, uint32_t u) );
int pmdp_msg_add_category PMDP_PARAMS( (pmdp_msg_t *msg, uint32_t cat) );
int pmdp_msg_add_subcategory PMDP_PARAMS( (pmdp_msg_t *msg, uint32_t subcat) );
int pmdp_msg_add_fraction PMDP_PARAMS( (pmdp_msg_t *msg, int32_t dividend, uint32_t divisor) );
int pmdp_msg_add_text PMDP_PARAMS( (pmdp_msg_t *msg, const char *txt) );
int pmdp_msg_add_resource PMDP_PARAMS( (pmdp_msg_t *msg, const char *txt) );
int pmdp_msg_add_uuid PMDP_PARAMS( (pmdp_msg_t *msg, const uint8_t *uuid) );
int pmdp_msg_add_timestamp PMDP_PARAMS( (pmdp_msg_t *msg, const struct timeval *tv) );
int pmdp_msg_add_padding PMDP_PARAMS( (pmdp_msg_t *msg, const void *data, size_t datalen) );
int pmdp_msg_add_encrypted PMDP_PARAMS( (pmdp_msg_t *msg, pmdp_msg_t *encr, pmdp_x509_crt_t *crt, int method) );

int pmdp_msg_set_severity PMDP_PARAMS( (pmdp_msg_t *msg, unsigned severity) );
int pmdp_msg_get_severity PMDP_PARAMS( (const pmdp_msg_t *msg, unsigned *severity) );
int pmdp_msg_set_sequence PMDP_PARAMS( (pmdp_msg_t *msg, unsigned sequence) );
int pmdp_msg_get_sequence PMDP_PARAMS( (const pmdp_msg_t *msg, unsigned *sequence) );
int pmdp_msg_set_flags PMDP_PARAMS( (pmdp_msg_t *msg, uint32_t flags) );
int pmdp_msg_get_flags PMDP_PARAMS( (const pmdp_msg_t *msg, uint32_t *flags) );
int pmdp_msg_set_uuid PMDP_PARAMS( (pmdp_msg_t *msg, uint8_t *uuid) );
int pmdp_msg_get_uuid PMDP_PARAMS( (pmdp_msg_t *msg, uint8_t *uuid) );
int pmdp_msg_set_timestamp PMDP_PARAMS( (pmdp_msg_t *msg, const struct timeval *tv) );
int pmdp_msg_get_timestamp PMDP_PARAMS( (const pmdp_msg_t *msg, struct timeval *tv) );
int pmdp_msg_get_version PMDP_PARAMS( (const pmdp_msg_t *msg, unsigned *version) );

int pmdp_msg_generate PMDP_PARAMS( (pmdp_msg_t *msg, const pmdp_x509_crt_t *sigcrt, const pmdp_x509_key_t *sigkey, void **pdata, size_t *plen) );
int pmdp_msg_parse PMDP_PARAMS( (void *data, size_t datalen, pmdp_msg_t **pmsg, unsigned *piflags) );
int pmdp_msg_parse_encrypted PMDP_PARAMS( (const pmdp_x509_key_t *pkey, const pmdp_msg_part_t *epart, pmdp_msg_t **pmsg, unsigned *piflags) );
pmdp_msg_part_t *pmdp_msg_get_fingerprint PMDP_PARAMS( (const pmdp_msg_t *msg) );
int pmdp_msg_verify PMDP_PARAMS( (const pmdp_msg_t *msg, const pmdp_x509_crt_t *crt) );
int pmdp_msg_iterate_parts PMDP_PARAMS( (const pmdp_msg_t *msg, pmdp_fncb_parts_t callback, void *ptr) );

int pmdp_x509_privkeysize PMDP_PARAMS( (const pmdp_x509_key_t *crt) );
int pmdp_x509_pubkeysize PMDP_PARAMS( (const pmdp_x509_crt_t *crt) );
int pmdp_x509_fingerprint PMDP_PARAMS( (const pmdp_x509_crt_t *crt, uint8_t **buf, size_t *bufsize) );
int pmdp_x509_sign PMDP_PARAMS( (const pmdp_x509_key_t *pkey, const void *data, size_t datalen, void *sig, size_t *siglen) );
int pmdp_x509_verify PMDP_PARAMS( (const pmdp_x509_crt_t *crt, const void *data, size_t datalen, void *sig, size_t siglen) );

pmdp_x509_crt_t *pmdp_x509_crt_load_der PMDP_PARAMS( (const void *buf, size_t bufsize) );
pmdp_x509_crt_t *pmdp_x509_crt_load_pem PMDP_PARAMS( (const void *buf, size_t bufsize) );
pmdp_x509_crt_t *pmdp_x509_crt_load_file PMDP_PARAMS( (const char *fname) );
int pmdp_x509_crt_bundle_load_file PMDP_PARAMS( (const char *fname, pmdp_fncb_crt_t callback, void *ptr) );
int pmdp_x509_crt_write PMDP_PARAMS( (const pmdp_x509_crt_t *crt, void **buf, size_t *bufsize) );
void pmdp_x509_crt_free PMDP_PARAMS( (pmdp_x509_crt_t *crt) );

pmdp_x509_key_t *pmdp_x509_key_load_pem PMDP_PARAMS( (const char *buf, size_t bufsize, pmdp_fncb_passwd_t cb, void *arg) );
pmdp_x509_key_t *pmdp_x509_key_load_file PMDP_PARAMS( (const char *fname, pmdp_fncb_passwd_t cb, void *arg) );
void pmdp_x509_key_free PMDP_PARAMS( (pmdp_x509_key_t *pkey) );

int pmdp_x509_encrypt PMDP_PARAMS( (const pmdp_x509_crt_t *crt, const uint8_t *data, size_t datalen, uint8_t **encr, size_t *encrlen) );
int pmdp_x509_decrypt PMDP_PARAMS( (const pmdp_x509_key_t *key, const uint8_t *data, size_t datalen, uint8_t **decr, size_t *decrlen) );

int pmdp_dns_crt_by_name PMDP_PARAMS( (const char *domname, pmdp_fncb_crt_t callback, void *ptr) );
int pmdp_dns_crt_by_addr PMDP_PARAMS( (const struct sockaddr *sa, int salen, pmdp_fncb_crt_t callback, void *ptr) );
int pmdp_dns_srv_by_name PMDP_PARAMS( (const char *domname, pmdp_fncb_addr_t callback, void *ptr) );
int pmdp_dns_ptr_by_addr PMDP_PARAMS( (const struct sockaddr *sa, int salen, pmdp_fncb_name_t callback, void *ptr) );

int pmdp_uuid_parse PMDP_PARAMS( (const char *str, uint8_t *uu) );
void pmdp_uuid_unparse PMDP_PARAMS( (const uint8_t *uu, char *str) );
void pmdp_uuid_generate PMDP_PARAMS( (uint8_t *uu) );

int pmdp_random_bytes PMDP_PARAMS( (uint8_t *buf, size_t buflen) );

END_PMDP_C_DECLS

#endif
