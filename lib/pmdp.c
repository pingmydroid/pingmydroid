/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <pmdp_priv.h>

#ifdef USE_ICONV
#include <iconv.h>
#endif

#if defined(HAVE_LIBCRYPTO)
#include <openssl/rand.h>
#include <openssl/evp.h>
#elif defined (HAVE_LIBGNUTLS)
#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>
#else
#error "No crypto library available, please use openssl or gnutls"
#endif

#include <pmdp.h>


/** \file pmdp.c
 * Main PMDP library interface functions
 * Message container, parsing and construction functions for PMDP messages.
 */

#ifdef USE_ICONV
static void *memdup(const void *p, size_t len)
{
	void *np;
	if(!len)
		return NULL;
	if((np = malloc(len)))
		memcpy(np, p, len);
	return np;
}
#endif

static void pmdp_timeval_to_timestamp(const struct timeval *tv, pmdp_pkt_timestamp_t *ts)
{
	if(!tv || !ts)
		return;
	/*
	 * The usecs field is awkward because it is a bitfield that cannot be
	 * handled properly by hton/ntoh routines. However, we know the
	 * organisation in bits and do the setting manually.
	 * The lowest byte address has the MSByte in big-endian (network
	 * order). This holds true also for non-byte-modulo-sized fields.
	 */
	ts->usecsup = tv->tv_usec >> 12;
	ts->usecshi = tv->tv_usec >> 4;
	ts->usecslo = tv->tv_usec << 4;
	ts->secslo = htobe32(tv->tv_sec);
#if SIZEOF_TIME_T > 4
	ts->secshi = (uint8_t)(tv->tv_sec >> 32);
#else
	ts->secshi = 0;		/* FIXME: Well, it will fail in 2038 for 32 bit signed time_t */
#warning "time_t is too small for epochs after 2038, needs to be fixed"
#endif
}

static void pmdp_timestamp_to_timeval(const pmdp_pkt_timestamp_t *ts, struct timeval *tv)
{
	if(!ts || ! tv)
		return;
	/* See usecs comment above */
	tv->tv_usec = ((unsigned)ts->usecsup << 12) + ((unsigned)ts->usecshi << 4) + ((unsigned)ts->usecslo >> 4);
	if(tv->tv_usec > 999999)
		tv->tv_usec = 0;
	tv->tv_sec = (time_t)be32toh(ts->secslo);
#if SIZEOF_TIME_T > 4
	tv->tv_sec += (time_t)ts->secshi << 32;
#else
	/* FIXME: We cannot represent epochs after 2038 for 32 bit signed time_t */
#warning "time_t is too small for epochs after 2038, needs to be fixed"
#endif
}

/* Algorithm's key and IV sizes */
static size_t pmdp_keyiv_size(unsigned algo)
{
	switch(algo & PMDE_ALGO_MASK) {
	case PMDE_ALGO_AES128:
	case PMDE_ALGO_CAMELLIA128:
		return 128/8;
	case PMDE_ALGO_AES192:
	case PMDE_ALGO_CAMELLIA192:
		return 192/8;
	case PMDE_ALGO_AES256:
	case PMDE_ALGO_CAMELLIA256:
		return 256/8;
	default:
		return 0;
	}
}

#if defined(HAVE_LIBCRYPTO)
/* Algorithm's key function reference */
static const EVP_CIPHER *pmdp_key_algo(unsigned algo)
{
	switch(algo) {
	case PMDE_AES128_CBC:		return EVP_aes_128_cbc();
	case PMDE_AES192_CBC:		return EVP_aes_192_cbc();
	case PMDE_AES256_CBC:		return EVP_aes_256_cbc();
	case PMDE_AES128_CFB:		return EVP_aes_128_cfb();
	case PMDE_AES192_CFB:		return EVP_aes_192_cfb();
	case PMDE_AES256_CFB:		return EVP_aes_256_cfb();
	case PMDE_CAMELLIA128_CBC:	return EVP_aes_128_cbc();
	case PMDE_CAMELLIA192_CBC:	return EVP_aes_192_cbc();
	case PMDE_CAMELLIA256_CBC:	return EVP_aes_256_cbc();
	case PMDE_CAMELLIA128_CFB:	return EVP_aes_128_cfb();
	case PMDE_CAMELLIA192_CFB:	return EVP_aes_192_cfb();
	case PMDE_CAMELLIA256_CFB:	return EVP_aes_256_cfb();
	default:			return NULL;
	}
}
#elif defined(HAVE_LIBGNUTLS)
static int pmdp_key_algo(unsigned algo)
{
	switch(algo) {
	case PMDE_AES128_CBC:		return GNUTLS_CIPHER_AES_128_CBC;
	case PMDE_AES192_CBC:		return GNUTLS_CIPHER_AES_192_CBC;
	case PMDE_AES256_CBC:		return GNUTLS_CIPHER_AES_256_CBC;
	case PMDE_AES128_CFB:		return GNUTLS_CIPHER_AES128_PGP_CFB;
	case PMDE_AES192_CFB:		return GNUTLS_CIPHER_AES192_PGP_CFB;
	case PMDE_AES256_CFB:		return GNUTLS_CIPHER_AES256_PGP_CFB;
	case PMDE_CAMELLIA128_CBC:	return GNUTLS_CIPHER_CAMELLIA_128_CBC;
	case PMDE_CAMELLIA192_CBC:	return GNUTLS_CIPHER_CAMELLIA_192_CBC;
	case PMDE_CAMELLIA256_CBC:	return GNUTLS_CIPHER_CAMELLIA_256_CBC;
	case PMDE_CAMELLIA128_CFB:	return -1;	/* Not supported by GnuTLS */
	case PMDE_CAMELLIA192_CFB:	return -1;	/* Not supported by GnuTLS */
	case PMDE_CAMELLIA256_CFB:	return -1;	/* Not supported by GnuTLS */
	default:			return 0;
	}
}
#endif

/* Table of possible CRC-CCITT values for lower 8 bits */
static const uint16_t crc_ccitt_table[256] = {
	0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf, 0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
	0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e, 0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
	0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd, 0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
	0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c, 0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
	0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb, 0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
	0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a, 0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
	0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9, 0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
	0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738, 0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
	0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7, 0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
	0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036, 0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
	0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5, 0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
	0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134, 0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
	0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3, 0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
	0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232, 0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
	0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1, 0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
	0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330, 0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};

/*! Standard CCITT CRC calculation
 *
 * \param buf Data buffer
 * \param len Data length
 *
 * Returns the CCITT CRC on buf with initialization -1.
 *
 * The CCITT polynomial is X^16 + x^12 + x^5 + 1.
 */
uint16_t pmdp_crc_ccitt(const uint8_t *buf, size_t len)
{
	uint16_t crc = (uint16_t)-1;
	if(!buf)
		return crc;
	while(len--)
		crc = (crc >> 8) ^ crc_ccitt_table[(crc ^ *buf++) & 0xff];
	return crc;
}

/*! Convert pmdp library error into readable string
 *
 * \param err Error returned by a library call
 *
 */
const char *pmdp_strerror(int err)
{
	switch(err) {
	case PMDP_ERR_NOMEM:		return "Memory allocation failed";
	case PMDP_ERR_INVARG:		return "Invalid argument";
	case PMDP_ERR_NORANDOM:		return "Random data not available";
	case PMDP_ERR_NOTUTF8:		return "String not valid UTF-8";
	case PMDP_ERR_BADKEY:		return "Invalid certificate key";
	case PMDP_ERR_SIZE:		return "Content size not correct";
	case PMDP_ERR_TOOLARGE:		return "Total content size too large to send";
	case PMDP_ERR_TOOSMALL:		return "Packet size too small to interpret";
	case PMDP_ERR_RESERVED:		return "Packet has reserved fields set to non-zero";
	case PMDP_ERR_VERSION:		return "Packet version mismatch";
	case PMDP_ERR_TYPE:		return "Packet contains unknown content type";
	case PMDP_ERR_TRAILING:		return "Packet contains trailing junk";
	case PMDP_ERR_FINGERPRINT:	return "Failed to calculate certificate fingerprint";
	case PMDP_ERR_SIGN:		return "Failed to calculate signature";
	case PMDP_ERR_VERIFY:		return "Failed to verify signature";
	case PMDP_ERR_CERTIFICATE:	return "Certificate error";
	case PMDP_ERR_ENCRYPT:		return "Encryption failed";
	case PMDP_ERR_DECRYPT:		return "Decryption failed";
	case PMDP_ERR_NOFILE:		return "File cannot be opened";
	case PMDP_ERR_RESOLVE:		return "Resolver DNS query failed";
	case PMDP_ERR_NOTFOUND:		return "Resolver DNS query gave no result";
	case PMDP_ERR_EXPIRED:		return "Certificate is expired or not yet valid";
	case PMDP_ERR_NOTIMPL:		return "Function not implemented";
	case PMDP_ERR_NOKEY:		return "Missing decryption key";
	case PMDP_ERR_NOALGO:		return "Unsupported key algorithm";
	case PMDP_ERR_CRC:		return "CRC on key failed";
	default: 			return "Unknown error";
	}
}

/*! Create a new message container structure
 *
 * Returns the newly created message.
 * The following fields are initialized:
 * - version	PMD_VERSION
 * - uuid	new random generated UUID
 * - timestamp	'now'
 * - severity	PMDS_INFO
 * - sequence	0 (zero)
 * - flags	none
 */
pmdp_msg_t *pmdp_msg_new(void)
{
	pmdp_msg_t *m = (pmdp_msg_t *)calloc(1, sizeof(pmdp_msg_t));
	if(m) {
		m->version = PMDP_PKT_VERSION;
		m->severity = PMDS_INFO;
		pmdp_uuid_generate(m->uuid);
		gettimeofday(&m->timestamp, NULL);
	}
	return m;
}

/*! Destroy a message container structure
 *
 * \param msg Reference of the message to destroy
 *
 * All memory associated with the message is freed as is the message container
 * itself. References to user memory are not freed.
 */
void pmdp_msg_free(pmdp_msg_t *msg)
{
	if(!msg)
		return;
	if(msg->parts) {
		int i;
		for(i = 0; i < msg->nparts; i++)
			free(msg->parts[i]);
		free(msg->parts);
	}
	if(!(msg->intflags & PMDP_INTF_REFFP))
		free(msg->fingerprint);
	if(!(msg->intflags & PMDP_INTF_REFSIG))
		free(msg->signature);
	if(!(msg->intflags & PMDP_INTF_REFPKT))
		free(msg->pkt);
	free(msg);
}

/*! Message content add primitive
 *
 * \param msg Message container
 * \param part Message content part to add
 *
 * Adds a message content part to the message container.
 */
int pmdp_msg_add_raw(pmdp_msg_t *msg, pmdp_msg_part_t *part)
{
	if(!msg || !part)
		return PMDP_ERR_INVARG;

	if(!msg->parts) {
		if(!(msg->parts = malloc(16 * sizeof(*msg->parts))))
			return PMDP_ERR_NOMEM;
		msg->nparts = 0;
		msg->naparts = 16;
	} else if(msg->nparts >= msg->naparts) {
		pmdp_msg_part_t **np = (pmdp_msg_part_t **)realloc(msg->parts, sizeof(*msg->parts) * msg->naparts * 2);
		if(!np)
			return PMDP_ERR_NOMEM;
		msg->naparts *= 2;
		msg->parts = np;
	}

	msg->parts[msg->nparts++] = part;
	return 0;
}

static int pmdp_check_utf8(const char *txt, size_t len)
{
#ifdef USE_ICONV
	iconv_t ic;
	size_t ilen = len;
	size_t olen = len;
	char *iptr, *xiptr;
	char *optr, *xoptr;
	int rv = 0;

	if(!len)
		return 0;

	if(!txt)
		return PMDP_ERR_INVARG;

	if(!(iptr = memdup(txt, len)))
		return PMDP_ERR_NOMEM;
	if(!(optr = malloc(olen))) {
		free(iptr);
		return PMDP_ERR_NOMEM;
	}
	if((iconv_t)-1 == (ic = iconv_open("UTF-8//IGNORE", "UTF-8"))) {
		free(optr);
		free(iptr);
		return PMDP_ERR_NOMEM;
	}
	xiptr = iptr;
	xoptr = optr;
	/* We should get the same number of output bytes as we have at the input */
	if((size_t)-1 == iconv(ic, &xiptr, &ilen, &xoptr, &olen) || ilen != 0 || olen != 0)
		rv = PMDP_ERR_NOTUTF8;
	free(optr);
	free(iptr);
	iconv_close(ic);
	return rv;
#else
	(void)txt;
	(void)len;
	return 0;
#endif
}

/*! Add string or resource to a PMD packet
 *
 * \param msg Message container
 * \param type Content type to add (PMDT_TEXT or PMDT_RESOURCE)
 * \param txt String data in UTF-8 format
 * \param len Length of string data
 *
 * This function serves as a primitive for pmdp_msg_add_text() and
 * pmdp_msg_add_resource().
 */
int pmdp_msg_add_string(pmdp_msg_t *msg, uint32_t type, const char *txt, size_t len)
{
	pmdp_msg_part_t *mp;
	int rv;
	if(!msg || (!txt && len))
		return PMDP_ERR_INVARG;
	if(0 != (rv = pmdp_check_utf8(txt, len)))
		return PMDP_ERR_NOTUTF8;
	if(!(mp = calloc(1, sizeof(*mp) + len + 1)))
		return PMDP_ERR_NOMEM;
	mp->type = type;
	mp->length = len;
	mp->flags |= PMDP_PARSE_VALIDUTF8;
	if(len)
		memcpy(mp->string, txt, len);
	mp->string[len] = 0;
	if(0 != (rv = pmdp_msg_add_raw(msg, mp)))
		free(mp);
	return rv;
}

/*! Add a 32-bit signed integer (PMDT_INTEGER)
 *
 * \param msg Message container
 * \param i Integer value
 */
int pmdp_msg_add_int32(pmdp_msg_t *msg, int32_t i)
{
	pmdp_msg_part_t *mp;
	int rv;
	if(!msg)
		return PMDP_ERR_INVARG;
	if(!(mp = calloc(1, sizeof(*mp))))
		return PMDP_ERR_NOMEM;
	mp->type = PMDT_INTEGER;
	mp->length = sizeof(i);
	mp->integer = i;
	if(0 != (rv = pmdp_msg_add_raw(msg, mp)))
		free(mp);
	return rv;
}

/*! Add a 32-bit unsigned integer (PMDT_CARDINAL)
 *
 * \param msg Message container
 * \param u Unsigned integer value
 */
int pmdp_msg_add_uint32(pmdp_msg_t *msg, uint32_t u)
{
	pmdp_msg_part_t *mp;
	int rv;
	if(!msg)
		return PMDP_ERR_INVARG;
	if(!(mp = calloc(1, sizeof(*mp))))
		return PMDP_ERR_NOMEM;
	mp->type = PMDT_CARDINAL;
	mp->length = sizeof(u);
	mp->cardinal = u;
	if(0 != (rv = pmdp_msg_add_raw(msg, mp)))
		free(mp);
	return rv;
}

/*! Add a category (PMDT_CATAGORY)
 *
 * \param msg Message container
 * \param cat Catagory (see PMDC_* definitions)
 *
 * Each PMDP message should contain a category and a subcategory.
 */
int pmdp_msg_add_category(pmdp_msg_t *msg, uint32_t cat)
{
	pmdp_msg_part_t *mp;
	int rv;
	if(!msg)
		return PMDP_ERR_INVARG;
	if(!(mp = calloc(1, sizeof(*mp))))
		return PMDP_ERR_NOMEM;
	mp->type = PMDT_CATEGORY;
	mp->length = sizeof(cat);
	mp->category = cat;
	if(0 != (rv = pmdp_msg_add_raw(msg, mp)))
		free(mp);
	return rv;
}

/*! Add a sub-category (PMDT_SUBCATAGORY)
 *
 * \param msg Message container
 * \param subcat Sub-catagory (see PMDC_* definitions)
 *
 * Each PMDP message should contain a category and a subcategory.
 */
int pmdp_msg_add_subcategory(pmdp_msg_t *msg, uint32_t subcat)
{
	pmdp_msg_part_t *mp;
	int rv;
	if(!msg)
		return PMDP_ERR_INVARG;
	if(!(mp = calloc(1, sizeof(*mp))))
		return PMDP_ERR_NOMEM;
	mp->type = PMDT_SUBCATEGORY;
	mp->length = sizeof(subcat);
	mp->category = subcat;
	if(0 != (rv = pmdp_msg_add_raw(msg, mp)))
		free(mp);
	return rv;
}

/*! Add a fractional number (PMDT_FRACTION)
 *
 * \param msg Message container
 * \param dividend Signed dividend (number above the line)
 * \param divisor Unsigned divisor (number under the line)
 *
 * Fractional value is computed by "dividend divided by divisor". Plus/minus
 * infinity is defined by setting divisor to 0 (zero) and the dividend to a
 * positive or negative value. Not-a-number (NaN) is defined by setting both
 * dividend and divisor to 0 (zero).
 */
int pmdp_msg_add_fraction(pmdp_msg_t *msg, int32_t dividend, uint32_t divisor)
{
	pmdp_msg_part_t *mp;
	int rv;
	if(!msg)
		return PMDP_ERR_INVARG;
	if(!(mp = calloc(1, sizeof(*mp))))
		return PMDP_ERR_NOMEM;
	mp->type = PMDT_FRACTION;
	mp->length = sizeof(mp->fraction);
	mp->fraction.dividend = dividend;
	mp->fraction.divisor = divisor;
	if(0 != (rv = pmdp_msg_add_raw(msg, mp)))
		free(mp);
	return rv;
}

/*! Add a text (PMDT_TEXT)
 *
 * \param msg Message container
 * \param txt String in UTF-8 format
 *
 * Adds the string txt to the message container. The string must be in UTF-8
 * format and zero-terminated (\0).
 */
int pmdp_msg_add_text(pmdp_msg_t *msg, const char *txt)
{
	size_t l = 0;
	if(!msg)
		return PMDP_ERR_INVARG;
	if(txt)
		l = strlen(txt);
	return pmdp_msg_add_string(msg, PMDT_TEXT, txt, l);
}

/*! Add a resource URI (PMDT_RESOURCE)
 *
 * \param msg Message container
 * \param txt Resource string in UTF-8 format
 *
 * Adds the resource string txt to the message container. The string must be in UTF-8
 * format and zero-terminated (\0).
 *
 * The resource is aa URI and must follow the format as described in RFC1630.
 * Any valid URI scheme is allowed and not checks are performed on the validity
 * of the resource string itself not its content.
 */
int pmdp_msg_add_resource(pmdp_msg_t *msg, const char *txt)
{
	size_t l = 0;
	if(!msg)
		return PMDP_ERR_INVARG;
	if(txt)
		l = strlen(txt);
	return pmdp_msg_add_string(msg, PMDT_RESOURCE, txt, l);
}

/*! Add binary data of specified type
 *
 * \param msg Message container
 * \param type Type of data to be added (see PMDT_*)
 * \param data Data to be added in binary form
 * \param datalen Size of the data block
 *
 * No checks are performed on adding binary data. The caller must ensure that
 * the type is set to represent the encapsulated data.
 */
int pmdp_msg_add_binary(pmdp_msg_t *msg, uint32_t type, const void *data, size_t datalen)
{
	pmdp_msg_part_t *mp;
	int rv;
	if(!msg || (!data && datalen))
		return PMDP_ERR_INVARG;
	if(!(mp = calloc(1, sizeof(*mp) + datalen)))
		return PMDP_ERR_NOMEM;
	mp->type = type;
	mp->length = datalen;
	if(datalen)
		memcpy(mp->octets, data, datalen);
	if(0 != (rv = pmdp_msg_add_raw(msg, mp)))
		free(mp);
	return rv;
}

/*! Add a UUID (PMDT_UUID)
 *
 * \param msg Message container
 * \param uuid Binary UUID to add
 *
 * Adds a UUID content.
 */
int pmdp_msg_add_uuid(pmdp_msg_t *msg, const uint8_t *uuid)
{
	if(!msg || !uuid)
		return PMDP_ERR_INVARG;
	return pmdp_msg_add_binary(msg, PMDT_UUID, uuid, PMDP_UUID_SIZE);
}

/*! Add a timestamp (PMDT_TIMESTAMP)
 *
 * \param msg Message container
 * \param tv Timestamp value in microsecond resolution
 *
 * Adds timestamp from the value supplied. See gettimeofday(2) for struct
 * timeval definition.
 */
int pmdp_msg_add_timestamp(pmdp_msg_t *msg, const struct timeval *tv)
{
	pmdp_msg_part_t *mp;
	int rv;
	if(!msg || !tv)
		return PMDP_ERR_INVARG;
	if(!(mp = calloc(1, sizeof(*mp))))
		return PMDP_ERR_NOMEM;
	mp->type = PMDT_TIMESTAMP;
	mp->length = sizeof(pmdp_pkt_timestamp_t);
	memcpy(&mp->timestamp, tv, sizeof(mp->timestamp));
	if(0 != (rv = pmdp_msg_add_raw(msg, mp)))
		free(mp);
	return rv;
}

/*! Add padding data (PMDT_PADDING)
 *
 * \param msg Message container
 * \param data Pad data to add
 * \param datalen Size of padding data to add
 *
 * If data is NULL and datalen > 0, then random data of size datalen will be
 * added from a cryptographically solid random number generator.
 */
int pmdp_msg_add_padding(pmdp_msg_t *msg, const void *data, size_t datalen)
{
	if(!msg)
		return PMDP_ERR_INVARG;
	if(!data && datalen > 0) {
		char *ldata;
		int fd;
		int rv;
		size_t len = datalen;
		if(!(ldata = (char *)malloc(datalen)))
			return PMDP_ERR_NOMEM;
		if(-1 == (fd = open("/dev/urandom", O_RDONLY))) {
			free(ldata);
			return PMDP_ERR_NORANDOM;
		}
		while(len) {
			ssize_t err = read(fd, ldata + datalen - len, len);
			if(-1 == err) {
				if(errno == EINTR)
					continue;
				close(fd);
				free(ldata);
				return PMDP_ERR_NORANDOM;
			}
			if(!err) {
				/* EOF on /dev/random is a bad sign */
				close(fd);
				free(ldata);
				return PMDP_ERR_NORANDOM;
			}
			len -= err;
		}
		close(fd);
		rv = pmdp_msg_add_binary(msg, PMDT_PADDING, ldata, datalen);
		free(ldata);
		return rv;
	} else
		return pmdp_msg_add_binary(msg, PMDT_PADDING, data, datalen);
}

/*! Add encrypted data to a message (PMDT_ENCRYPTED)
 *
 * \param msg Message container
 * \param encr Data to encrypt
 * \param crt X509 certificate to use for encryption
 * \param method Symmetric cipher to use (PMDE_*)
 *
 * Encrypt the message content parts from container encr and add it to message
 * container msg. The size of the encrypted part will always be a multiple of
 * the symmetric key size, which depends on method.
 *
 * Three content parts are added to msg: a key (PMDT_KEY), the encrypted data
 * (PMDT_ENCRYPTED) and the fingerprint of the encryption certificate
 * (PMDT_FINGERPRINT). The encrypted data will be padded before encryption, if
 * necessary, with PMDT_PADDING.
 */
int pmdp_msg_add_encrypted(pmdp_msg_t *msg, pmdp_msg_t *encr, pmdp_x509_crt_t *crt, int method)
{
#if defined(HAVE_LIBCRYPTO)
	const EVP_CIPHER *cipher = pmdp_key_algo(method);
	EVP_CIPHER_CTX ctx;
	int enclen;
	int elen;
#elif defined(HAVE_LIBGNUTLS)
	gnutls_cipher_hd_t chd;
	gnutls_datum_t gkey;
	gnutls_datum_t giv;
	int cipher = pmdp_key_algo(method);
#endif
	pmdp_msg_part_t *partkey = NULL;
	pmdp_msg_part_t *partenc = NULL;
	pmdp_msg_part_t *partfp = NULL;
	size_t fplen;
	uint8_t *fp = NULL;
	pmdp_pkt_key_t *kiv = NULL;
	size_t keyivsize = pmdp_keyiv_size(method);
	int rv;

	/* Note: keys smaller than a content header are invalid (i.e. < 32bit), which should never occur... */
	if(!msg || !encr || !crt)
		return PMDP_ERR_INVARG;

	if(!cipher || !keyivsize)
		return PMDP_ERR_NOALGO;

	if(keyivsize < sizeof(pmdp_pkt_content_t))
		return PMDP_ERR_INVARG;

	/* Create a fingerprint of the certificate */
	if(0 != (rv = pmdp_x509_fingerprint(crt, &fp, &fplen)))
		return rv;

	if(!(partfp = calloc(1, sizeof(*partfp) + fplen))) {
		free(fp);
		return PMDP_ERR_NOMEM;
	}
	partfp->type = PMDT_FINGERPRINT;
	partfp->length = fplen;
	memcpy(partfp->octets, fp, fplen);
	free(fp);
	fp = NULL;

	/* Create a key and IV */
	if(!(kiv = malloc(sizeof(*kiv) + 2 * keyivsize))) {
		free(partfp);
		return PMDP_ERR_NOMEM;
	}
	kiv->type = htobe16(method);
	if(pmdp_random_bytes(&kiv->octets[0], 2*keyivsize)) {
		rv = PMDP_ERR_NORANDOM;
		goto err_out;
	}
	kiv->crc = htobe16(pmdp_crc_ccitt((const uint8_t *)&kiv->type, 2*keyivsize + sizeof(kiv->type)));

	/* And encrypt this key+iv with the public certificate */
	if(0 != (rv = pmdp_x509_encrypt(crt, (const uint8_t *)kiv, sizeof(*kiv)+2*keyivsize, &fp, &fplen)))
		goto err_out;

	if(!(partkey = calloc(1, sizeof(*partkey) + fplen))) {
		rv = PMDP_ERR_NOMEM;
		goto err_out;
	}

	partkey->type = PMDT_KEY;
	partkey->length = fplen;
	memcpy(partkey->octets, fp, fplen);
	free(fp);
	fp = NULL;

	/* Generate binary content */
	if(0 != (rv = pmdp_msg_generate(encr, NULL, NULL, NULL, &fplen)))
		goto err_out;
	if(fplen <= sizeof(pmdp_pkt_hdr_t)) {
		rv = PMDP_ERR_TOOSMALL;
		goto err_out;
	}
	fplen -= sizeof(pmdp_pkt_hdr_t);	/* Correct for the header size */
	if(!fplen) {
		rv = PMDP_ERR_TOOSMALL;		/* There must be some data to encrypt */
		goto err_out;
	}

	/* Check if we need padding to match the key size */
	if(fplen % keyivsize) {
		/* The encrypted content must be a multiple of the encryption size */
		/* Adding padding will cause it to be on a word boundary */
		fplen += 3;
		fplen &= ~3;
		if(0 != (rv = pmdp_msg_add_padding(encr, NULL, keyivsize - (fplen % keyivsize) - sizeof(pmdp_pkt_content_t))))
			goto err_out;
		/* Regenerate message content */
		if(0 != (rv = pmdp_msg_generate(encr, NULL, NULL, NULL, &fplen)))
			goto err_out;
		fplen -= sizeof(pmdp_pkt_hdr_t);
		assert((fplen % keyivsize) == 0);
	}

	if(!(partenc = calloc(1, sizeof(*partenc) + fplen))) {
		rv = PMDP_ERR_NOMEM;
		goto err_out;
	}
	partenc->type = PMDT_ENCRYPTED;
	partenc->length = fplen;
	partenc->encrypted.key = NULL;
	partenc->encrypted.fingerprint = NULL;
#if defined(HAVE_LIBCRYPTO)
	EVP_CIPHER_CTX_init(&ctx);
	if(!EVP_CipherInit_ex(&ctx, cipher, NULL, NULL, NULL, 1 /* encrypt*/)) {
		EVP_CIPHER_CTX_cleanup(&ctx);
		rv = PMDP_ERR_ENCRYPT;
		goto err_out;
	}
	EVP_CIPHER_CTX_set_padding(&ctx, 0);
	if(!EVP_CipherInit_ex(&ctx, NULL, NULL, &kiv->octets[0] /*key*/, &kiv->octets[keyivsize]/*iv*/, 1 /*encrypt*/)) {
		EVP_CIPHER_CTX_cleanup(&ctx);
		rv = PMDP_ERR_ENCRYPT;
		goto err_out;
	}
	elen = fplen;
	enclen = 0;
	if(!EVP_CipherUpdate(&ctx, partenc->encrypted.octets, &elen, (unsigned char *)&encr->pkt[1], fplen)) {
		EVP_CIPHER_CTX_cleanup(&ctx);
		rv = PMDP_ERR_ENCRYPT;
		goto err_out;
	}
	enclen += elen;
	elen -= fplen;
	if(!EVP_CipherFinal_ex(&ctx, partenc->encrypted.octets+enclen, &elen)) {
		EVP_CIPHER_CTX_cleanup(&ctx);
		rv = PMDP_ERR_ENCRYPT;
		goto err_out;
	}
	enclen += elen;
	EVP_CIPHER_CTX_cleanup(&ctx);
	/* Encrypted length should be source length */
	if(enclen != fplen) {
		rv = PMDP_ERR_ENCRYPT;
		goto err_out;
	}
#elif defined(HAVE_LIBGNUTLS)
	gkey.data = &kiv->octets[0];
	gkey.size = keyivsize;
	giv.data = &kiv->octets[keyivsize];
	giv.size = keyivsize;
	if(gnutls_cipher_init(&chd, cipher, &gkey, &giv) < 0) {
		rv = PMDP_ERR_ENCRYPT;
		goto err_out;
	}
	if(gnutls_cipher_encrypt2(chd, &encr->pkt[1], fplen, partenc->encrypted.octets, fplen) < 0) {
		gnutls_cipher_deinit(chd);
		rv = PMDP_ERR_ENCRYPT;
		goto err_out;
	}
	gnutls_cipher_deinit(chd);
#endif
	if(0 != (rv = pmdp_msg_add_raw(msg, partkey)))
		goto err_out;
	if(0 != (rv = pmdp_msg_add_raw(msg, partenc)))
		goto err_out2;
	if(0 != (rv = pmdp_msg_add_raw(msg, partfp)))
		goto err_out3;
	partenc->encrypted.key = partkey;
	partenc->encrypted.fingerprint = partfp;
	return 0;
err_out:
	free(partkey);
err_out2:
	free(partenc);
err_out3:
	free(partfp);
	free(fp);
	free(kiv);
	return rv;
}

int pmdp_msg_set_severity(pmdp_msg_t *msg, unsigned severity)
{
	if(!msg || severity >= (1<<3))
		return PMDP_ERR_INVARG;
	msg->severity = severity;
	return 0;
}

int pmdp_msg_get_severity(const pmdp_msg_t *msg, unsigned *severity)
{
	if(!msg || !severity)
		return PMDP_ERR_INVARG;
	*severity = msg->severity;
	return 0;
}

int pmdp_msg_set_sequence(pmdp_msg_t *msg, unsigned sequence)
{
	if(!msg || sequence >= (1<<4))
		return PMDP_ERR_INVARG;
	msg->sequence = sequence;
	if(msg->pkt && !(msg->intflags & PMDP_INTF_REFPKT))
		msg->pkt->sequence = sequence;
	return 0;
}

int pmdp_msg_get_sequence(const pmdp_msg_t *msg, unsigned *sequence)
{
	if(!msg || !sequence)
		return PMDP_ERR_INVARG;
	*sequence = msg->sequence;
	return 0;
}

int pmdp_msg_set_flags(pmdp_msg_t *msg, uint32_t flags)
{
	if(!msg)
		return PMDP_ERR_INVARG;
	msg->flags = flags;
	return 0;
}

int pmdp_msg_get_flags(const pmdp_msg_t *msg, uint32_t *flags)
{
	if(!msg || !flags)
		return PMDP_ERR_INVARG;
	*flags = msg->flags;
	return 0;
}

int pmdp_msg_set_uuid(pmdp_msg_t *msg, uint8_t *uuid)
{
	if(!msg || !uuid)
		return PMDP_ERR_INVARG;
	memcpy(msg->uuid, uuid, sizeof(msg->uuid));
	return 0;
}

int pmdp_msg_get_uuid(pmdp_msg_t *msg, uint8_t *uuid)
{
	if(!msg || !uuid)
		return PMDP_ERR_INVARG;
	memcpy(uuid, msg->uuid, sizeof(msg->uuid));
	return 0;
}

int pmdp_msg_set_timestamp(pmdp_msg_t *msg, const struct timeval *tv)
{
	if(!msg)
		return PMDP_ERR_INVARG;
	if(tv)
		memcpy(&msg->timestamp, tv, sizeof(msg->timestamp));
	else
		memset(&msg->timestamp, 0, sizeof(msg->timestamp));
	if(msg->pkt && !(msg->intflags & PMDP_INTF_REFPKT))
		pmdp_timeval_to_timestamp(tv, &msg->pkt->timestamp);
	return 0;
}

int pmdp_msg_get_timestamp(const pmdp_msg_t *msg, struct timeval *tv)
{
	if(!msg || !tv)
		return PMDP_ERR_INVARG;
	memcpy(tv, &msg->timestamp, sizeof(msg->timestamp));
	return 0;
}

int pmdp_msg_get_version(const pmdp_msg_t *msg, unsigned *version)
{
	if(!msg || !version)
		return PMDP_ERR_INVARG;
	*version = msg->version;
	return 0;
}

int pmdp_msg_generate(pmdp_msg_t *msg, const pmdp_x509_crt_t *sigcrt, const pmdp_x509_key_t *sigkey, void **pdata, size_t *plen)
{
	size_t pktlen;
	pmdp_pkt_hdr_t *pkt;
	uint8_t *pptr;
	pmdp_pkt_content_t *sigptr = NULL;
	unsigned i;
	size_t keysize = 0;
	size_t fplen;
	int rv;

	if(!msg)
		return PMDP_ERR_INVARG;

	/* Clear any previous generation of the message */
	if(msg->pkt) {
		if(!(msg->intflags & PMDP_INTF_REFPKT))
			free(msg->pkt);
		msg->intflags &= ~PMDP_INTF_REFPKT;
		msg->pkt = NULL;
		msg->sigtag = NULL;
		msg->pktlen = 0;
	}

	/* If the message contains a signature, remove it */
	if(msg->signature) {
		if(!(msg->intflags & PMDP_INTF_REFSIG))
			free(msg->signature);
		msg->intflags &= ~PMDP_INTF_REFSIG;
		msg->signature = NULL;
	}

	if(sigcrt && sigkey) {
		uint8_t *fp;

		/* Find out what the size of the signature will be */
		if(!(keysize = pmdp_x509_privkeysize(sigkey)) || 0 != (keysize % 8))
			return PMDP_ERR_BADKEY;

		/* Calculate the fingerprint of the signing certificate */
		if(0 != (rv = pmdp_x509_fingerprint(sigcrt, &fp, &fplen)))
			return rv;

		/* If the last message content part is encrypted, it implicitly
		 * has a fingerprint attached. We may use that to indicate the
		 * signature.
		 * If the last content part is an explicit fingerprint, we may
		 * also use it.
		 */
		msg->intflags &= ~PMDP_INTF_REFFP;
		msg->fingerprint = NULL;
		if(PMDT_ENCRYPTED == msg->parts[msg->nparts-1]->type) {
			if(fplen == msg->parts[msg->nparts-1]->encrypted.fingerprint->length && !memcmp(msg->parts[msg->nparts-1]->encrypted.fingerprint->octets, fp, fplen)) {
				msg->fingerprint = msg->parts[msg->nparts-1]->encrypted.fingerprint;
				msg->intflags |= PMDP_INTF_REFFP;
			}
		} else if(PMDT_FINGERPRINT == msg->parts[msg->nparts-1]->type) {
			if(fplen == msg->parts[msg->nparts-1]->length && !memcmp(msg->parts[msg->nparts-1]->octets, fp, fplen)) {
				msg->fingerprint = msg->parts[msg->nparts-1];
				msg->intflags |= PMDP_INTF_REFFP;
			}
		}

		/* Check is we still need a fingerprint */
		if(!msg->fingerprint) {
			if(!(msg->fingerprint = calloc(1, sizeof(*msg->fingerprint) + fplen))) {
				free(fp);
				return PMDP_ERR_NOMEM;
			}
			msg->fingerprint->type = PMDT_FINGERPRINT;
			msg->fingerprint->length = fplen;
			memcpy(msg->fingerprint->octets, fp, fplen);
		}
		free(fp);

		if(!(msg->signature = calloc(1, sizeof(*msg->signature) + keysize / 8)))
			return PMDP_ERR_NOMEM;
		/* We will fill out the content later */
		msg->signature->type = PMDT_SIGNATURE;
		msg->signature->length = keysize / 8;
	} else {
		/* If we do not need to sign the message, remove the fingerprint as well */
		if(msg->fingerprint) {
			if(!(msg->intflags & PMDP_INTF_REFFP))
				free(msg->fingerprint);
			msg->intflags &= ~PMDP_INTF_REFFP;
			msg->fingerprint = NULL;
		}
	}

	/* Calculate the final packet size */
	pktlen = sizeof(pmdp_pkt_hdr_t);
	/* Content aligned to next 32bit boundary, except the last */
	for(i = 0; msg->nparts && i < msg->nparts-1; i++)
		pktlen += (msg->parts[i]->length + sizeof(pmdp_pkt_content_t) + 3) & ~0x3;
	pktlen += msg->parts[i]->length + sizeof(pmdp_pkt_content_t);

	/* Add the fingerprint size to the packet only if it is not a reference */
	if(msg->fingerprint && !(msg->intflags & PMDP_INTF_REFFP)) {
		pktlen = (pktlen + 3) & ~0x3;	/* Align to next 32bit boundary */
		pktlen +=  + sizeof(pmdp_pkt_content_t) + msg->fingerprint->length;
	}

	if(msg->signature) {
		pktlen = (pktlen + 3) & ~0x3;	/* Align to next 32bit boundary */
		pktlen +=  + sizeof(pmdp_pkt_content_t) + msg->signature->length;
	}

	if(pktlen > PMDP_MAX_MSGSIZE)
		return PMDP_ERR_TOOLARGE;

	if(!(pkt = malloc(pktlen)))
		return PMDP_ERR_NOMEM;

	/* Contruct the packet */
	memset(pkt, 0, sizeof(*pkt));
	pkt->version = msg->version;
	pkt->severity = msg->severity;
	pkt->sequence = msg->sequence;
	pkt->forcesilent = (msg->flags & PMDF_FORCESILENT) ? 1 : 0;
	pmdp_timeval_to_timestamp(&msg->timestamp, &pkt->timestamp);
	memcpy(pkt->uuid, msg->uuid, sizeof(msg->uuid));
	pptr = (uint8_t *)&pkt[1];
	for(i = 0; i < msg->nparts; i++) {
		uint32_t		u;
		pmdp_fraction_t		f;
		pmdp_msg_part_t *m = msg->parts[i];
		pmdp_pkt_content_t *p = (pmdp_pkt_content_t *)pptr;
		p->ctype = htobe16(m->type);	/* FIXME: type-punned -> alignment? should be fine with 32bit align. */
		p->clen = htobe16(m->length);
		switch(m->type) {
		case PMDT_CATEGORY:
		case PMDT_SUBCATEGORY:
		case PMDT_INTEGER:
		case PMDT_CARDINAL:
			if(m->length != sizeof(uint32_t))
				goto size_err_out;
			u = htobe32(m->cardinal);
			memcpy(p->bytes, &u, sizeof(u));
			break;
		case PMDT_FRACTION:
			if(m->length != sizeof(pmdp_fraction_t))
				goto size_err_out;
			f.dividend = htobe32(m->fraction.dividend);
			f.divisor = htobe32(m->fraction.divisor);
			memcpy(p->bytes, &f, sizeof(f));
			break;
		case PMDT_TIMESTAMP:
			if(m->length != sizeof(pmdp_pkt_timestamp_t))
				goto size_err_out;
			pmdp_timeval_to_timestamp(&m->timestamp, (pmdp_pkt_timestamp_t *)p->bytes);
			break;
		case PMDT_ENCRYPTED:
			if(m->length)
				memcpy(p->bytes, m->encrypted.octets, m->length);
			break;
		case PMDT_UUID:
			if(m->length != PMDP_UUID_SIZE)
				goto size_err_out;
			/* Fallthrough */
		default:
			if(m->length)
				memcpy(p->bytes, m->octets, m->length);
			break;
		}
		/* Align the pointer to next 32bit boundary */
		/* It is _intentional_ not to initialize the skipped octets */
		pptr += (m->length + sizeof(*p) + 3) & ~0x3;
	}

	if(msg->fingerprint && !(msg->intflags & PMDP_INTF_REFFP)) {
		pmdp_pkt_content_t *p = (pmdp_pkt_content_t *)pptr;
		p->ctype = htobe16(msg->fingerprint->type);
		p->clen = htobe16(msg->fingerprint->length);
		memcpy(p->bytes, msg->fingerprint->octets, msg->fingerprint->length);
		pptr += (sizeof(*p) + msg->fingerprint->length + 3) & ~0x3;
		if(msg->signature)
			msg->signature->flags |= PMDP_PARSE_VALIDFP;
	}

	if(msg->signature) {
		pmdp_pkt_content_t *p = (pmdp_pkt_content_t *)pptr;
		msg->sigtag = pptr;
		p->ctype = htobe16(msg->signature->type);
		p->clen = htobe16(msg->signature->length);
		memcpy(p->bytes, msg->signature->octets, msg->signature->length);
		sigptr = p;
		pptr += (sizeof(*p) + msg->signature->length + 3) & ~0x3;
		fplen = keysize / 8;
		if(keysize && 0 != (rv = pmdp_x509_sign(sigkey, &pkt->uuid[0], (uint8_t *)sigptr - &pkt->uuid[0], sigptr->bytes, &fplen))) {
			free(pkt);
			return rv;
		}
		msg->signature->flags |= PMDP_PARSE_VALIDSIG;
	}

	msg->pkt = pkt;
	msg->pktlen = pktlen;
	if(pdata)
		*pdata = pkt;
	if(plen)
		*plen = pktlen;
	return 0;

size_err_out:
	free(pkt);
	return PMDP_ERR_SIZE;
}

/*
 * Parse the actual content into parts
 */
static int pmdp_msg_part_parse(void *data, size_t len, pmdp_msg_t *msg, unsigned pflags, unsigned *resflags)
{
	uint8_t *pptr = (uint8_t *)data;
	int rv;

	assert(data != NULL);
	assert(msg != NULL);
	assert(resflags != NULL);

	while(len >= sizeof(pmdp_pkt_content_t)) {
		size_t alen;
		uint32_t u;
		struct timeval t;
		pmdp_fraction_t f;
		pmdp_msg_part_t *mp;
		pmdp_pkt_content_t *p = (pmdp_pkt_content_t *)pptr;
		uint16_t cl, ct;
		int isbad;
		ct = be16toh(p->ctype);
		cl = be16toh(p->clen);
		/* There must be buffer left for both header and actual content */
		if(len < cl + sizeof(pmdp_pkt_content_t)) {
			return PMDP_ERR_TOOSMALL;
		}
		switch(ct) {
		case PMDT_CATEGORY:
		case PMDT_SUBCATEGORY:
		case PMDT_INTEGER:
		case PMDT_CARDINAL:
			if(cl != sizeof(uint32_t))
				return PMDP_ERR_SIZE;
			memcpy(&u, p->bytes, sizeof(u));
			u = be32toh(u);
			if(0 != (rv = pmdp_msg_add_binary(msg, ct, &u, cl)))
				return rv;
			break;
		case PMDT_FRACTION:
			if(cl != sizeof(pmdp_fraction_t))
				return PMDP_ERR_SIZE;
			memcpy(&f, p->bytes, sizeof(f));
			f.dividend = be32toh(f.dividend);
			f.divisor = be32toh(f.divisor);
			if(0 != (rv = pmdp_msg_add_binary(msg, ct, &f, cl)))
				return rv;
			break;
		case PMDT_TIMESTAMP:
			if(cl != sizeof(pmdp_pkt_timestamp_t))
				return PMDP_ERR_SIZE;
			pmdp_timestamp_to_timeval((pmdp_pkt_timestamp_t *)p->bytes, &t);
			if(0 != (rv = pmdp_msg_add_binary(msg, ct, &t, cl)))
				return rv;
			break;
		case PMDT_TEXT:
		case PMDT_RESOURCE:
			isbad = 0;
			if(0 != (rv = pmdp_check_utf8((const char *)p->bytes, cl))) {
				isbad = 1;
				if(!(pflags & PMDP_PARSE_BADUTF8))
					return rv;
				*resflags |= PMDP_PARSE_BADUTF8;
			}
			if(0 != (rv = pmdp_msg_add_binary(msg, ct, p->bytes, cl)))
				return rv;
			if(!isbad)
				msg->parts[msg->nparts-1]->flags |= PMDP_PARSE_VALIDUTF8;
			break;
		case PMDT_UUID:
			if(cl != PMDP_UUID_SIZE)
				return PMDP_ERR_SIZE;
			if(0 != (rv = pmdp_msg_add_binary(msg, ct, p->bytes, cl)))
				return rv;
			break;
		case PMDT_PADDING:
		case PMDT_KEY:
		case PMDT_FINGERPRINT:
		case PMDT_SIGNATURE:
			if(0 != (rv = pmdp_msg_add_binary(msg, ct, p->bytes, cl)))
				return rv;
			if(ct == PMDT_SIGNATURE)
				msg->sigtag = pptr;
			if(ct == PMDT_FINGERPRINT && msg->nparts > 1 && msg->parts[msg->nparts-2]->type == PMDT_ENCRYPTED)
				msg->parts[msg->nparts-2]->encrypted.fingerprint = msg->parts[msg->nparts-1];
			break;
		case PMDT_ENCRYPTED:
			if(!(mp = calloc(1, sizeof(*mp) + cl))) {
				rv = PMDP_ERR_NOMEM;
				return rv;
			}
			mp->type = ct;
			mp->length = cl;
			memcpy(mp->encrypted.octets, p->bytes, cl);
			mp->encrypted.key = NULL;
			mp->encrypted.fingerprint = NULL;
			if(msg->nparts && msg->parts[msg->nparts-1]->type == PMDT_KEY)
				mp->encrypted.key = msg->parts[msg->nparts-1];
			if(0 != (rv = pmdp_msg_add_raw(msg, mp))) {
				free(mp);
				return rv;
			}
			break;
		default:
			if(!(pflags & PMDP_PARSE_UTYPES)) {
				rv = PMDP_ERR_TYPE;
				return rv;
			}
			*resflags |= PMDP_PARSE_UTYPES;
			if(0 != (rv = pmdp_msg_add_binary(msg, ct, p->bytes, cl)))
				return rv;
			break;
		}
		alen = sizeof(*p) + cl;
		pptr += alen;
		len -= alen;
		/* Possible situations:
		 * - alen not aligned
		 *   - len > alignment && len - alignment >= content header --> OK
		 *   - else --> trailing junk
		 * - alen is multiple of 4
		 *   - len >= content header --> OK
		 *   - len < content header --> trailing junk
		 */
		if(alen & 0x03) {
			if(len < sizeof(*p) || len - (4 - (alen & 0x3)) < sizeof(*p))
				break;
			pptr += 4 - (alen & 0x3);
			len -= 4 - (alen & 0x3);
		} else if(len < sizeof(*p))
			break;
	}

	if(len) {
		if(!(pflags & PMDP_PARSE_TRAILING)) {
			return PMDP_ERR_TRAILING;
		}
		*resflags |= PMDP_PARSE_TRAILING;
	}
	return 0;
}

int pmdp_msg_parse(void *data, size_t datalen, pmdp_msg_t **pmsg, unsigned *piflags)
{
	pmdp_msg_t *msg;
	pmdp_pkt_hdr_t *pkt;
	size_t len = datalen;
	int rv;
	unsigned pflags = piflags ? (*piflags) : 0;
	unsigned resflags = 0;

	if(!data || !pmsg)
		return PMDP_ERR_INVARG;

	*pmsg = NULL;

	if(datalen < sizeof(pmdp_pkt_hdr_t))
		return PMDP_ERR_TOOSMALL;

	if(!(msg = pmdp_msg_new()))
		return PMDP_ERR_NOMEM;

	pkt = (pmdp_pkt_hdr_t *)data;

	msg->pkt = pkt;
	msg->pktlen = datalen;
	msg->intflags |= PMDP_INTF_REFPKT;

	if(PMDP_PKT_VERSION != (msg->version = pkt->version)) {
		pmdp_msg_free(msg);
		return PMDP_ERR_VERSION;
	}
	msg->sequence = pkt->sequence;
	msg->severity = pkt->severity;
	msg->flags = pkt->forcesilent ? PMDF_FORCESILENT : 0;
	pmdp_timestamp_to_timeval(&pkt->timestamp, &msg->timestamp);
	memcpy(msg->uuid, pkt->uuid, sizeof(msg->uuid));
	if(pkt->reserved || (pkt->timestamp.usecslo & 0x0f)) {
		pmdp_msg_free(msg);
		return PMDP_ERR_RESERVED;
	}

	/* Parse the parts */
	if(0 != (rv = pmdp_msg_part_parse(&pkt[1], len - sizeof(*pkt), msg, pflags, &resflags))) {
		pmdp_msg_free(msg);
		return rv;
	}

	/* Check if we have a signature we can verify i.e. fp + sig as last parts */
	if(msg->nparts >= 2 && msg->parts[msg->nparts-1]->type == PMDT_SIGNATURE && msg->parts[msg->nparts-2]->type == PMDT_FINGERPRINT) {
		resflags |= PMDP_PARSE_HAVESIG;
		msg->parts[msg->nparts-1]->flags |= PMDP_PARSE_VALIDFP;
	}
	/* Check if we have a valid encrypted part i.e. key + encr + fp */
	if(msg->nparts >= 3) {
		unsigned i;
		for(i = 0; i < msg->nparts-2; i++) {
			if(msg->parts[i]->type == PMDT_KEY && msg->parts[i+1]->type == PMDT_ENCRYPTED && msg->parts[i+2]->type == PMDT_FINGERPRINT) {
				resflags |= PMDP_PARSE_HAVEENCR;
				break;
			}
		}
	}

	*pmsg = msg;
	msg->intflags |= resflags;
	if(piflags)
		*piflags = resflags;
	return 0;
}

int pmdp_msg_parse_encrypted(const pmdp_x509_key_t *pkey, const pmdp_msg_part_t *epart, pmdp_msg_t **pmsg, unsigned *piflags)
{
#if defined(HAVE_LIBCRYPTO)
	const EVP_CIPHER *cipher;
	EVP_CIPHER_CTX ctx;
	int dlen;
#elif defined(HAVE_LIBGNUTLS)
	gnutls_cipher_hd_t chd;
	gnutls_datum_t gkey;
	gnutls_datum_t giv;
	int cipher;
#endif
	int rv = 0;
	unsigned pflags = piflags ? *piflags : 0;
	unsigned resflags = 0;
	pmdp_msg_t *msg = NULL;
	pmdp_pkt_key_t *keydata = NULL;
	uint8_t *decdata = NULL;
	size_t keydatasize;
	size_t keyivsize;
	int declen;

	if(!pkey || !epart || !pmsg || PMDT_ENCRYPTED != epart->type)
		return PMDP_ERR_INVARG;

	*pmsg = NULL;

	if(!epart->encrypted.key)
		return PMDP_ERR_NOKEY;

	keydatasize = epart->encrypted.key->length;
	if(keydatasize <= sizeof(*keydata))
		return PMDP_ERR_SIZE;

	if(!(msg = pmdp_msg_new()))
		return PMDP_ERR_NOMEM;

	keydatasize = 0;
	/* Decrypt PMDP_KEY data into the new buffer */
	if(0 != (rv = pmdp_x509_decrypt(pkey, epart->encrypted.key->octets, epart->encrypted.key->length, (uint8_t **)&keydata, &keydatasize))) {
		goto err_out;
	}
	/* If all went well, we have the decrypted key and IV. Check the key-type. */
	cipher = pmdp_key_algo(be16toh(keydata->type));
	keyivsize = pmdp_keyiv_size(be16toh(keydata->type));
	if(!cipher || !keyivsize) {
		rv = PMDP_ERR_NOALGO;
		goto err_out;
	}

	/* The decrypted key+iv must have the correct size for the algorithm */
	if(2*keyivsize + sizeof(keydata->type) + sizeof(keydata->crc) != keydatasize) {
		rv = PMDP_ERR_SIZE;
		goto err_out;
	}

	/* The CRC must match for successful decryption (if the CRC is set) */
	if(keydata->crc) {
		if(keydata->crc != htobe16(pmdp_crc_ccitt((const uint8_t *)&keydata->type, 2*keyivsize + sizeof(keydata->type)))) {
			rv = PMDP_ERR_CRC;
			goto err_out;
		}
	}

	/* We should have a decrypted and valid key+iv here and can finally decrypt the actual message */
	if(!(decdata = malloc(epart->length))) {
		rv = PMDP_ERR_NOMEM;
		goto err_out;
	}

#if defined(HAVE_LIBCRYPTO)
	EVP_CIPHER_CTX_init(&ctx);
	if(!EVP_CipherInit_ex(&ctx, cipher, NULL, NULL, NULL, 0 /*decrypt*/)) {
		EVP_CIPHER_CTX_cleanup(&ctx);
		rv = PMDP_ERR_DECRYPT;
		goto err_out;
	}
	EVP_CIPHER_CTX_set_padding(&ctx, 0);
	if(!EVP_CipherInit_ex(&ctx, NULL, NULL, &keydata->octets[0] /*key*/, &keydata->octets[keyivsize]/*iv*/, 0 /*decrypt*/)) {
		EVP_CIPHER_CTX_cleanup(&ctx);
		rv = PMDP_ERR_DECRYPT;
		goto err_out;
	}
	dlen = epart->length;
	declen = 0;
	if(!EVP_CipherUpdate(&ctx, decdata, &dlen, (unsigned char *)&epart->encrypted.octets[0], epart->length)) {
		EVP_CIPHER_CTX_cleanup(&ctx);
		rv = PMDP_ERR_DECRYPT;
		goto err_out;
	}
	declen += dlen;
	if(!EVP_CipherFinal_ex(&ctx, decdata+dlen, &dlen)) {
		EVP_CIPHER_CTX_cleanup(&ctx);
		rv = PMDP_ERR_DECRYPT;
		goto err_out;
	}
	declen += dlen;
	EVP_CIPHER_CTX_cleanup(&ctx);

	/* We should have same amount of decrypted data as there was in encrypted form */
	if(declen != epart->length) {
		rv = PMDP_ERR_DECRYPT;
		goto err_out;
	}
#elif defined(HAVE_LIBGNUTLS)
	declen = epart->length;
	gkey.data = &keydata->octets[0];
	gkey.size = keyivsize;
	giv.data = &keydata->octets[keyivsize];
	giv.size = keyivsize;
	if(gnutls_cipher_init(&chd, cipher, &gkey, &giv) < 0) {
		rv = PMDP_ERR_DECRYPT;
		goto err_out;
	}
	if(gnutls_cipher_decrypt2(chd, &epart->encrypted.octets[0], declen, decdata, declen) < 0) {
		gnutls_cipher_deinit(chd);
		rv = PMDP_ERR_DECRYPT;
		goto err_out;
	}
	gnutls_cipher_deinit(chd);
#endif

	msg = pmdp_msg_new();

	/* Parse the parts */
	if(0 != (rv = pmdp_msg_part_parse(decdata, declen, msg, pflags, &resflags)))
		goto err_out;

	msg->intflags |= resflags | PMDP_INTF_NOHDR;
	msg->intflags &= ~PMDP_INTF_REFPKT;	/* Let the free msg operation free the buffer */
	msg->pkt = (pmdp_pkt_hdr_t *)decdata;
	msg->pktlen = declen;
	*pmsg = msg;
	free(keydata);

	if(piflags)
		*piflags = resflags;
	return 0;

err_out:
	if(keydata)
		free(keydata);
	if(decdata)
		free(decdata);
	pmdp_msg_free(msg);
	
	return rv;
}

/*! Get the message's fingerprint
 *
 * \param msg Message container
 *
 * Returns the message content part of the fingerprint (PMDT_FINGERPRINT). The
 * function returns NULL is no fingerprint is available. The returned
 * fingerprint is the fingerprint associated with the signature (the last
 * fingerprint in the message).
 */
pmdp_msg_part_t *pmdp_msg_get_fingerprint(const pmdp_msg_t *msg)
{
	if(!msg)
		return NULL;
	if(!(msg->intflags & PMDP_PARSE_HAVESIG))
		return NULL;
	if(msg->nparts < 2)
		return NULL;
	if(msg->parts[msg->nparts-2]->type != PMDT_FINGERPRINT)
		return NULL;
	return msg->parts[msg->nparts-2];
}

/*! Verify message signature
 *
 * \param msg Message container
 * \param crt X509 certificate
 *
 * Verify the cryptographic signature of the message using the certificate
 * supplied. The message container flags are updated to include
 * PMDP_PARSE_VALIDSIG upon successful message verification.
 *
 * The functions returns 0 (zero) if the signature is verified correctly. If
 * msg or crt is NULL, PMDP_ERR_INVARG is returned. If the message does not
 * have a signature, then PMDP_ERR_VERIFY is returned. If the certificate is
 * invalid then PMDP_ERR_CERTIFICATE is returned. Expired certificates result
 * in PMDP_ERR_EXPIRED.
 */
int pmdp_msg_verify(const pmdp_msg_t *msg, const pmdp_x509_crt_t *crt)
{
	int rv;
	if(!msg || !crt)
		return PMDP_ERR_INVARG;
	if(!msg->pkt || !msg->pktlen || !msg->sigtag)
		return PMDP_ERR_VERIFY;
	if(msg->nparts < 2)
		return PMDP_ERR_VERIFY;
	if(msg->parts[msg->nparts-1]->type != PMDT_SIGNATURE)
		return PMDP_ERR_VERIFY;
	if(!(rv = pmdp_x509_verify(crt, msg->pkt->uuid, msg->sigtag - msg->pkt->uuid, msg->parts[msg->nparts-1]->octets, msg->parts[msg->nparts-1]->length)))
		msg->parts[msg->nparts-1]->flags |= PMDP_PARSE_VALIDSIG;
	else
		msg->parts[msg->nparts-1]->flags &= ~PMDP_PARSE_VALIDSIG;
	return rv;
}

/*! Iterate message content parts
 *
 * \param msg Message container
 * \param callback Callback funtion to call for each part
 * \param ptr User defined argument supplied to the callback function
 *
 * The function iterates all messages content parts and calls the callback
 * function for each part. The user supplied ptr argument may be used to convey
 * user data.
 *
 * The function returns 0 (zero) on success or PMDP_ERR_INVARG is msg or
 * callback is NULL.
 */
int pmdp_msg_iterate_parts(const pmdp_msg_t *msg, pmdp_fncb_parts_t callback, void *ptr)
{
	unsigned i;
	if(!msg || !callback)
		return PMDP_ERR_INVARG;

	for(i = 0; i < msg->nparts; i++) {
		if(!callback(msg, msg->parts[i], ptr))
			break;
	}
	return 0;
}

