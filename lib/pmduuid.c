/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2014  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <ctype.h>

#include <pmdp_priv.h>
#include <pmdp.h>

#ifdef USE_LIBUUID
#include <uuid/uuid.h>

int pmdp_uuid_parse(const char *str, uint8_t *uu)
{
	return uuid_parse(str, uu);
}

void pmdp_uuid_generate(uint8_t *uu)
{
	uuid_generate(uu);
}

void pmdp_uuid_unparse(const uint8_t *uu, char *str)
{
	uuid_unparse_lower(uu, str);
}

#else	/* Create our own versions */

static inline unsigned hexval(char c)
{
	return c <= '9' ? c - '0' : toupper(c) - 'A' + 0x0a;
}

/*
 * Parse a textual UUID in the form:
 * xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
 * 012345678901234567890123456789012345
 * 0         1         2         3
 */
int pmdp_uuid_parse(const char *str, uint8_t *uu)
{
	char buf[36+1];
	unsigned i;
	unsigned bp = 0;

	if(strlen(str) != 36)
		return -1;
	for(i = 0; i < 36; i++) {
		switch(i) {
		case 8:
		case 13:
		case 18:
		case 23:
			if(str[i] != '-')
				return -1;
			break;
		default:
			if(!isxdigit(str[i]))
				return -1;
			buf[bp++] = str[i];
			break;
		}
	}
	for(i = 0; i < bp; i+= 2) {
		uu[i/2] = (hexval(buf[i]) << 4) | hexval(buf[i+1]);
	}
	return 0;
}

/*
 * Generate a UUID from random data.
 */
void pmdp_uuid_generate(uint8_t *uu)
{
	pmdp_random_bytes(uu, PMDP_UUID_SIZE);
	uu[6] = (uu[6] & 0x0f) | 0x40;	/* Version 4 UUID */
	uu[8] = (uu[8] & 0x3f) | 0x80;
}

/*
 * Generate a textual UUID from data.
 */
void pmdp_uuid_unparse(const uint8_t *uu, char *str)
{
	sprintf(str, "%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x",
		uu[0], uu[1], uu[2], uu[3],
		uu[4], uu[5],
		uu[6], uu[7],
		uu[8], uu[9],
		uu[10], uu[11], uu[12], uu[13], uu[14], uu[15]);
}

#endif
