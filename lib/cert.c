/*
 * Ping-My-Droid
 * Multicasting to my smartphone
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <pmdp_priv.h>
#include <pmdp.h>

#if defined(HAVE_LIBCRYPTO)
#include <openssl/err.h>
#include <openssl/x509.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/rand.h>
#elif defined(HAVE_LIBGNUTLS)
#include <gnutls/x509.h>
#include <gnutls/abstract.h>
#include <gnutls/crypto.h>
#else
#error "No crypto library available, please use openssl or gnutls"
#endif

#define NOISY	1

/** @file cert.c
 * PMDP library X509 certificate interface functions for certificate reading
 * and writing, private key reading, signature calculation, encryption and
 * decryption.
 */

static int isinit = 0;

static void crt_init(void)
{
#if defined(HAVE_LIBCRYPTO)
	if(!isinit) {
		OpenSSL_add_all_algorithms();
		ERR_load_crypto_strings();
		isinit = 1;
	}
	ERR_clear_error();
#elif defined(HAVE_LIBGNUTLS)
	if(!isinit) {
		gnutls_global_init();
		isinit = 1;
	}
#endif
}

static int check_expired(const pmdp_x509_crt_t *crt)
{
#if defined(HAVE_LIBCRYPTO)
	int i;
	crt_init();
	if(!(i = X509_cmp_time(X509_get_notBefore((X509 *)crt), NULL)))
		return PMDP_ERR_CERTIFICATE;
	if(i > 0)
		return PMDP_ERR_EXPIRED;	/* Not yet valid */
	if(!(i = X509_cmp_time(X509_get_notAfter((X509 *)crt), NULL)))
		return PMDP_ERR_CERTIFICATE;
	if(i < 0)
		return PMDP_ERR_EXPIRED;	/* Has expired */
	return 0;
#elif defined(HAVE_LIBGNUTLS)
	time_t s, e, now;
	crt_init();
	s = gnutls_x509_crt_get_activation_time((gnutls_x509_crt_t)crt);
	e = gnutls_x509_crt_get_expiration_time((gnutls_x509_crt_t)crt);
	now = time(NULL);	/* FIXME: timezone... */
	if((time_t)-1 == s || (time_t)-1 == e)
		return PMDP_ERR_CERTIFICATE;
	if(now < s || now > e)
		return PMDP_ERR_EXPIRED;	/* Not yet valid or expired */
	return 0;
#endif
}

static char *load_file(const char *fname, size_t *size)
{
	FILE *fp;
	char *buf;
	struct stat sb;

	if(!fname || !size)
		return NULL;
	if(!(fp = fopen(fname, "rb")))
		return NULL;
	if(fstat(fileno(fp), &sb) < 0) {
		fclose(fp);
		return NULL;
	}
	if(!(buf = malloc(sb.st_size))) {
		fclose(fp);
		return NULL;
	}
	if(sb.st_size != fread(buf, 1, sb.st_size, fp)) {
		free(buf);
		fclose(fp);
		return NULL;
	}
	fclose(fp);
	*size = sb.st_size;
	return buf;
}

/** Load DER formatted X509 certificate from memory
 *
 * Load a certificate from a buffer and assume it is formatted in DER format.
 *
 * Returns a certificate pointer upon success or NULL on error.
 *
 * @param buf Buffer containing binary DER formatted certificate data
 * @param bufsize Size of the buffer data
 */
pmdp_x509_crt_t *pmdp_x509_crt_load_der(const void *buf, size_t bufsize)
{
#if defined(HAVE_LIBCRYPTO)
	BIO *bio;
	X509 *crt;

	crt_init();
	if(!(bio = BIO_new_mem_buf((void *)buf, bufsize)))
		return NULL;
	crt = d2i_X509_bio(bio, NULL);
	BIO_free(bio);
	return (pmdp_x509_crt_t *)crt;
#elif defined(HAVE_LIBGNUTLS)
	gnutls_x509_crt_t crt;
	gnutls_datum_t d;
	crt_init();
	d.data = (unsigned char *)buf;
	d.size = bufsize;
	if(GNUTLS_E_SUCCESS != gnutls_x509_crt_init(&crt))
		return NULL;
	if(GNUTLS_E_SUCCESS != gnutls_x509_crt_import(crt, &d, GNUTLS_X509_FMT_DER)) {
		gnutls_x509_crt_deinit(crt);
		return NULL;
	}
	return (pmdp_x509_crt_t *)crt;
#endif
}

/** Load PEM formatted X509 certificate from memory
 *
 * Load a certificate from a buffer and assume it is formatted in PEM format.
 *
 * Returns a certificate pointer upon success or NULL on error.
 *
 * @param buf Buffer containing PEM formatted certificate data
 * @param bufsize Size of the buffer data
 */
pmdp_x509_crt_t *pmdp_x509_crt_load_pem(const void *buf, size_t bufsize)
{
#if defined(HAVE_LIBCRYPTO)
	BIO *bio;
	X509 *crt;

	crt_init();
	if(!(bio = BIO_new_mem_buf((void *)buf, bufsize)))
		return NULL;
	crt = PEM_read_bio_X509(bio, NULL, 0, NULL);
	BIO_free(bio);
	return (pmdp_x509_crt_t *)crt;
#elif defined(HAVE_LIBGNUTLS)
	gnutls_x509_crt_t crt;
	gnutls_datum_t d;
	crt_init();
	d.data = (unsigned char *)buf;
	d.size = bufsize;
	if(GNUTLS_E_SUCCESS != gnutls_x509_crt_init(&crt))
		return NULL;
	if(GNUTLS_E_SUCCESS != gnutls_x509_crt_import(crt, &d, GNUTLS_X509_FMT_PEM)) {
		gnutls_x509_crt_deinit(crt);
		return NULL;
	}
	return (pmdp_x509_crt_t *)crt;
#endif
}

/** Load PEM formatted X509 certificate from file
 *
 * Load a certificate from a file and assume it is formatted in PEM format.
 *
 * Returns a certificate pointer upon success or NULL on error.
 *
 * @param fname Filename of file containing the certificate
 */
pmdp_x509_crt_t *pmdp_x509_crt_load_file(const char *fname)
{
	char *buf;
	size_t size;
	pmdp_x509_crt_t *crt;
	crt_init();
	if(!(buf = load_file(fname, &size)))
		return NULL;
	crt = pmdp_x509_crt_load_pem(buf, size);
	free(buf);
	return crt;
}

/** Free a X590 certificate
 *
 * Frees all memory associated with the certificate.
 *
 * @param crt Certificate to free
 */
void pmdp_x509_crt_free(pmdp_x509_crt_t *crt)
{
	crt_init();
	if(!crt)
		return;
#if defined(HAVE_LIBCRYPTO)
	X509_free((X509 *)crt);
#elif defined(HAVE_LIBGNUTLS)
	gnutls_x509_crt_deinit((gnutls_x509_crt_t)crt);
#endif
}

/** Load all PEM formatted X509 certificates from bundle-file
 *
 * Load all certificates contained in a file and assume they are formatted in PEM format.
 *
 * Return value:
 * - 0 upon success
 * - PMDP_ERR_INVARG if fname or callback are NULL
 * - PMDP_ERR_NOFILE if fname cannot be opened
 *
 * @param fname Filename of file containing the certificates
 * @param callback Callback function to be called for each certificate read
 * @param ptr Extra argument passed to the callback function
 */
int pmdp_x509_crt_bundle_load_file(const char *fname, pmdp_fncb_crt_t callback, void *ptr)
{
#if defined(HAVE_LIBCRYPTO)
	FILE *fp;
	X509 *crt;

	if(!fname || !callback)
		return PMDP_ERR_INVARG;
	crt_init();
	if(!(fp = fopen(fname, "rb")))
		return PMDP_ERR_NOFILE;

	while(1) {
		if(!(crt = PEM_read_X509(fp, NULL, 0, NULL))) {
			ERR_clear_error();
			break;
		}
		if(!callback((pmdp_x509_crt_t *)crt, ptr))
			X509_free(crt);
	}
	fclose(fp);
	return 0;
#elif defined(HAVE_LIBGNUTLS)
	char *buf;
	size_t size;
	int n;
	unsigned int ncrts, i;
	gnutls_datum_t d;
	gnutls_x509_crt_t *crts;
	crt_init();
	if(!(buf = load_file(fname, &size)))
		return PMDP_ERR_NOFILE;
	d.data = (unsigned char *)buf;
	d.size = size;
	n = gnutls_x509_crt_list_import2(&crts, &ncrts, &d, GNUTLS_X509_FMT_PEM, 0);
	free(buf);
	if(n < 0)
		return PMDP_ERR_CERTIFICATE;

	for(i = 0; i < ncrts; i++) {
		if(!callback((pmdp_x509_crt_t *)crts[i], ptr))
			gnutls_x509_crt_deinit(crts[i]);
	}
	gnutls_free(crts);
	return 0;
#endif
}

/** Write a certificate to a buffer
 *
 * Writes a certificate into a newly allocated buffer in PEM format. The buf
 * parameter will receive a pointer to the allocated memory and the bufsize
 * parameter will receive the size of the allocated buffer. The caller is
 * responsible to free the buffer.
 *
 * Return value is:
 * - 0 upon success
 * - PMDP_ERR_EINVAL if an argument is invalid
 * - PMDP_ERR_CERTIFICATE if the certificate cannot be written
 * - PMDP_ERR_NOMEM if no memory can be allocated
 *
 * @param crt Certificate to write
 * @param buf Reference to the buffer
 * @param bufsize Reference to the buffer size
 */
int pmdp_x509_crt_write(const pmdp_x509_crt_t *crt, void **buf, size_t *bufsize)
{
#if defined(HAVE_LIBCRYPTO)
	BIO *bio;
	long sz;
	char *cptr;

	if(!crt || !buf || ! bufsize)
		return PMDP_ERR_INVARG;

	crt_init();

	*buf = NULL;
	*bufsize = 0;

	if(!(bio = BIO_new(BIO_s_mem())))
		return PMDP_ERR_NOMEM;

	if(!PEM_write_bio_X509(bio, (X509 *)crt)) {
		BIO_free(bio);
		return PMDP_ERR_CERTIFICATE;
	}

	sz = BIO_get_mem_data(bio, &cptr);
	if(sz <= 0) {
		BIO_free(bio);
		return PMDP_ERR_CERTIFICATE;
	}

	if(!(*buf = malloc(sz))) {
		BIO_free(bio);
		return PMDP_ERR_NOMEM;
	}

	memcpy(*buf, cptr, sz);
	*bufsize = (size_t)sz;
	BIO_free(bio);
	return 0;
#elif defined(HAVE_LIBGNUTLS)
	void *pb = NULL;
	size_t pbs = 4096;	/* Will fit 2048 bit keys */
	int err;
	if(!(pb = malloc(pbs)))
		return PMDP_ERR_NOMEM;

	while(1) {
		size_t sz = pbs;
		err = gnutls_x509_crt_export((gnutls_x509_crt_t)crt, GNUTLS_X509_FMT_PEM, pb, &sz);
		if(!err) {
			*buf = pb;
			*bufsize = sz;
			return 0;
		} else if(GNUTLS_E_SHORT_MEMORY_BUFFER == err) {
			void *npb;
			pbs *= 2;
			if(pbs > 64*1024) {
				/* A cert that is larger than 64k in PEM format is a bad sign... */
				free(pb);
				return PMDP_ERR_CERTIFICATE;
			}
			if(!(npb = realloc(pb, pbs))) {
				free(pb);
				return PMDP_ERR_NOMEM;
			}
			pb = npb;
		}
	}
#endif
}

#if defined(HAVE_LIBCRYPTO)
struct pw_cb_arg {
	void			*arg;
	pmdp_fncb_passwd_t	cb;
};

static int password_callback(char *str, int slen, int rw, void *arg)
{
	assert(arg != NULL);
	assert(rw == 0);
	assert(str != NULL);
	assert(slen > 0);
	return ((struct pw_cb_arg *)arg)->cb(str, slen, ((struct pw_cb_arg *)arg)->arg);
}
#endif

/** Load a PEM formatted X509 private key from memory
 *
 * Creates a private key from a memory buffer. The callback function is called
 * if the private key is encrypted and cb is not NULL. The underlying password
 * mechanism is used if cb is NULL and a password is required. No callback is
 * required for private keys without password.
 *
 * Return value is a private key or NULL upon error.
 *
 * @param buf Buffer that holds the PEM formmated key
 * @param bufsize Size of the buffer data
 * @param cb Callback for any required password
 * @param arg Extra parameter passed to the callback function
 */
pmdp_x509_key_t *pmdp_x509_key_load_pem(const char *buf, size_t bufsize, pmdp_fncb_passwd_t cb, void *arg)
{
#if defined(HAVE_LIBCRYPTO)
	BIO *bio;
	EVP_PKEY *pkey;
	struct pw_cb_arg pw;

	pw.arg = arg;
	pw.cb = cb;

	crt_init();

	if(!(bio = BIO_new_mem_buf((void *)buf, bufsize)))
		return NULL;
	if(cb)
		pkey = PEM_read_bio_PrivateKey(bio, NULL, password_callback, &pw);
	else
		pkey = PEM_read_bio_PrivateKey(bio, NULL, NULL, NULL);
	BIO_free(bio);
	return (pmdp_x509_key_t *)pkey;
#elif defined(HAVE_LIBGNUTLS)
	gnutls_x509_privkey_t pkey;
	gnutls_datum_t d;
	int err;
	char *pwbuf;
	int pwsize;

	crt_init();

	if(GNUTLS_E_SUCCESS != gnutls_x509_privkey_init(&pkey))
		return NULL;

	d.data = (unsigned char *)buf;
	d.size = bufsize;
	/* First try plain text private key */
	err = gnutls_x509_privkey_import_pkcs8(pkey, &d, GNUTLS_X509_FMT_PEM, NULL, GNUTLS_PKCS_PLAIN);
	if(GNUTLS_E_SUCCESS == err)
		return (pmdp_x509_key_t *)pkey;
	if(GNUTLS_E_DECRYPTION_FAILED != err) {
		/* Some other error that decryption failure */
		gnutls_x509_privkey_deinit(pkey);
		return NULL;
	}

	if(!cb) {
		/* FIXME: no callback, do console interfacing to get pass phrase */
		gnutls_x509_privkey_deinit(pkey);
		return NULL;
	}

#define PWBUFSIZE	(1024-1)
	pwbuf = calloc(PWBUFSIZE+1, 1);
	if(!pwbuf) {
		gnutls_x509_privkey_deinit(pkey);
		return NULL;
	}
	pwsize = cb(pwbuf, PWBUFSIZE, arg);
	if(pwsize <= 0 || pwsize > PWBUFSIZE) {
		/* No pass phrase given, error on input or too large a result, cannot decrypt */
		gnutls_x509_privkey_deinit(pkey);
		return NULL;
	}
	pwbuf[pwsize] = 0;	/* Sure to terminate */
	/* Try again to decrypt the private key */
	err = gnutls_x509_privkey_import_pkcs8(pkey, &d, GNUTLS_X509_FMT_PEM, pwbuf, 0);
	free(pwbuf);
	if(GNUTLS_E_SUCCESS == err)
		return (pmdp_x509_key_t *)pkey;

	/* Failure */
	gnutls_x509_privkey_deinit(pkey);
	return NULL;
#endif
}

/** Load a PEM formatted X509 private key from file
 *
 * Creates a private key from a file. The callback function is called if the
 * private key is encrypted and cb is not NULL. The underlying password
 * mechanism is used if cb is NULL and a password is required. No callback is
 * required for private keys without password.
 *
 * Return value is a private key or NULL upon error.
 *
 * @param fname Filename of file containing PEM formmated key
 * @param cb Callback for any required password
 * @param arg Extra parameter passed to the callback function
 */
pmdp_x509_key_t *pmdp_x509_key_load_file(const char *fname, pmdp_fncb_passwd_t cb, void *arg)
{
	char *buf;
	size_t size;
	pmdp_x509_key_t *pkey;

	crt_init();
	if(!(buf = load_file(fname, &size)))
		return NULL;
	pkey = pmdp_x509_key_load_pem(buf, size, cb, arg);
	free(buf);
	return pkey;
}

/** Free a X590 private key
 *
 * Frees all memory associated with the private key.
 *
 * @param pkey Private key to free
 */
void pmdp_x509_key_free(pmdp_x509_key_t *pkey)
{
	crt_init();
	if(!pkey)
		return;
#if defined(HAVE_LIBCRYPTO)
	EVP_PKEY_free((EVP_PKEY *)pkey);
#elif defined(HAVE_LIBGNUTLS)
	gnutls_x509_privkey_deinit((gnutls_x509_privkey_t)pkey);
#endif
}

/** Retrieve the private key size
 *
 * Returns the private key size in # of bits, zero (0) is returned if an error
 * is encountered and PMDP_ERR_INVARG if key is NULL.
 *
 * @param key Private key to examine
 */
int pmdp_x509_privkeysize(const pmdp_x509_key_t *key)
{
#if defined(HAVE_LIBCRYPTO)
	int size = 0;
	crt_init();
	if(!key)
		return PMDP_ERR_INVARG;
	if(EVP_PKEY_RSA == EVP_PKEY_type(((EVP_PKEY *)key)->type)) {
		RSA *rsa = EVP_PKEY_get1_RSA((EVP_PKEY *)key);
		if(rsa)
			size = 8 * RSA_size(rsa);
	}
	return size;
#elif defined(HAVE_LIBGNUTLS)
	unsigned int size = 0;
	crt_init();
	if(!key)
		return PMDP_ERR_INVARG;
	if(gnutls_x509_privkey_get_pk_algorithm2((gnutls_x509_privkey_t)key, &size) < 0)
		return 0;
	return (int)size;
#endif
}

/** Retrieve the public key size
 *
 * Returns the public key size in # of bits, PMDP_ERR_CERTIFICATE if the public
 * key cannot be extracted from the certificate and zero (0) is returned if any
 * other error is encountered.
 *
 * @param key Public key to examine
 */
int pmdp_x509_pubkeysize(const pmdp_x509_crt_t *crt)
{
#if defined(HAVE_LIBCRYPTO)
	EVP_PKEY *pubkey;
	int size;
	crt_init();
	if(!crt)
		return PMDP_ERR_INVARG;
	if(!(pubkey = X509_get_pubkey((X509 *)crt))) {
#ifdef NOISY
		ERR_print_errors_fp(stderr);
#endif
		return PMDP_ERR_CERTIFICATE;
	}
	size = pmdp_x509_privkeysize((pmdp_x509_key_t *)pubkey);
	EVP_PKEY_free(pubkey);
	return size;
#elif defined(HAVE_LIBGNUTLS)
	unsigned int size = 0;
	gnutls_pubkey_t pubkey;
	crt_init();
	if(!crt)
		return PMDP_ERR_INVARG;
	if(GNUTLS_E_SUCCESS != gnutls_pubkey_init(&pubkey))
		return 0;
	if(GNUTLS_E_SUCCESS != gnutls_pubkey_import_x509(pubkey, (gnutls_x509_crt_t)crt, 0)) {
		gnutls_pubkey_deinit(pubkey);
		return 0;
	}
	if(gnutls_pubkey_get_pk_algorithm(pubkey, &size) < 0) {
		gnutls_pubkey_deinit(pubkey);
		return 0;
	}
	gnutls_pubkey_deinit(pubkey);
	return size;
#endif
}

/** Calculate the certificate fingerprint
 *
 * The certificate's fingerprint (hash) is returned in a newly allocated
 * buffer. The size of the fingerprint is returned in bufsize. The caller must
 * free the buffer after use.
 *
 * Return value:
 * - 0 upon success and buf/bufsize contain the fingerprint information
 * - PMDP_ERR_INVARG is any argument is NULL
 * - PMDP_ERR_NONEM if no memory could be allocated for the buffer
 * - PMDP_ERR_FINGERPRINT is the fingerprint could not be calculated
 *
 * @param crt Certificate to fingerprint
 * @param buf Buffer reference to receive the buffer pointer
 * @param bufsize Buffer size reference to receive the fingerprint size
 */
int pmdp_x509_fingerprint(const pmdp_x509_crt_t *crt, uint8_t **buf, size_t *bufsize)
{
#if defined(HAVE_LIBCRYPTO)
	unsigned n = EVP_MAX_MD_SIZE;
	int res;
	unsigned char lbuf[EVP_MAX_MD_SIZE];
	crt_init();
	if(!crt || !buf || !bufsize)
		return PMDP_ERR_INVARG;
	*buf = NULL;
	*bufsize = 0;
	if(0 != (res = X509_digest((X509 *)crt, EVP_sha1(), lbuf, &n))) {
		if(!(*buf = malloc(n)))
			return PMDP_ERR_NOMEM;
		memcpy(*buf, lbuf, n);
		*bufsize = n;
		return 0;
	}
	return PMDP_ERR_FINGERPRINT;
#elif defined(HAVE_LIBGNUTLS)
	size_t sz = 0;
	crt_init();
	if(!crt || !buf || !bufsize)
		return PMDP_ERR_INVARG;
	*buf = NULL;
	*bufsize = 0;
	if(GNUTLS_E_SHORT_MEMORY_BUFFER != gnutls_x509_crt_get_fingerprint((gnutls_x509_crt_t)crt, GNUTLS_DIG_SHA1, NULL, &sz))
		return PMDP_ERR_FINGERPRINT;
	if(!sz)	/* Size must be larger than zero now */
		return PMDP_ERR_FINGERPRINT;
	if(!(*buf = malloc(sz)))
		return PMDP_ERR_NOMEM;
	if(GNUTLS_E_SUCCESS != gnutls_x509_crt_get_fingerprint((gnutls_x509_crt_t)crt, GNUTLS_DIG_SHA1, *buf, &sz)) {
		free(*buf);
		*buf = NULL;
		return PMDP_ERR_FINGERPRINT;
	}
	*bufsize = sz;
	return 0;
#endif
}

/** Sign data with a private key
 *
 * Calculates the signature of data and places the signature in sig. The size
 * of the signature buffer sig must be greater or equal than the private key
 * length. The minimum size may be found by calling pmdp_x509_privkeysize() on
 * the private key. The actual signature size (in bytes) is returned in siglen
 * if siglen is not NULL.
 *
 * Return value:
 * - 0 upon success and sig contains the signature
 * - PMDP_ERR_INVARG is pkey, data or sig is NULL
 * - PMDP_ERR_SIGN if the signature calculation failed
 * - PMDP_ERR_NOMEM if no memory was available
 *
 * @param pkey Private key to sign with
 * @param data Source data for signature
 * @param datalen Size of the source data
 * @param sig Buffer receiving the signature
 * @param siglen Size of actual signature
 */
int pmdp_x509_sign(const pmdp_x509_key_t *pkey, const void *data, size_t datalen, void *sig, size_t *siglen)
{
#if defined(HAVE_LIBCRYPTO)
	EVP_MD_CTX md_ctx;
	unsigned n = *siglen;

	crt_init();
	if(!pkey || !data || !sig)
		return PMDP_ERR_INVARG;
	EVP_MD_CTX_init(&md_ctx);
	EVP_SignInit(&md_ctx, EVP_sha1());
	EVP_SignUpdate(&md_ctx, data, datalen);
	if(-1 == EVP_SignFinal(&md_ctx, sig, &n, (EVP_PKEY *)pkey)) {
#ifdef NOISY
		ERR_print_errors_fp(stderr);
#endif
		EVP_MD_CTX_cleanup(&md_ctx);
		if(siglen)
			*siglen = n;
		return PMDP_ERR_SIGN;
	}
	EVP_MD_CTX_cleanup(&md_ctx);
	if(siglen)
		*siglen = n;
	return 0;
#elif defined(HAVE_LIBGNUTLS)
	gnutls_privkey_t privkey;
	gnutls_datum_t src;
	gnutls_datum_t dst;
	crt_init();
	if(!pkey || !data || !sig)
		return PMDP_ERR_INVARG;

	if(GNUTLS_E_SUCCESS != gnutls_privkey_init(&privkey))
		return PMDP_ERR_NOMEM;
	if(GNUTLS_E_SUCCESS != gnutls_privkey_import_x509(privkey, (gnutls_x509_privkey_t)pkey, GNUTLS_PRIVKEY_IMPORT_AUTO_RELEASE)) {
		gnutls_privkey_deinit(privkey);
		return PMDP_ERR_SIGN;
	}
	src.data = (unsigned char *)data;
	src.size = datalen;
	memset(&dst, 0, sizeof(dst));
	if(GNUTLS_E_SUCCESS != gnutls_privkey_sign_data(privkey, GNUTLS_DIG_SHA1, 0, &src, &dst)) {
		gnutls_privkey_deinit(privkey);
		return PMDP_ERR_SIGN;
	}
	gnutls_privkey_deinit(privkey);
	memcpy(sig, dst.data, dst.size);
	*siglen = dst.size;
	return 0;
#endif
}

/** Verify signed data with a certificate
 *
 * Verifies the signature in sig of length siglen over data and length datalen
 * using certificate crt. The certificate is tested for validity prior to
 * verification.
 *
 * Return value:
 * - 0 upon successful verification
 * - PMDP_ERR_INVARG is crt, data or sig is NULL or datalen or siglen is zero
 * - PMDP_ERR_VERIFY if the signature verification failed
 * - PMDP_ERR_CERTIFICATE if the certificate is invalid
 * - PMDP_ERR_EXPIRED if the certificate is expired
 *
 * @param crt Certificate to verify with
 * @param data Source data for signature
 * @param datalen Size of the source data
 * @param sig Buffer containing the signature
 * @param siglen Size of signature
 */
int pmdp_x509_verify(const pmdp_x509_crt_t *crt, const void *data, size_t datalen, void *sig, size_t siglen)
{
#if defined(HAVE_LIBCRYPTO)
	EVP_PKEY *pubkey;
	EVP_MD_CTX md_ctx;
	int res;

	crt_init();
	if(!crt || !data || !datalen || !sig || !siglen)
		return PMDP_ERR_INVARG;

	if(0 != (res = check_expired(crt)))
		return res;

	if(!(pubkey = X509_get_pubkey((X509 *)crt))) {
#ifdef NOISY
		ERR_print_errors_fp(stderr);
#endif
		return PMDP_ERR_VERIFY;
	}
	EVP_MD_CTX_init(&md_ctx);
	EVP_VerifyInit(&md_ctx, EVP_sha1());
	EVP_VerifyUpdate(&md_ctx, data, datalen);
	res = EVP_VerifyFinal(&md_ctx, sig, siglen, pubkey);
	EVP_PKEY_free(pubkey);
	EVP_MD_CTX_cleanup(&md_ctx);
	return res == 1 ? 0 : PMDP_ERR_VERIFY;
#elif defined(HAVE_LIBGNUTLS)
	gnutls_pubkey_t pubkey;
	gnutls_datum_t srcdat;
	gnutls_datum_t sigdat;
	int res;

	crt_init();

	if(!crt || !data || !datalen || !sig || !siglen)
		return PMDP_ERR_INVARG;

	if(0 != (res = check_expired(crt)))
		return res;

	if(GNUTLS_E_SUCCESS != gnutls_pubkey_init(&pubkey))
		return PMDP_ERR_CERTIFICATE;
	if(GNUTLS_E_SUCCESS != (res = gnutls_pubkey_import_x509(pubkey, (gnutls_x509_crt_t)crt, 0))) {
		gnutls_pubkey_deinit(pubkey);
		return PMDP_ERR_CERTIFICATE;
	}
	srcdat.data = (unsigned char *)data;
	srcdat.size = datalen;
	sigdat.data = (unsigned char *)sig;
	sigdat.size = siglen;
	if((res = gnutls_pubkey_verify_data2(pubkey, GNUTLS_DIG_NULL, 0, &srcdat, &sigdat)) < 0) {
		gnutls_pubkey_deinit(pubkey);
		return PMDP_ERR_VERIFY;
	}
	gnutls_pubkey_deinit(pubkey);
	return 0;
#endif
}

/** Encrypt data with a certificate
 *
 * Encrypts data of length datalen using crt and places the result in a newly
 * allocated buffer referenced by encr. The certificate is verified before
 * usage. The encrypted data is formatted using PKCS1 v1.5 padding. The caller
 * is responsible for freeing the newly allocated buffer.
 *
 * Return value:
 * - 0 upon successful verification
 * - PMDP_ERR_INVARG is crt, data, encr or encrlen is NULL
 * - PMDP_ERR_ENCRYPT if the encryption failed
 * - PMDP_ERR_CERTIFICATE if the certificate is invalid
 * - PMDP_ERR_EXPIRED if the certificate is expired
 * - PMDP_ERR_NOTIMPL is this function is not implemented
 *
 * @param crt Certificate to encrypt with
 * @param data Source data to encrypt
 * @param datalen Size of the source data
 * @param encr Buffer reference receiving the pointer to the newly allocaed buffer containing the encrypted data
 * @param encrlen Size of encrypted data
 */
int pmdp_x509_encrypt(const pmdp_x509_crt_t *crt, const uint8_t *data, size_t datalen, uint8_t **encr, size_t *encrlen)
{
#if defined(HAVE_LIBCRYPTO)
#ifdef HAVE_EVP_PKEY_CTX
	uint8_t *target;
	size_t targetsize = 0;
	EVP_PKEY *key;
	EVP_PKEY_CTX *ctx;
	int rv;

	crt_init();
	if(!crt || !data || !datalen || !encr || !encrlen)
		return PMDP_ERR_INVARG;

	*encr = NULL;
	*encrlen = 0;

	if(0 != (rv = check_expired(crt)))
		return rv;

	if(!(key = X509_get_pubkey((X509 *)crt)))
		return PMDP_ERR_CERTIFICATE;

	if(!(ctx = EVP_PKEY_CTX_new(key, NULL))) {
		EVP_PKEY_free(key);
		return PMDP_ERR_CERTIFICATE;
	}

	if(0 >= EVP_PKEY_encrypt_init(ctx)) {
		EVP_PKEY_CTX_free(ctx);
		EVP_PKEY_free(key);
		return PMDP_ERR_ENCRYPT;
	}

	if(0 >= EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_PADDING)) {
		EVP_PKEY_CTX_free(ctx);
		EVP_PKEY_free(key);
		return PMDP_ERR_ENCRYPT;
	}

	if(0 >= EVP_PKEY_encrypt(ctx, NULL, &targetsize, data, datalen)) {
		EVP_PKEY_CTX_free(ctx);
		EVP_PKEY_free(key);
		return PMDP_ERR_ENCRYPT;
	}

	if(!(target = malloc(targetsize))) {
		EVP_PKEY_CTX_free(ctx);
		EVP_PKEY_free(key);
		return PMDP_ERR_NOMEM;
	}

	if(0 >= EVP_PKEY_encrypt(ctx, target, &targetsize, data, datalen)) {
		EVP_PKEY_CTX_free(ctx);
		EVP_PKEY_free(key);
		free(target);
		return PMDP_ERR_ENCRYPT;
	}

	*encr = target;
	*encrlen = targetsize;
	EVP_PKEY_CTX_free(ctx);
	EVP_PKEY_free(key);
	return 0;
#else
	(void)crt;
	(void)data;
	(void)datalen;
	(void)encr;
	(void)encrlen;
	return PMDP_ERR_NOTIMPL;
#endif
#elif defined(HAVE_LIBGNUTLS)
	gnutls_pubkey_t pubkey;
	gnutls_datum_t src;
	gnutls_datum_t dst;
	int res;

	crt_init();

	if(!crt || !data || !datalen || !encr || !encrlen)
		return PMDP_ERR_INVARG;

	if(0 != (res = check_expired(crt)))
		return res;

	if(GNUTLS_E_SUCCESS != gnutls_pubkey_init(&pubkey))
		return PMDP_ERR_CERTIFICATE;

	if(GNUTLS_E_SUCCESS != gnutls_pubkey_import_x509(pubkey, (gnutls_x509_crt_t)crt, 0)) {
		gnutls_pubkey_deinit(pubkey);
		return PMDP_ERR_CERTIFICATE;
	}
	src.data = (unsigned char *)data;
	src.size = datalen;
	memset(&dst, 0, sizeof(dst));
	if(GNUTLS_E_SUCCESS != gnutls_pubkey_encrypt_data(pubkey, 0, &src, &dst)) {
		gnutls_pubkey_deinit(pubkey);
		return PMDP_ERR_ENCRYPT;
	}
	gnutls_pubkey_deinit(pubkey);
	if(!(*encr = malloc(dst.size))) {
		gnutls_free(dst.data);
		return PMDP_ERR_NOMEM;
	}
	memcpy(*encr, dst.data, dst.size);
	*encrlen = dst.size;
	gnutls_free(dst.data);
	return 0;
#endif
}

/** Decrypt data with a private key
 *
 * Decrypts data of length datalen using key and places the result in a newly
 * allocated buffer referenced by decr. The encrypted data is assumed formatted
 * using PKCS1 v1.5 padding. The caller is responsible for freeing the newly
 * allocated buffer.
 *
 * Return value:
 * - 0 upon successful verification
 * - PMDP_ERR_INVARG is key, data, decr or decrlen is NULL
 * - PMDP_ERR_DECRYPT if the decryption failed
 * - PMDP_ERR_CERTIFICATE if the certificate is invalid
 * - PMDP_ERR_NOTIMPL is this function is not implemented
 *
 * @param key Private key to decrypt with
 * @param data Source data to decrypt
 * @param datalen Size of the source data
 * @param decr Buffer reference receiving the pointer to the newly allocated buffer containing the decrypted data
 * @param decrlen Size of decrypted data
 */
int pmdp_x509_decrypt(const pmdp_x509_key_t *key, const uint8_t *data, size_t datalen, uint8_t **decr, size_t *decrlen)
{
#if defined(HAVE_LIBCRYPTO)
#ifdef HAVE_EVP_PKEY_CTX
	uint8_t *target;
	size_t targetsize = 0;
	EVP_PKEY_CTX *ctx;

	crt_init();
	if(!key || !data || !datalen || !decr || !decrlen)
		return PMDP_ERR_INVARG;

	*decr = NULL;
	*decrlen = 0;

	if(!(ctx = EVP_PKEY_CTX_new((EVP_PKEY *)key, NULL)))
		return PMDP_ERR_CERTIFICATE;

	if(0 >= EVP_PKEY_decrypt_init(ctx)) {
		EVP_PKEY_CTX_free(ctx);
		return PMDP_ERR_DECRYPT;
	}

	if(0 >= EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_PADDING)) {
		EVP_PKEY_CTX_free(ctx);
		return PMDP_ERR_DECRYPT;
	}

	if(0 >= EVP_PKEY_decrypt(ctx, NULL, &targetsize, data, datalen)) {
		EVP_PKEY_CTX_free(ctx);
		return PMDP_ERR_DECRYPT;
	}
	if(!(target = malloc(targetsize))) {
		EVP_PKEY_CTX_free(ctx);
		return PMDP_ERR_NOMEM;
	}

	if(0 >= EVP_PKEY_decrypt(ctx, target, &targetsize, data, datalen)) {
		EVP_PKEY_CTX_free(ctx);
		free(target);
		return PMDP_ERR_DECRYPT;
	}

	*decr = target;
	*decrlen = targetsize;
	EVP_PKEY_CTX_free(ctx);
	return 0;
#else
	(void)key;
	(void)data;
	(void)datalen;
	(void)decr;
	(void)decrlen;
	return PMDP_ERR_NOTIMPL;
#endif
#elif defined(HAVE_LIBGNUTLS)
	gnutls_privkey_t privkey;
	gnutls_datum_t src;
	gnutls_datum_t dst;
	crt_init();
	if(!key || !data || !decr || !decrlen)
		return PMDP_ERR_INVARG;

	if(GNUTLS_E_SUCCESS != gnutls_privkey_init(&privkey))
		return PMDP_ERR_NOMEM;
	if(GNUTLS_E_SUCCESS != gnutls_privkey_import_x509(privkey, (gnutls_x509_privkey_t)key, 0)) {
		gnutls_privkey_deinit(privkey);
		return PMDP_ERR_DECRYPT;
	}
	src.data = (unsigned char *)data;
	src.size = datalen;
	memset(&dst, 0, sizeof(dst));
	if(GNUTLS_E_SUCCESS != gnutls_privkey_decrypt_data(privkey, 0, &src, &dst)) {
		gnutls_privkey_deinit(privkey);
		return PMDP_ERR_SIGN;
	}
	gnutls_privkey_deinit(privkey);
	if(!(*decr = malloc(dst.size))) {
		gnutls_privkey_deinit(privkey);
		return PMDP_ERR_NOMEM;
	}
	memcpy(*decr, dst.data, dst.size);
	*decrlen = dst.size;
	gnutls_free(dst.data);
	return 0;
#endif
}

int pmdp_random_bytes(uint8_t *buf, size_t buflen)
{
#if defined(HAVE_LIBCRYPTO)
	if(!RAND_bytes((unsigned char *)buf, buflen))
		return PMDP_ERR_NORANDOM;
	return 0;
#elif defined(HAVE_LIBGNUTLS)
	gnutls_rnd_refresh();
	if(gnutls_rnd(GNUTLS_RND_RANDOM, buf, buflen) < 0)
		return PMDP_ERR_NORANDOM;
	return 0;
#endif
}
